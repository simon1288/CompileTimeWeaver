﻿using System.Reflection;
using System.Threading.Tasks;

namespace CompileTimeWeaver
{
    public interface IAdvice
    {
        bool ExcludeConstructors { get; set; }
        bool ExcludeAutoProperties { get; set; }
        bool ExcludeGetters { get; set; }
        bool ExcludeSetters { get; set; }
        bool ExcludeStatic { get; set; }
        int Order { get; set; }

        /// <summary>
        /// Intercept a sync method.
        /// </summary>
        /// <param name="invocation">The invocation context.</param>
        object Advise(IInvocation invocation);

        /// <summary>
        /// Intercept an async method with return
        /// </summary>
        /// <param name="invocation"></param>
        /// <returns></returns>
        Task<object> AdviseAsync(IInvocation invocation);
    }
}