﻿namespace CompileTimeWeaver.Logging
{
    public enum LoggingVerbosity
    {
        Debug,
        Info,
        Warn,
        Error,
        Fatal
    }
}