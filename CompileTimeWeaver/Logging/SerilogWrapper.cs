﻿using System;

namespace CompileTimeWeaver.Logging
{
    internal class SerilogWrapper : ILogger
    {
        protected readonly Serilog.ILogger logger;

        public SerilogWrapper(Type type)
        {
            logger = Serilog.Log.ForContext(type);
        }

        public bool IsDebugEnabled
        {
            get { return logger.IsEnabled(Serilog.Events.LogEventLevel.Debug); }
        }

        public bool IsErrorEnabled
        {
            get { return logger.IsEnabled(Serilog.Events.LogEventLevel.Error); }
        }

        public bool IsFatalEnabled
        {
            get { return logger.IsEnabled(Serilog.Events.LogEventLevel.Fatal); }
        }

        public bool IsInfoEnabled
        {
            get { return logger.IsEnabled(Serilog.Events.LogEventLevel.Information); }
        }

        public bool IsWarnEnabled
        {
            get { return logger.IsEnabled(Serilog.Events.LogEventLevel.Warning); }
        }

        //public void SetVerbosity(LoggingVerbosity verbosity)
        //{
        //    var logLevel = Serilog.Events.LogEventLevel.Information;
        //    switch (verbosity)
        //    {
        //        case LoggingVerbosity.Debug:
        //            logLevel = Serilog.Events.LogEventLevel.Debug;
        //            break;
        //        case LoggingVerbosity.Info:
        //            logLevel = Serilog.Events.LogEventLevel.Information;
        //            break;
        //        case LoggingVerbosity.Warn:
        //            logLevel = Serilog.Events.LogEventLevel.Warning;
        //            break;
        //        case LoggingVerbosity.Error:
        //            logLevel = Serilog.Events.LogEventLevel.Error;
        //            break;
        //        case LoggingVerbosity.Fatal:
        //            logLevel = Serilog.Events.LogEventLevel.Fatal;
        //            break;
        //    }
        //    ...
        //}

        public void Debug(object message)
        {
            logger.Debug(message.ToString());
        }

        public void Debug(object message, Exception exception)
        {
            logger.Debug(exception, message.ToString());
        }

        public void DebugFormat(string format, params object[] args)
        {
            logger.Debug(format, args);
        }

        public void DebugFormat(string format, object arg0)
        {
            logger.Debug(format, arg0);
        }

        public void DebugFormat(string format, object arg0, object arg1)
        {
            logger.Debug(format, arg0, arg1);
        }

        public void DebugFormat(string format, object arg0, object arg1, object arg2)
        {
            logger.Debug(format, arg0, arg1, arg2);
        }

        public void DebugFormat(IFormatProvider provider, string format, params object[] args)
        {
            if (logger.IsEnabled(Serilog.Events.LogEventLevel.Debug))
                logger.Debug(string.Format(provider, format, args));
        }

        public void Info(object message)
        {
            logger.Information(message.ToString());
        }

        public void Info(object message, Exception exception)
        {
            logger.Information(exception, message.ToString());
        }

        public void InfoFormat(string format, params object[] args)
        {
            logger.Information(format, args);
        }

        public void InfoFormat(string format, object arg0)
        {
            logger.Information(format, arg0);
        }

        public void InfoFormat(string format, object arg0, object arg1)
        {
            logger.Information(format, arg0, arg1);
        }

        public void InfoFormat(string format, object arg0, object arg1, object arg2)
        {
            logger.Information(format, arg0, arg1, arg2);
        }

        public void InfoFormat(IFormatProvider provider, string format, params object[] args)
        {
            if (logger.IsEnabled(Serilog.Events.LogEventLevel.Information))
                logger.Information(string.Format(provider, format, args));
        }

        public void Warn(object message)
        {
            logger.Warning(message.ToString());
        }

        public void Warn(object message, Exception exception)
        {
            logger.Warning(exception, message.ToString());
        }

        public void WarnFormat(string format, params object[] args)
        {
            logger.Warning(format, args);
        }

        public void WarnFormat(string format, object arg0)
        {
            logger.Warning(format, arg0);
        }

        public void WarnFormat(string format, object arg0, object arg1)
        {
            logger.Warning(format, arg0, arg1);
        }

        public void WarnFormat(string format, object arg0, object arg1, object arg2)
        {
            logger.Warning(format, arg0, arg1, arg2);
        }

        public void WarnFormat(IFormatProvider provider, string format, params object[] args)
        {
            if (logger.IsEnabled(Serilog.Events.LogEventLevel.Warning))
                logger.Warning(string.Format(provider, format, args));
        }

        public void Error(object message)
        {
            logger.Error(message.ToString());
        }

        public void Error(object message, Exception exception)
        {
            logger.Error(exception, message.ToString());
        }

        public void ErrorFormat(string format, params object[] args)
        {
            logger.Error(format, args);
        }

        public void ErrorFormat(string format, object arg0)
        {
            logger.Error(format, arg0);
        }

        public void ErrorFormat(string format, object arg0, object arg1)
        {
            logger.Error(format, arg0, arg1);
        }

        public void ErrorFormat(string format, object arg0, object arg1, object arg2)
        {
            logger.Error(format, arg0, arg1, arg2);
        }

        public void ErrorFormat(IFormatProvider provider, string format, params object[] args)
        {
            if (logger.IsEnabled(Serilog.Events.LogEventLevel.Error))
                logger.Error(string.Format(provider, format, args));
        }

        public void Fatal(object message)
        {
            logger.Fatal(message.ToString());
        }

        public void Fatal(object message, Exception exception)
        {
            logger.Fatal(exception, message.ToString());
        }

        public void FatalFormat(string format, params object[] args)
        {
            logger.Fatal(format, args);
        }

        public void FatalFormat(string format, object arg0)
        {
            logger.Fatal(format, arg0);
        }

        public void FatalFormat(string format, object arg0, object arg1)
        {
            logger.Fatal(format, arg0, arg1);
        }

        public void FatalFormat(string format, object arg0, object arg1, object arg2)
        {
            logger.Fatal(format, arg0, arg1, arg2);
        }

        public void FatalFormat(IFormatProvider provider, string format, params object[] args)
        {
            if (logger.IsEnabled(Serilog.Events.LogEventLevel.Fatal))
                logger.Fatal(string.Format(provider, format, args));
        }
    }
}