﻿using System;
using System.Reflection;

//WARNING: Do not include any reference to any type in the log4net or NLog namespace in this class. 

namespace CompileTimeWeaver.Logging
{
    internal static class LogManager
    {
        private enum LoggingFramework
        {
            Null,
            Log4Net,
            Nlog,
            Console,
            Serilog
        }

        private static readonly LoggingFramework loggingFramework;

        static LogManager()
        {
            loggingFramework = LoggingFramework.Null;

            try
            {
                Assembly.Load("NLog");
                loggingFramework = LoggingFramework.Nlog;
                return;
            }
            catch
            {
            }

            if (loggingFramework == LoggingFramework.Null)
            {
                try
                {
                    Assembly.Load("log4net");
                    loggingFramework = LoggingFramework.Log4Net;
                    return;
                }
                catch
                {
                }
            }

            if (loggingFramework == LoggingFramework.Null)
            {
                try
                {
                    Assembly.Load("Serilog");
                    loggingFramework = LoggingFramework.Serilog;
                    return;
                }
                catch
                {
                }
            }
#if DEBUG
            if (loggingFramework == LoggingFramework.Null)
            {
                loggingFramework = LoggingFramework.Console;
            }
#endif
        }

        public static ILogger GetLogger(Type type)
        {
            switch (loggingFramework)
            {
                case LoggingFramework.Log4Net:
                    return new Log4NetWrapper(type);
                case LoggingFramework.Nlog:
                    return new NlogWrapper(type);
                case LoggingFramework.Console:
                    return ConsoleLogger.Instance;
                case LoggingFramework.Serilog:
                    return new SerilogWrapper(type);
                default:
                    return NullLogger.Instance;
            }
        }
    }
}
