using System;
using System.IO;
using System.Reflection;
using System.Xml;
using log4net;

namespace CompileTimeWeaver.Logging
{
    internal class Log4NetWrapper : ILogger
    {
        private static bool configured = false;

        private static void ConfigureLog4Net()
        {
            if (!configured)
            {
                XmlDocument log4netConfig = new XmlDocument();
                log4netConfig.Load(File.OpenRead("log4net.config"));
                var repo = log4net.LogManager.CreateRepository(Assembly.GetEntryAssembly(), typeof(log4net.Repository.Hierarchy.Hierarchy));
                log4net.Config.XmlConfigurator.Configure(repo, log4netConfig["log4net"]);
                configured = true;
            }
        }

        private readonly ILog _iLog;

        public Log4NetWrapper(Type type)
        {
            ConfigureLog4Net();
            this._iLog = log4net.LogManager.GetLogger(type);
        }

        public bool IsDebugEnabled
        {
            get { return _iLog.IsDebugEnabled; }
        }

        public bool IsErrorEnabled
        {
            get { return _iLog.IsErrorEnabled; }
        }

        public bool IsFatalEnabled
        {
            get { return _iLog.IsFatalEnabled; }
        }

        public bool IsInfoEnabled
        {
            get { return _iLog.IsInfoEnabled; }
        }

        public bool IsWarnEnabled
        {
            get { return _iLog.IsWarnEnabled; }
        }

        //public void SetVerbosity(LoggingVerbosity verbosity)
        //{
        //    switch (verbosity)
        //    {
        //        case LoggingVerbosity.Debug:
        //            ((log4net.Repository.Hierarchy.Logger)_iLog.Logger).Level = log4net.Core.Level.Debug;
        //            break;
        //        case LoggingVerbosity.Info:
        //            ((log4net.Repository.Hierarchy.Logger)_iLog.Logger).Level = log4net.Core.Level.Info;
        //            break;
        //        case LoggingVerbosity.Warn:
        //            ((log4net.Repository.Hierarchy.Logger)_iLog.Logger).Level = log4net.Core.Level.Warn;
        //            break;
        //        case LoggingVerbosity.Error:
        //            ((log4net.Repository.Hierarchy.Logger)_iLog.Logger).Level = log4net.Core.Level.Error;
        //            break;
        //        case LoggingVerbosity.Fatal:
        //            ((log4net.Repository.Hierarchy.Logger)_iLog.Logger).Level = log4net.Core.Level.Fatal;
        //            break;
        //    }
        //}

        public void Debug(object message)
        {
            _iLog.Debug(message);
        }

        public void Debug(object message, Exception exception)
        {
            _iLog.Debug(message, exception);
        }

        public void DebugFormat(string format, object arg0)
        {
            _iLog.DebugFormat(format, arg0);
        }

        public void DebugFormat(string format, params object[] args)
        {
            _iLog.DebugFormat(format, args);
        }

        public void DebugFormat(string format, object arg0, object arg1)
        {
            _iLog.DebugFormat(format, arg0, arg1);
        }

        public void DebugFormat(string format, object arg0, object arg1, object arg2)
        {
            _iLog.DebugFormat(format, arg0, arg1, arg2);
        }

        public void DebugFormat(IFormatProvider provider, string format, params object[] args)
        {
            _iLog.DebugFormat(provider, format, args);
        }

        public void Info(object message)
        {
            _iLog.Info(message);
        }

        public void Info(object message, Exception exception)
        {
            _iLog.Info(message, exception);
        }

        public void InfoFormat(string format, params object[] args)
        {
            _iLog.InfoFormat(format, args);
        }

        public void InfoFormat(string format, object arg0)
        {
            _iLog.InfoFormat(format, arg0);
        }

        public void InfoFormat(string format, object arg0, object arg1)
        {
            _iLog.InfoFormat(format, arg0, arg1);
        }

        public void InfoFormat(string format, object arg0, object arg1, object arg2)
        {
            _iLog.InfoFormat(format, arg0, arg1, arg2);
        }

        public void InfoFormat(IFormatProvider provider, string format, params object[] args)
        {
            _iLog.InfoFormat(provider, format, args);
        }

        public void Warn(object message)
        {
            _iLog.Warn(message);
        }

        public void Warn(object message, Exception exception)
        {
            _iLog.Warn(message, exception);
        }

        public void WarnFormat(string format, params object[] args)
        {
            _iLog.WarnFormat(format, args);
        }

        public void WarnFormat(string format, object arg0)
        {
            _iLog.WarnFormat(format, arg0);
        }

        public void WarnFormat(string format, object arg0, object arg1)
        {
            _iLog.WarnFormat(format, arg0, arg1);
        }

        public void WarnFormat(string format, object arg0, object arg1, object arg2)
        {
            _iLog.WarnFormat(format, arg0, arg1, arg2);
        }

        public void WarnFormat(IFormatProvider provider, string format, params object[] args)
        {
            _iLog.WarnFormat(provider, format, args);
        }

        public void Error(object message)
        {
            _iLog.Error(message);
        }

        public void Error(object message, Exception exception)
        {
            _iLog.Error(message, exception);
        }

        public void ErrorFormat(string format, params object[] args)
        {
            _iLog.ErrorFormat(format, args);
        }

        public void ErrorFormat(string format, object arg0)
        {
            _iLog.ErrorFormat(format, arg0);
        }

        public void ErrorFormat(string format, object arg0, object arg1)
        {
            _iLog.ErrorFormat(format, arg0, arg1);
        }

        public void ErrorFormat(string format, object arg0, object arg1, object arg2)
        {
            _iLog.ErrorFormat(format, arg0, arg1, arg2);
        }

        public void ErrorFormat(IFormatProvider provider, string format, params object[] args)
        {
            _iLog.ErrorFormat(provider, format, args);
        }

        public void Fatal(object message)
        {
            _iLog.Fatal(message);
        }

        public void Fatal(object message, Exception exception)
        {
            _iLog.Fatal(message, exception);
        }

        public void FatalFormat(string format, params object[] args)
        {
            _iLog.FatalFormat(format, args);
        }

        public void FatalFormat(string format, object arg0)
        {
            _iLog.FatalFormat(format, arg0);
        }

        public void FatalFormat(string format, object arg0, object arg1)
        {
            _iLog.FatalFormat(format, arg0, arg1);
        }

        public void FatalFormat(string format, object arg0, object arg1, object arg2)
        {
            _iLog.FatalFormat(format, arg0, arg1, arg2);
        }

        public void FatalFormat(IFormatProvider provider, string format, params object[] args)
        {
            _iLog.FatalFormat(provider, format, args);
        }
    }
}