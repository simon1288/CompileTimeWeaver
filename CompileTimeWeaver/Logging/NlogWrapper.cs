using System;
using NLog;

namespace CompileTimeWeaver.Logging
{
    internal class NlogWrapper : ILogger
    {
        protected readonly NLog.Logger _logger;

        public NlogWrapper(Type type)
        {
            _logger = NLog.LogManager.GetLogger(type.FullName);
        }

        public bool IsDebugEnabled
        {
            get { return _logger.IsDebugEnabled; }
        }

        public bool IsErrorEnabled
        {
            get { return _logger.IsErrorEnabled; }
        }

        public bool IsFatalEnabled
        {
            get { return IsFatalEnabled; }
        }

        public bool IsInfoEnabled
        {
            get { return IsInfoEnabled; }
        }

        public bool IsWarnEnabled
        {
            get { return IsWarnEnabled; }
        }

        //public void SetVerbosity(LoggingVerbosity verbosity)
        //{
        //    var logLevel = LogLevel.Off;
        //    switch (verbosity)
        //    {
        //        case LoggingVerbosity.Debug:
        //            logLevel = LogLevel.Debug;
        //            break;
        //        case LoggingVerbosity.Info:
        //            logLevel = LogLevel.Info;
        //            break;
        //        case LoggingVerbosity.Warn:
        //            logLevel = LogLevel.Warn;
        //            break;
        //        case LoggingVerbosity.Error:
        //            logLevel = LogLevel.Error;
        //            break;
        //        case LoggingVerbosity.Fatal:
        //            logLevel = LogLevel.Fatal;
        //            break;
        //    }
        //    foreach (var rule in NLog.LogManager.Configuration.LoggingRules)
        //    {
        //        rule.EnableLoggingForLevel(logLevel);
        //    }

        //    NLog.LogManager.ReconfigExistingLoggers();
        //}

        public void Debug(object message)
        {
            _logger.Debug(message);
        }

        public void Debug(object message, Exception exception)
        {
            //nlog v2 - v3
            //logger.Debug(message.ToString(), exception);
            //nlog v4
            _logger.Debug(exception, message.ToString());
        }

        public void DebugFormat(string format, params object[] args)
        {
            _logger.Debug(format, args);
        }

        public void DebugFormat(string format, object arg0)
        {
            _logger.Debug(format, arg0);
        }
        
        public void DebugFormat(string format, object arg0, object arg1)
        {
            _logger.Debug(format, arg0, arg1);
        }

        public void DebugFormat(string format, object arg0, object arg1, object arg2)
        {
            _logger.Debug(format, arg0, arg1, arg2);
        }

        public void DebugFormat(IFormatProvider provider, string format, params object[] args)
        {
            _logger.Debug(provider, format, args);
        }

        public void Info(object message)
        {
            _logger.Info(message);
        }

        public void Info(object message, Exception exception)
        {
            //nlog v2 - v3
            //logger.Info(message.ToString(), exception);
            //nlog v4
            _logger.Info(exception, message.ToString());
        }

        public void InfoFormat(string format, params object[] args)
        {
            _logger.Info(format, args);
        }

        public void InfoFormat(string format, object arg0)
        {
            _logger.Info(format, arg0);
        }

        public void InfoFormat(string format, object arg0, object arg1)
        {
            _logger.Info(format, arg0, arg1);
        }

        public void InfoFormat(string format, object arg0, object arg1, object arg2)
        {
            _logger.Info(format, arg0, arg1, arg2);
        }

        public void InfoFormat(IFormatProvider provider, string format, params object[] args)
        {
            _logger.Info(provider, format, args);
        }

        public void Warn(object message)
        {
            _logger.Warn(message);
        }

        public void Warn(object message, Exception exception)
        {
            //nlog v2 - v3
            //logger.Warn(message.ToString(), exception);
            //nlog v4
            _logger.Warn(exception, message.ToString());
        }

        public void WarnFormat(string format, params object[] args)
        {
            _logger.Warn(format, args);
        }

        public void WarnFormat(string format, object arg0)
        {
            _logger.Warn(format, arg0);
        }

        public void WarnFormat(string format, object arg0, object arg1)
        {
            _logger.Warn(format, arg0, arg1);
        }

        public void WarnFormat(string format, object arg0, object arg1, object arg2)
        {
            _logger.Warn(format, arg0, arg1, arg2);
        }

        public void WarnFormat(IFormatProvider provider, string format, params object[] args)
        {
            _logger.Warn(provider, format, args);
        }

        public void Error(object message)
        {
            _logger.Error(message);
        }

        public void Error(object message, Exception exception)
        {
            //nlog v2 - v3
            //logger.Error(message.ToString(), exception);
            //nlog v4
            _logger.Error(exception, message.ToString());
        }

        public void ErrorFormat(string format, params object[] args)
        {
            _logger.Error(format, args);
        }

        public void ErrorFormat(string format, object arg0)
        {
            _logger.Error(format, arg0);
        }

        public void ErrorFormat(string format, object arg0, object arg1)
        {
            _logger.Error(format, arg0, arg1);
        }

        public void ErrorFormat(string format, object arg0, object arg1, object arg2)
        {
            _logger.Error(format, arg0, arg1, arg2);
        }

        public void ErrorFormat(IFormatProvider provider, string format, params object[] args)
        {
            _logger.Error(provider, format, args);
        }

        public void Fatal(object message)
        {
            _logger.Fatal(message);
        }

        public void Fatal(object message, Exception exception)
        {
            //nlog v2 - v3
            //logger.Fatal(message.ToString(), exception);
            //nlog v4
            _logger.Fatal(exception, message.ToString());
        }

        public void FatalFormat(string format, params object[] args)
        {
            _logger.Fatal(format, args);
        }

        public void FatalFormat(string format, object arg0)
        {
            _logger.Fatal(format, arg0);
        }

        public void FatalFormat(string format, object arg0, object arg1)
        {
            _logger.Fatal(format, arg0, arg1);
        }

        public void FatalFormat(string format, object arg0, object arg1, object arg2)
        {
            _logger.Fatal(format, arg0, arg1, arg2);
        }

        public void FatalFormat(IFormatProvider provider, string format, params object[] args)
        {
            _logger.Fatal(provider, format, args);
        }
    }
}