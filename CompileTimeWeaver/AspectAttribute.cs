﻿using System;
using System.Threading.Tasks;

namespace CompileTimeWeaver
{
    //[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Interface | AttributeTargets.Method | AttributeTargets.Constructor, AllowMultiple = true)]
    //public class AspectAttribute : Attribute
    //{
    //    /// <summary>
    //    /// The type has to implement IAdvice
    //    /// </summary>
    //    public Type AdviceType { get; set; }

    //    /// <summary>
    //    /// The constructor args of AdviceType, each arg format is "name:value"
    //    /// </summary>
    //    public string[] AdviceArgs { get; set; }

    //    public AspectAttribute()
    //    {
    //    }
    //}

    //Uncomment it when C# support generic attribute later.
    //[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Interface | AttributeTargets.Method | AttributeTargets.Constructor, AllowMultiple = true)]
    //public class AspectAttribute<T> : AspectAttribute
    //    where T : IAdvice
    //{
    //    public AspectAttribute()
    //        :base(typeof(T))
    //    {
    //    }
    //}
}
