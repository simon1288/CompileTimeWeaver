﻿using System;
using System.Threading.Tasks;

namespace CompileTimeWeaver
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Interface | AttributeTargets.Method | AttributeTargets.Constructor, AllowMultiple = true)]
    public abstract class AdviceAttribute : Attribute, IAdvice
    {
        public bool ExcludeConstructors { get; set; }
        public bool ExcludeAutoProperties { get; set; }
        public bool ExcludeGetters { get; set; }
        public bool ExcludeSetters { get; set; }
        public bool ExcludeStatic { get; set; }

        public int Order { get; set; }

        /// <summary>
        /// Intercepts the method.
        /// </summary>
        /// <param name="invocation">The invocation.</param>
        public abstract object Advise(IInvocation invocation);

        public abstract Task<object> AdviseAsync(IInvocation invocation);
    }
}
