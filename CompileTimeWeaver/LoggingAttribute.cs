﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using CompileTimeWeaver.Logging;

namespace CompileTimeWeaver
{
    public class LoggingAttribute : AdviceAttribute
    {
        //private static JavaScriptSerializer _jsonSerializer = new JavaScriptSerializer();

        public bool LogParameters { get; set; } = true;
        public bool Profiling { get; set; } = true;

        public override object Advise(IInvocation invocation)
        {
            var ignoreLogging = invocation.Method.IsDefined(typeof(IgnoreLoggingAttribute), false);
            if (ignoreLogging)
                return invocation.Proceed();

            var methodName = invocation.Method.Name;
            var logger = LogManager.GetLogger(invocation.Method.DeclaringType);

            if (LogParameters)
            {
                var parameters = new StringBuilder(1024);
                ParameterInfo[] parameterInfo = invocation.Method.GetParameters();
                for (int i = 0; i < parameterInfo.Length; i++)
                {
                    if (!parameterInfo[i].IsDefined(typeof(IgnoreLoggingAttribute), false))
                        parameters.AppendFormat($"{Environment.NewLine}\t{parameterInfo[i].Name}: {invocation.Args[i]}");
                }
                logger.Info($"{methodName}...{parameters}");
                parameters.Clear();
            }

            if (Profiling)
            {
                var stopWatch = new Stopwatch();
                stopWatch.Start();
                try
                {
                    return invocation.Proceed();
                }
                finally
                {
                    stopWatch.Stop();
                    logger.Info($"{methodName} completed in {stopWatch.ElapsedMilliseconds} milliseconds.");
                }
            }
            else
            {
                return invocation.Proceed();
            }
        }

        public override async Task<object> AdviseAsync(IInvocation invocation)
        {
            var ignoreLogging = invocation.Method.IsDefined(typeof(IgnoreLoggingAttribute), false);
            if (ignoreLogging)
                return await invocation.ProceedAsync().ConfigureAwait(false);

            var methodName = invocation.Method.Name;
            var logger = LogManager.GetLogger(invocation.Method.DeclaringType);

            if (LogParameters)
            {
                var parameters = new StringBuilder(1024);
                ParameterInfo[] parameterInfo = invocation.Method.GetParameters();
                for (int i = 0; i < parameterInfo.Length; i++)
                {
                    if (!parameterInfo[i].IsDefined(typeof(IgnoreLoggingAttribute), false))
                        parameters.Append($"{Environment.NewLine}\t{parameterInfo[i].Name}: {invocation.Args[i]}");
                }
                logger.Info($"{methodName}...{parameters}");
                parameters.Clear();
            }

            if (Profiling)
            {
                var stopWatch = new Stopwatch();
                stopWatch.Start();
                try
                {
                    return await invocation.ProceedAsync().ConfigureAwait(false);
                }
                finally
                {
                    stopWatch.Stop();
                    logger.Info($"{methodName} completed in {stopWatch.ElapsedMilliseconds} milliseconds.");
                }
            }
            else
            {
                return await invocation.ProceedAsync().ConfigureAwait(false);
            }
        }
    }


    [AttributeUsage(AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Parameter)]
    public class IgnoreLoggingAttribute : Attribute
    {
    }
}
