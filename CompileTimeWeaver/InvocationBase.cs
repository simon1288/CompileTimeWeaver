﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace CompileTimeWeaver
{
    /// <summary>
    /// MethodWeaver generates a subclass of InvocationBase for each target method to be weaved.
    /// </summary>
    public abstract class InvocationBase : IInvocation
    {
        private static readonly ConcurrentDictionary<Type, IEnumerable<IAdvice>> _enumeratorCache = new ConcurrentDictionary<Type, IEnumerable<IAdvice>>();
        private readonly IEnumerator<IAdvice> _enumerator;

        #region constructors

        protected InvocationBase(object instance, MethodBase method, object[] args)
        {
            Debug.Assert(method != null);
            Debug.Assert(args != null);

            Instance = instance;
            Method = method;
            Args = args;

            _enumerator = _enumeratorCache.GetOrAdd(this.GetType(), x => CreateAdviceChain(method)).GetEnumerator();
        }

        private static IEnumerable<IAdvice> CreateAdviceChain(MethodBase method)
        {
            Debug.WriteLine($"Creating advice chain for {method.Name}...");

            // find all advices on the method level and class level
            var advices = method.DeclaringType.GetCustomAttributes(typeof(IAdvice), true)
                                .Concat(method.GetCustomAttributes(typeof(IAdvice), false))
                                .Cast<IAdvice>();

            // filter advices
            var adviceChain = advices.Where(x =>
            {
                if (x.ExcludeConstructors && IsConstructor(method))
                    return false;
                if (x.ExcludeAutoProperties && IsAutoProperty(method))
                    return false;
                if (x.ExcludeStatic && method.IsStatic)
                    return false;
                if (x.ExcludeGetters && method.IsSpecialName && method.Name.StartsWith("get_"))
                    return false;
                if (x.ExcludeSetters && method.IsSpecialName && method.Name.StartsWith("set_"))
                    return false;
                return true;
            }).ToArray();

            //assign order in each type group
            var groupByType = from x in adviceChain
                              group x by x.GetType()
                              into grp
                              select grp.ToArray();
            foreach (var grp in groupByType)
            {
                for (int i = 0; i < grp.Length; i++)
                {
                    grp[i].Order = i;
                }
            }

            return adviceChain;
        }

        /*
        private static IEnumerable<IAdvice> CreateAdviceChain(MethodBase method)
        {
            Debug.WriteLine($"Creating advice chain for {method.Name}...");

            // find all advices on the method level and class level
            var advices = method.DeclaringType.GetCustomAttributes(true)
                                .Concat(method.GetCustomAttributes(false))
                                .Where(x => x is IAdvice || x is AspectAttribute)
                                .Select(x =>
                                {
                                    if (x is IAdvice)
                                        return (IAdvice)x;

                                            //x must be AspectAttribute;
                                            var attr = (AspectAttribute)x;
                                    var adviceType = attr.AdviceType;
                                    if (!typeof(IAdvice).IsAssignableFrom(adviceType))
                                        throw new ArgumentException($"AspectAttribute.AdivceType {adviceType.FullName} must implement IAdvice");
#if DEBUG
                                            if (attr.AdviceArgs != null)
                                    {
                                        foreach (var arg in attr.AdviceArgs)
                                        {
                                            Debug.WriteLine(arg);
                                        }
                                    }
#endif
                                            return attr.AdviceArgs == null
                                            ? (IAdvice)Activator.CreateInstance(adviceType)
                                            : (IAdvice)Activator.CreateInstance(adviceType, new object[] { attr.AdviceArgs });
                                });

            // filter advices
            var adviceChain = advices.Where(x =>
                                    {
                                        if (x.ExcludeConstructors && IsConstructor(method))
                                            return false;
                                        if (x.ExcludeAutoProperties && IsAutoProperty(method))
                                            return false;
                                        if (x.ExcludeStatic && method.IsStatic)
                                            return false;
                                        if (x.ExcludeGetters && method.IsSpecialName && method.Name.StartsWith("get_"))
                                            return false;
                                        if (x.ExcludeSetters && method.IsSpecialName && method.Name.StartsWith("set_"))
                                            return false;
                                        return true;
                                    }).ToArray();

            //assign order in each type group
            var groupByType = from x in adviceChain
                              group x by x.GetType()
                              into grp
                              select grp.ToArray();
            foreach (var grp in groupByType)
            {
                for (int i = 0; i < grp.Length; i++)
                {
                    grp[i].Order = i;
                }
            }

            return adviceChain;
        }
        */

        #endregion

        #region IInvocation

        public object Instance { get; protected set; }
        public MethodBase Method { get; protected set; }
        public object[] Args { get; protected set; }
        public object ReturnValue { get; set; }

        /// <summary>
        /// Each advice's Advise() method calls this method to pass processing down to next advice, and finally to target method.
        /// </summary>
        /// <returns>The result of target method.</returns>
        public object Proceed()
        {
            if (_enumerator.MoveNext())
            {
                ReturnValue = _enumerator.Current.Advise(this);
            }
            else
            {
                string invocationName = this.GetType().Name;
                CallContext.LogicSetData(invocationName, true);
                try
                {
                    ReturnValue = Invoke();
                }
                finally
                {
                    CallContext.FreeNamedDataSlot(invocationName);
                }
            }

            return ReturnValue;
        }

        /// <summary>
        /// Each advice's AdviseAsync() method calls this method to pass processing down to next advice, and finally to target method.
        /// </summary>
        /// <returns>The result of target method.</returns>
        public async Task<object> ProceedAsync()
        {
            if (_enumerator.MoveNext())
            {
                ReturnValue = await _enumerator.Current.AdviseAsync(this).ConfigureAwait(false);
            }
            else
            {
                string invocationName = this.GetType().Name;
                CallContext.LogicSetData(invocationName, true);
                try
                {
                    ReturnValue = await InvokeAsync().ConfigureAwait(false);
                }
                finally
                {
                    CallContext.FreeNamedDataSlot(invocationName);
                }
            }
            return ReturnValue;
        }

        #endregion

        /// <summary>
        /// Method weaver implements this method in auto-generated derived Invocation class to call target method,
        /// user should not call this method.
        /// </summary>
        /// <returns>The result of target method.</returns>
        protected virtual object Invoke()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Method weaver implements this method in auto-generated derived Invocation class to call target method,
        /// user should not call this method.
        /// </summary>
        /// <returns>The result of target method.</returns>
        protected virtual Task<object> InvokeAsync()
        {
            throw new NotImplementedException();
        }

        public static bool IsAutoProperty(MethodBase method)
        {
            if (method.IsSpecialName && method.Name.Length > 4 && (method.Name.StartsWith("get_") || method.Name.StartsWith("set_")))
            {
                //if (method.DeclaringType == null)
                //    return false;
                var propName = method.Name.Substring(4);
                return method.DeclaringType.GetField($"<{propName}>k__BackingField", BindingFlags.NonPublic | BindingFlags.Instance) != null;                
            }
            return false;
        }

        public static bool IsConstructor(MethodBase method)
        {
            return method.IsConstructor || method.Name.StartsWith("<.ctor>_");
        }

        //private static bool IsAsyncMethod(MethodBase methodInfo)
        //{
        //    return methodInfo.IsDefined(typeof (AsyncStateMachineAttribute));
        //}

        //public static MethodBase GetCurrentMethod(Type type, [CallerMemberName] string callerName = "")
        //{
        //    return type.GetMethod(callerName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
        //}

        public static async Task<T> Convert1<T>(Task<object> task)
        {
            return (T)await task.ConfigureAwait(false);
        }

        public static async Task<object> Convert2<T>(Task<T> task)
        {
            return await task.ConfigureAwait(false);
        }

        public static async Task<object> Convert3(Task task)
        {
            await task.ConfigureAwait(false);
            return null;
        }

        public static async Task Convert4(Task<object> task)
        {
            await task.ConfigureAwait(false);
        }

        public static T GetDefault<T>()
        {
            return default(T);
        }
    }
}
