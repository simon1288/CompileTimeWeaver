using System;

namespace CompileTimeWeaver
{
    public interface IExceptionHandler
    {
        /// <summary>
        /// Handle exception e.
        /// </summary>
        /// <param name="e"></param>
        /// <returns>Rethrow when return value is true.</returns>
        bool HandleException(Exception e);
    }
}
