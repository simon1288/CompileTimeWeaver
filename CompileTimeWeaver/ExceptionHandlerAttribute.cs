﻿using System;
using System.Reflection;
using System.Threading.Tasks;

namespace CompileTimeWeaver
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Interface)]
    public class ExceptionHandlerAttribute : AdviceAttribute
    {
        public ExceptionHandlerAttribute(Type exceptionType, Type handlerType)
        {
            if (!typeof(IExceptionHandler).IsAssignableFrom(handlerType))
                throw new ArgumentException($"handlerType {handlerType.FullName} must implement IExceptionHandler");
            if (handlerType.IsAbstract && handlerType.IsSealed)
                throw new ArgumentException($"handlerType {handlerType.FullName} cannot be static.");
            if (handlerType.IsAbstract)
                throw new ArgumentException($"handlerType {handlerType.FullName} cannot be abstract.");

            this.ExceptionType = exceptionType;
            this.HandlerType = handlerType;
        }

        public Type ExceptionType { get; set; }
        public Type HandlerType { get; set; }

        public override object Advise(IInvocation invocation)
        {
            try
            {
                return invocation.Proceed();
            }
            catch (Exception e)
            {
                if (e.GetType() == this.ExceptionType)
                {
                    IExceptionHandler exceptionHandler = (IExceptionHandler)Activator.CreateInstance(this.HandlerType);
                    if (!exceptionHandler.HandleException(e))
                    {
                        //don't rethrow if HandleException return false, but I must create a default return value
                        Type returnType = ((MethodInfo)invocation.Method).ReturnType;
                        return returnType.IsValueType && returnType != typeof(void) ? Activator.CreateInstance(returnType) : null;
                    }
                }

                throw;  //rethrow
            }
        }

        public override async Task<object> AdviseAsync(IInvocation invocation)
        {
            try
            {
                return await invocation.ProceedAsync().ConfigureAwait(false);
            }
            catch (Exception e)
            {
                if (e.GetType() == this.ExceptionType)
                {
                    IExceptionHandler exceptionHandler = (IExceptionHandler)Activator.CreateInstance(this.HandlerType);
                    if (!exceptionHandler.HandleException(e))
                    {
                        //don't rethrow if HandleException return false, but I must create a default return value
                        Type returnType = ((MethodInfo)invocation.Method).ReturnType;
                        if (!returnType.IsGenericType) // invoked method is of async Task Fo(...)
                            return null;

                        //invoked method is of async Task<T> Fo(...), get type of T
                        returnType = returnType.GetGenericArguments()[0];
                        return returnType.IsValueType && returnType != typeof(void) ? Activator.CreateInstance(returnType) : null;
                    }
                }

                throw;  //rethrow
            }
        }
    }
}
