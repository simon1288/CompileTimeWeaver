﻿using System.Reflection;
using System.Threading.Tasks;

namespace CompileTimeWeaver
{
    public interface IInvocation
    {
        object Instance { get; }
        MethodBase Method { get; }
        object[] Args { get; }
        object ReturnValue { get; }

        object Proceed();
        Task<object> ProceedAsync();
    }
}
