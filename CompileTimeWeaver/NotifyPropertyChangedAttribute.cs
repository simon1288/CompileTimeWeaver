﻿using System;

namespace CompileTimeWeaver
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Property, Inherited = false)]
    public class NotifyPropertyChangedAttribute : Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Property, Inherited = false)]
    public class IgnoreNotifyPropertyChangedAttribute : Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Property, Inherited = false)]
    public class DeterminedByPropertiesAttribute : Attribute
    {
        public DeterminedByPropertiesAttribute(string parentProperty)
        {
        }

        public DeterminedByPropertiesAttribute(string parentProperty, params string[] otherParentProperties)
        {
        }
    }
}
