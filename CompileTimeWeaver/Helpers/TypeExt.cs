﻿using System;
using System.Linq;

namespace CompileTimeWeaver.Helpers
{
    internal static class TypeExt
    {
        private static readonly Type[] List;

        static TypeExt()
        {
            List = new[]
            {
                typeof (Enum),
                typeof (string),
                typeof (decimal),
                typeof (DateTime),
                typeof (DateTimeOffset),
                typeof (TimeSpan),
                typeof (Guid)
            };
        }

        public static bool IsSimpleType(this Type type)
        {
            return type.IsPrimitive ||
                   List.Contains(type) ||
                   Convert.GetTypeCode(type) != TypeCode.Object ||
                   (type.IsGenericType && type.GetGenericTypeDefinition() == typeof (Nullable<>) && IsSimpleType(type.GetGenericArguments()[0]));
        }
    }
}
