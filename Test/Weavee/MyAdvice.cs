using System;
using System.Diagnostics;
using System.Threading.Tasks;
using CompileTimeWeaver;

namespace Weavee
{
    public class MyAdvice : AdviceAttribute
    {
        public MyAdvice()
        {
        }

        public MyAdvice(string[] args)
        {
            foreach (var arg in args)
            {
                var argParts = arg.Split(':');
                if (argParts.Length != 2)
                    continue;

                if (argParts[0].Equals("ExcludeAutoProperties", StringComparison.OrdinalIgnoreCase))
                {
                    this.ExcludeAutoProperties = bool.Parse(argParts[1]);
                }
                else if (argParts[0].Equals("ExcludeConstructors", StringComparison.OrdinalIgnoreCase))
                {
                    this.ExcludeConstructors = bool.Parse(argParts[1]);
                }
            }
        }

        public override object Advise(IInvocation invocation)
        {
            Trace.WriteLine("Entering " + invocation.Method.Name);
            try
            {
                return invocation.Proceed();
            }
            catch (Exception e)
            {
                Trace.WriteLine("MyAdvice catches an exception: " + e.Message);
                throw;
            }
            finally
            {
                Trace.WriteLine("Leaving " + invocation.Method.Name);
            }
        }

        public override async Task<object> AdviseAsync(IInvocation invocation)
        {
            Trace.WriteLine("Entering async " + invocation.Method.Name);
            try
            {
                return await invocation.ProceedAsync().ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Trace.WriteLine("MyAdvice catches an exception: " + e.Message);
                throw;
            }
            finally
            {
                Trace.WriteLine("Leaving async " + invocation.Method.Name);
            }
        }
    }
}