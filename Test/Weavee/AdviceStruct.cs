﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Weavee
{
    public struct AdviceStruct
    {
        [MyAdvice]
        public AdviceStruct(string a, string b)
        {
            try
            {
                Trace.WriteLine($"Your Code - AdviceStruct.ctor({a}, {b})");
            }
            catch (Exception e)
            {
                Trace.WriteLine(e.Message);
            }
            finally
            {
                Trace.WriteLine($"Your Code - AdviceStruct.ctor({a}, {b}).finally");
            }
        }


        [MyAdvice]
        public int AddWithOutArg(int x, short y, string dumy, out int z)
        {
            Trace.WriteLine("Your Code - int AdviceStruct.AddWithOutArg(int x, short y, string dumy, out int z)");
            z = x + y;
            return x + y;
        }

        [MyAdvice]
        public int AddWithRefArg(int x, short y, string dumy, ref int z)
        {
            Trace.WriteLine("Your Code - int AdviceStruct.AddWithRefArg(int x, short y, string dumy, ref int z)");
            z = x + y + z;
            return x + y;
        }

        [MyAdvice]
        public async Task HelloAsyncNoReturn(int x, short y, string dumy)
        {
            Trace.WriteLine("Your Code - async Task AdviceStruct.HelloAsyncNoReturn(int x, short y, string dumy)");
            await Task.Delay(1);
        }

        [MyAdvice]
        public void AddWithRefArgNotReturn(int x, short y, string dumy, ref int z)
        {
            Trace.WriteLine("Your Code - void AdviceStruct.AddWithRefArgNotReturn(int x, short y, string dumy, ref int z)");
            z = x + y + z;
        }

        [MyAdvice]
        public async Task<int> AddAsyncWithReturn(int x, short y, string dumy)
        {
            Trace.WriteLine("Your Code - async Task<int> AdviceStruct.AddAsyncWithReturn(int x, short y, string dumy)");
            await Task.Delay(1);
            return x + y;
        }

        public int StaticAddWithRefArgCaller(int x, short y, string dumy, ref int z)
        {
            return StaticAddWithRefArg(x, y, dumy, ref z);
        }

        [MyAdvice]
        public static int StaticAddWithRefArg(int x, short y, string dumy, ref int z)
        {
            Trace.WriteLine("Your Code - int AdviceStruct.StaticAddWithRefArg(int x, short y, string dumy, ref int z)");
            z = x + y + z;
            return x + y;
        }
    }
}
