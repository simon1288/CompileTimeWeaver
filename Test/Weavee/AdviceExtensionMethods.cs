﻿using System.Diagnostics;
using System.Globalization;

namespace Weavee
{
    public class AdviceExtensionMethods
    {
        public string ReturnsString()
        {
            return "ToTitleCase2 is extension method.".ToTitleCase2();
        }
    }

    public static class StringExtensions2
    {
        [MyAdvice]
        public static string ToTitleCase2(this string value)
        {
            Trace.WriteLine(value);
            return value;
            //return new CultureInfo("en-GB", false).TextInfo.ToTitleCase(value);
        }
    }
}