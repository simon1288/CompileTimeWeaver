﻿using System.Diagnostics;

namespace Weavee
{
    public class AdviceAbstractMethods : AbstractBaseClass
    {
        public override void AbstractMethod()
        {
           Trace.WriteLine("AdviceAbstractMethods.AbstractMethod");
        }
    }

    public abstract class AbstractBaseClass
    {
        [MyAdvice]
        public abstract void AbstractMethod();
    }
}