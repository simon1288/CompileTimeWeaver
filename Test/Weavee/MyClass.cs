﻿using System.Diagnostics;
using System.Threading.Tasks;
using CompileTimeWeaver;

namespace Weavee
{
    public class MyClass<TResult>
    {
        public TResult Method1<TArg>(TArg t)
        {
            var a = typeof(TResult);
            Trace.WriteLine(a);
            return default(TResult);
        }

        private sealed class Method1Invocation<TArg>
        {
            private readonly object _instance;
            private readonly object[] _args;

            public Method1Invocation(MyClass<TResult> instance, object[] args)
            {
                _instance = instance;
                _args = args;
            }

            public object Invoke()
            {
                TResult result = ((MyClass<TResult>)_instance).Method1<TArg>((TArg)_args[0]);
                return result;
            }
        }

        public async Task<TResult> MethodAsync<TArg>(TArg t)
        {
            var a = typeof(TResult);
            Trace.WriteLine(a);
            await Task.Delay(10);
            return default(TResult);
        }

        private sealed class MethodAsyncInvocation<TArg>
        {
            private readonly object _instance;
            private readonly object[] _args;

            public MethodAsyncInvocation(MyClass<TResult> instance, object[] args)
            {
                _instance = instance;
                _args = args;
            }

            public object Invoke()
            {
                var result = ((MyClass<TResult>)_instance).MethodAsync<TArg>((TArg)_args[0]);
                return InvocationBase.Convert2<TResult>(result);
            }
        }
    }
}
