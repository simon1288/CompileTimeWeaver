﻿namespace Weavee
{
    public struct MyStruct<T>
    {
        public void F1()
        {
            F2(this);
        }

        public void F2(MyStruct<T> p)
        {
        }
    }
}
