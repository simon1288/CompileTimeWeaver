﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Weavee
{
    [MyAdvice]
    public class AdviceSample
    {
        [MyAdvice]
        public AdviceSample()
        {
            try
            {
                Trace.WriteLine("Your Code - Sample.ctor");
            }
            catch (Exception e)
            {
                Trace.WriteLine(e.Message);
            }
            finally
            {
                Trace.WriteLine("Your Code - Sample.ctor.finally");
            }
        }

        [MyAdvice]
        public AdviceSample(string a, string b)
        {
            try
            {
                Trace.WriteLine($"Your Code - Sample.ctor({a}, {b})");
            }
            catch (Exception e)
            {
                Trace.WriteLine(e.Message);
            }
            finally
            {
                Trace.WriteLine($"Your Code - Sample.ctor({a}, {b}).finally");
            }
        }

        public AdviceSample CallCtorWithArgs(string a, string b)
        {
            return new AdviceSample(a, b);
        }


        [MyAdvice]
        public int AddWithOutArg(int x, short y, string dumy, out int z)
        {
            Trace.WriteLine("Your Code - int AddWithOutArg(int x, short y, string dumy, out int z)");
            z = x + y;
            return x + y;
        }

        [MyAdvice]
        public int AddWithRefArg(int x, short y, string dumy, ref int z)
        {
            Trace.WriteLine("Your Code - int AddWithRefArg(int x, short y, string dumy, ref int z)");
            z = x + y + z;
            return x + y;
        }

        public int StaticAddWithRefArgCaller(int x, short y, string dumy, ref int z)
        {
            return StaticAddWithRefArg(x, y, dumy, ref z);
        }

        [MyAdvice]
        public static int StaticAddWithRefArg(int x, short y, string dumy, ref int z)
        {
            Trace.WriteLine("Your Code - int StaticAddWithRefArg(int x, short y, string dumy, ref int z)");
            z = x + y + z;
            return x + y;
        }

        [MyAdvice]
        public void AddWithRefArgNotReturn(int x, short y, string dumy, ref int z)
        {
            Trace.WriteLine("Your Code - void AddWithRefArgNotReturn(int x, short y, string dumy, ref int z)");
            z = x + y + z;
        }

        [MyAdvice]
        public async Task<int> AddAsyncWithReturn(int x, short y, string dumy)
        {
            Trace.WriteLine("Your Code - async Task<int> AddAsyncWithReturn(int x, short y, string dumy)");
            await Task.Delay(1);
            return x + y;
        }

        [MyAdvice]
        public async Task HelloAsyncNoReturn(int x, short y, string dumy)
        {
            Trace.WriteLine("Your Code - async Task HelloAsyncNoReturn(int x, short y, string dumy)");
            await Task.Delay(1);
        }
    }
}
