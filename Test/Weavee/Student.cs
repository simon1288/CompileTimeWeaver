﻿using CompileTimeWeaver;

namespace Weavee
{
    [NotifyPropertyChanged]
    public class Student : Person
    {
        public string School { get; set; }
    }
}