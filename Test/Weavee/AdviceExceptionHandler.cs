﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using CompileTimeWeaver;

namespace Weavee
{
    public class AdviceExceptionHandler
    {
        [ExceptionHandler(typeof(ApplicationException), typeof(ApplicationExceptionHandler))]
        public void Hello()
        {
            throw new ApplicationException();
        }

        [ExceptionHandler(typeof(ApplicationException), typeof(ApplicationExceptionHandler))]
        public int HelloReturnValueType()
        {
            throw new ApplicationException();
        }

        [ExceptionHandler(typeof(ApplicationException), typeof(ApplicationExceptionHandler))]
        public string HelloReturnRefType()
        {
            throw new ApplicationException();
        }

        [ExceptionHandler(typeof(ApplicationException), typeof(ApplicationExceptionHandler))]
        public async Task HelloAsync()
        {
            await Task.Delay(1).ConfigureAwait(false);
            throw new ApplicationException();
        }

        [ExceptionHandler(typeof(ApplicationException), typeof(ApplicationExceptionHandler))]
        public async Task<int> HelloReturnValueTypeAsync()
        {
            await Task.Delay(1).ConfigureAwait(false);
            throw new ApplicationException();
        }

        [ExceptionHandler(typeof(ApplicationException), typeof(ApplicationExceptionHandler))]
        public async Task<string> HelloReturnRefTypeAsync()
        {
            await Task.Delay(1).ConfigureAwait(false);
            throw new ApplicationException();
        }
    }

    internal class ApplicationExceptionHandler : IExceptionHandler
    {
        public bool HandleException(Exception e)
        {
            Trace.WriteLine(e.GetType() + " is caught");
            return false;
        }
    }
}
