﻿using System.Diagnostics;
using System.Reflection;
using System.Threading.Tasks;
using CompileTimeWeaver;

namespace Weavee
{
    public class ModelOfGeneratedCode<U>
    {
        public ModelOfGeneratedCode(string a, string b)
        {
        }

        public ModelOfGeneratedCode()
        {
            Ctor_1();
        }

        private void Ctor_1()
        {
            if (CallContext.LogicalGetData("Ctor_1_Invocation_1") == null)
            {
                var args = new object[] { };
                var invocation = new Ctor_1_Invocation_1(this, MethodBase.GetCurrentMethod(), args);
                invocation.Proceed();
                return;
            }

            Trace.WriteLine("Your Code - ModelOfGeneratedCode.ctor");
        }

        private sealed class Ctor_1_Invocation_1 : InvocationBase
        {
            public Ctor_1_Invocation_1(object instance, MethodBase method, object[] args) : base(instance, method, args)
            {
            }

            protected override object Invoke()
            {
                ((ModelOfGeneratedCode<U>)Instance).Ctor_1();
                return null;
            }
        }

        public static int StaticAddWithRefArg(int x, short y, string dumy, ref int z)
        {
            if (CallContext.LogicalGetData("StaticAddWithRefArg_Invocation_1") == null)
            {
                var args = new object[] { x, y, dumy, default(int) };
                var invocation = new StaticAddWithRefArg_Invocation_1(null, MethodBase.GetCurrentMethod(), args);
                var result = (int)invocation.Proceed();
                z = (int)args[3];
                return result;
            }

            Trace.WriteLine("Your Code - int StaticAddWithRefArg(int x, short y, string dumy, ref int z)");
            z = x + y + z;
            return x + y;
        }

        private sealed class StaticAddWithRefArg_Invocation_1 : InvocationBase
        {
            public StaticAddWithRefArg_Invocation_1(object instance, MethodBase method, object[] args) : base(instance, method, args)
            {
            }

            protected override object Invoke()
            {
                var args1 = Args;
                int z = (int)args1[3];
                var result = ModelOfGeneratedCode<U>.StaticAddWithRefArg((int)args1[0], (short)args1[1], (string)args1[2], ref z);
                args1[3] = z;
                return result;
            }
        }

        //[SampleAdvice]
        public int Add<MT>(int x, short y, string dumy, out int z, MT t)
        {
            if (CallContext.LogicalGetData("Add_Invocation_1`1") == null)
            {
                var args = new object[] { x, y, dumy, default(int), t };
                var invocation = new Add_Invocation_1<MT>(this, MethodBase.GetCurrentMethod(), args);
                var result = (int)invocation.Proceed();
                z = (int)args[3];
                return result;
            }

            Trace.WriteLine("Your Code - int Add(int x, short y, string dummy, out int z)");
            z = x + y;
            return x + y;
        }

        private sealed class Add_Invocation_1<MT> : InvocationBase
        {
            public Add_Invocation_1(object instance, MethodBase method, object[] args) : base(instance, method, args)
            {
            }

            protected override object Invoke()
            {
                var args1 = Args;
                int z = (int)args1[3];
                var result = ((ModelOfGeneratedCode<U>)Instance).Add((int)args1[0], (short)args1[1], (string)args1[2], out z, (MT)args1[4]);
                args1[3] = z;
                return result;
            }
        }

        //[SampleAdvice]
        public Task<int> AddAsync(int x, int y)
        {
            if (CallContext.LogicalGetData("AddAsync_Invocation_1") == null)
            {
                var invocation = new AddAsync_Invocation_1(this, MethodBase.GetCurrentMethod(), new object[] { x, y });
                return InvocationBase.Convert1<int>(invocation.ProceedAsync());
            }

            Trace.WriteLine("Your Code");
            //await Task.Delay(1);
            return Task.FromResult(x + y);
        }

        private sealed class AddAsync_Invocation_1 : InvocationBase
        {
            public AddAsync_Invocation_1(object instance, MethodBase method, object[] args) : base(instance, method, args)
            {
            }

            protected override Task<object> InvokeAsync()
            {
                return Convert2<int>(((ModelOfGeneratedCode<U>)Instance).AddAsync((int)Args[0], (int)Args[1]));
            }
        }

        //[SampleAdvice]
        public Task HelloAsync(int x, int y)
        {
            if (CallContext.LogicalGetData("HelloAsync_Invocation_3") == null)
            {
                var args = new object[] { x, y };
                var invocation = new HelloAsync_Invocation_3(this, MethodBase.GetCurrentMethod(), args);
                return InvocationBase.Convert4(invocation.ProceedAsync());
            }

            Trace.WriteLine("Your Code");
            return Task.Delay(1);
        }

        private sealed class HelloAsync_Invocation_3 : InvocationBase
        {
            public HelloAsync_Invocation_3(object instance, MethodBase method, object[] args) : base(instance, method, args)
            {
            }

            protected override Task<object> InvokeAsync()
            {
                return Convert3(((ModelOfGeneratedCode<U>)Instance).HelloAsync((int)Args[0], (int)Args[1]));
            }
        }

        public int Fabi(int x)
        {
            if (CallContext.LogicalGetData("Fabi_Invocation_1") == null)
            {
                var args = new object[] {x};
                var invocation = new Fabi_Invocation_1(this, MethodBase.GetCurrentMethod(), args);
                var result = (int) invocation.Proceed();
                return result;
            }

            int fabi;
            if (x == 1)
                fabi = 1;
            else if (x == 2)
                fabi = 2;
            else
                fabi = Fabi(x - 2) + Fabi(x - 1);
            Trace.WriteLine($"Fabi({x})={fabi}");
            return fabi;
        }

        private sealed class Fabi_Invocation_1 : InvocationBase
        {
            public Fabi_Invocation_1(object instance, MethodBase method, object[] args) : base(instance, method, args)
            {
            }

            protected override object Invoke()
            {
                var args1 = Args;
                var result = ((ModelOfGeneratedCode<U>)Instance).Fabi((int)args1[0]);
                return result;
            }
        }
    }
}
