//using CompileTimeWeaver;
//using System;
//using System.Diagnostics;
//using System.Threading.Tasks;

//namespace Weavee
//{
//    [Aspect(AdviceType = typeof(MyAdvice), AdviceArgs = new string[] { "ExcludeAutoProperties:false" })]
//    //[MyAdvice(ExcludeAutoProperties = false)]
//    public class TargetAspectGenericType<U>
//    {
//        public TargetAspectGenericType(int x, int y)
//        {
//            try
//            {
//                Trace.WriteLine($"this is TargetAspectGenericType.ctor({x},{y})");
//            }
//            catch (Exception e)
//            {
//                Trace.WriteLine(e);
//            }
//        }

//        public int MyProperty { get; set; }

//        public U GetValue<T>(T value)
//        {
//            Trace.WriteLine("U TargetAspectGenericType.GetValue<T>(T value)");
//            Trace.WriteLine($"{typeof(U).Name} TargetAspectGenericType.GetValue<{typeof(T).Name}>");
//            return default(U);
//        }

//        public async Task<U> GetValueAsync<T>(T value)
//        {
//            Trace.WriteLine("U TargetAspectGenericType.GetValueAsync<T>(T value)");
//            Trace.WriteLine($"{typeof(U).Name} TargetAspectGenericType.GetValueAsync<{typeof(T).Name}>");
//            await Task.Delay(1).ConfigureAwait(false);
//            return default(U);
//        }
//    }
//}