﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using CompileTimeWeaver;

namespace Weavee
{
    [Logging(ExcludeConstructors = true)]
    public class AdviceLoggingTarget
    {
        //private readonly InnerClass _inner;

        public AdviceLoggingTarget()
        {
            Trace.WriteLine("AdviceLoggingTarget.ctor");
            //_inner = new InnerClass();
        }

        public async Task<int> HelloAsync(string arg1, int arg2, [IgnoreLogging] string secret)
        {
            //Trace.WriteLine("InnerClass.HelloAsync1");
            await Task.Delay(6000).ConfigureAwait(false);
            //Trace.WriteLine("InnerClass.HelloAsync2");
            return 0;
        }

        //public async Task<int> HelloAsync()
        //{
        //    try
        //    {
        //        //Trace.WriteLine("AdviceLoggingTarget.HelloAsync");
        //        await _inner.HelloAsync("simonlu", new[] {1, 2, 3}, "password").ConfigureAwait(false);
        //        //Trace.WriteLine("AdviceLoggingTarget.HelloAsync");
        //    }
        //    catch (Exception e)
        //    {
        //        HandleException(e);
        //    }

        //    return -1;
        //}

        //[IgnoreLogging]
        //private void HandleException(Exception e)
        //{
        //    Trace.WriteLine(e.Message);
        //}

        //[Logging(ExcludeConstructors = true)]
        //internal class InnerClass
        //{
        //    public InnerClass()
        //    {
        //        //Trace.WriteLine("InnerClass.ctor");
        //    }

        //    public async Task<int> HelloAsync(string arg1, int[] arg2, [IgnoreLogging]string secret)
        //    {
        //        //Trace.WriteLine("InnerClass.HelloAsync1");
        //        await Task.Delay(6000).ConfigureAwait(false);
        //        //Trace.WriteLine("InnerClass.HelloAsync2");
        //        //throw new Exception("InnerClass.HelloAsync throws exception.");
        //        return 0;
        //    }
        //}
    }
}
