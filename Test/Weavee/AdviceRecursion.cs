﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Weavee
{
    [MyAdvice]
    public class AdviceRecursion
    {
        public int Fabi(int x)
        {
            int fabi;
            if (x == 1)
                fabi = 1;
            else if (x == 2)
                fabi = 2;
            else
                fabi = Fabi(x - 2) + Fabi(x - 1);
            Trace.WriteLine($"Fabi({x})={fabi}");
            return fabi;
        }
    }
}
