﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Threading.Tasks;
using CompileTimeWeaver;

namespace Weavee
{
    public struct NoAdviceStruct<T>
    {
        public NoAdviceStruct(string a, string b)
        {
            ctor_46763000(a, b);
        }

        private void ctor_46763000(string a, string b)
        {
            if (CallContext.LogicalGetData("CtorInvocation") == null)
            {
                new CtorInvocation(this, null, new object[2] {(object) a, (object) b})
                    .Proceed();
            }
            else
            {
                try
                {
                    Trace.WriteLine($"Your Code - Sample.ctor({a}, {b})");
                }
                catch (Exception e)
                {
                    Trace.WriteLine(e.Message);
                }
                finally
                {
                    Trace.WriteLine($"Your Code - Sample.ctor({a}, {b}).finally");
                }
            }
        }

        private sealed class CtorInvocation : InvocationBase
        {
            public CtorInvocation(object instance, MethodBase method, object[] args)
                : base((object) instance, method, args)
            {
            }

            protected override object Invoke()
            {
                ((NoAdviceStruct<T>)Instance).ctor_46763000((string)Args[0],(string)Args[1]);
                return null;
            }
        }

        public static int StaticAddWithRefArg(int x, short y, string dumy, ref int z)
        {
            if (CallContext.LogicalGetData("StaticAddWithRefArg_Invocation_1") == null)
            {
                var args = new object[] { x, y, dumy, default(int) };
                var invocation = new StaticAddWithRefArg_Invocation_1(null, MethodBase.GetCurrentMethod(), args);
                var result = (int)invocation.Proceed();
                z = (int)args[3];
                return result;
            }

            Trace.WriteLine("Your Code - int StaticAddWithRefArg(int x, short y, string dumy, ref int z)");
            z = x + y + z;
            return x + y;
        }

        private sealed class StaticAddWithRefArg_Invocation_1 : InvocationBase
        {
            public StaticAddWithRefArg_Invocation_1(object instance, MethodBase method, object[] args) : base(instance, method, args)
            {
            }

            protected override object Invoke()
            {
                var args1 = Args;
                int z = (int)args1[3];
                var result = NoAdviceStruct<T>.StaticAddWithRefArg((int)args1[0], (short)args1[1], (string)args1[2], ref z);
                args1[3] = z;
                return result;
            }
        }
    }
}
