﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Weavee
{
    [MyAdvice(ExcludeConstructors = true)]
    public class AdviceAsyncClass
    {
        public AdviceAsyncClass()
        {
            Trace.WriteLine("AdviceAsyncClass.ctor");
        }

        public int Age { get; set; }

        [MyAdvice]
        public int Add(int x, int y)
        {
            Trace.WriteLine("AdviceAsyncClass.Add()");
            return x+y;
        }

        [MyAdvice]
        public async Task SimpleAsyncMethod()
        {
            Trace.WriteLine("SimpleAsyncMethod");
            await Task.Delay(5000).ConfigureAwait(false);
            Trace.WriteLine("After await SimpleAsyncMethod");
        }

        public async Task<int> SimpleAsyncMethodWithResult()
        {
            Trace.WriteLine("Before await SimpleAsyncMethodWithResult");
            await Task.Delay(5000).ConfigureAwait(false);
            Trace.WriteLine("After await SimpleAsyncMethodWithResult");;
            return 1;
        }

        [MyAdvice]
        public async Task<int> SimpleAsyncMethodWithException()
        {
            var res = await Task.FromResult(1);
            throw new Exception("123");
        }
    }
}