﻿using System.Diagnostics;
using System.Threading.Tasks;

namespace Weavee
{
    [MyAdvice]
    public class AdviceGenericMethod
    {
        public string GetValue(string value)
        {
            Trace.WriteLine("string GetValue(string value)");
            return value;
        }

        public T GetValue<T>(T value)
        {
            Trace.WriteLine("T AdviceGenericMethod.GetValue<T>(T value)");
            Trace.WriteLine($"{typeof(T).Name} AdviceGenericMethod.GetValue<{typeof(T).Name}>");
            return value;
        }

        public async Task<T> GetValueAsync<T>(T value)
        {
            Trace.WriteLine("T AdviceGenericMethod.GetValueAsync<T>(T value)");
            Trace.WriteLine($"{typeof(T).Name} AdviceGenericMethod.GetValueAsync<{typeof(T).Name}>");
            return await Task.FromResult(value);
        }
    }
}