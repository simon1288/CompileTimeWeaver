﻿using CompileTimeWeaver;
using System;
using System.ComponentModel;

namespace Weavee
{
    [NotifyPropertyChanged]
    public class Person //: INotifyPropertyChanged
    {
        //public event PropertyChangedEventHandler PropertyChanged;

        public string Name { get; set; }
        public int Age { get; set; }
        public double? Weight { get; set; }


        [IgnoreNotifyPropertyChanged]
        public string Password { get; set; }

        public int A { get; set; }
        public int B { get; set; }

        [DeterminedByProperties("A")]
        public int C => A + 1;

        [DeterminedByProperties("B","C")]
        public int D => B + C;
    }
}