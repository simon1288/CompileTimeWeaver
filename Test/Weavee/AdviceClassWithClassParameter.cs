using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Weavee
{
    [MyAdvice(ExcludeConstructors = true)]
    public class AdviceClassWithClassParameter
    {
        public void CopyTo(object x, out object z)
        {
            Trace.WriteLine("Your Code - CopyTo");
            z = x;
        }

        public async Task<object> CopyToAsync(object x)
        {
            Trace.WriteLine("Your Code - CopyToAsync");
            await Task.Delay(1000);
            return x;
        }
    }
}