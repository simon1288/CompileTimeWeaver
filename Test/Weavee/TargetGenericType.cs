using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Weavee
{
    [MyAdvice]
    public class TargetGenericType<U>
    {
        public TargetGenericType(int x, int y)
        {
            try
            {
                Trace.WriteLine($"this is TargetGenericType.ctor({x},{y})");
            }
            catch (Exception e)
            {
                Trace.WriteLine(e);
            }
        }

        public int MyProperty { get; set; }

        public U GetValue<T>(T value)
        {
            Trace.WriteLine($"TargetGenericType>{typeof(U).Name}>.GetValue<{typeof(T).Name}>");
            return default(U);
        }

        public async Task<U> GetValueAsync<T>(T value)
        {
            Trace.WriteLine($"TargetGenericType>{typeof(U).Name}>.GetValueAsync<{typeof(T).Name}>");
            await Task.Delay(1).ConfigureAwait(false);
            return default(U);
        }
    }
}