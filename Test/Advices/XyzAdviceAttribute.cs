using System;
using System.Diagnostics;
using System.Threading.Tasks;
using CompileTimeWeaver;

namespace Advices
{
    public class XyzAdviceAttribute : AdviceAttribute
    {
        public override object Advise(IInvocation invocation)
        {
            Trace.WriteLine("Entering " + invocation.Method.Name);
            try
            {
                return invocation.Proceed();
            }
            catch (Exception e)
            {
                Trace.WriteLine("XyzAdvice catches an exception: " + e.Message);
                throw;
            }
            finally
            {
                Trace.WriteLine("Leaving " + invocation.Method.Name);
            }
        }

        public override async Task<object> AdviseAsync(IInvocation invocation)
        {
            Trace.WriteLine("Entering async " + invocation.Method.Name);
            try
            {
                return await invocation.ProceedAsync().ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Trace.WriteLine("XyzAdvice catches an exception: " + e.Message);
                throw;
            }
            finally
            {
                Trace.WriteLine("Leaving async " + invocation.Method.Name);
            }
        }
    }
}