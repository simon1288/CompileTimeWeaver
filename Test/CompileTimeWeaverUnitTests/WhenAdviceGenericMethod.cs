using System;
using System.Diagnostics;
using System.Threading.Tasks;
using CompileTimeWeaverUnitTests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CompileTimeWeaverUnitTests
{
    [TestClass]
    public class WhenAdviceGenericMethod : WeaverTestBase
    {
        public WhenAdviceGenericMethod() : base("Weavee.AdviceGenericMethod")
        {
        }

        [TestMethod]
        public void GetValue()
        {
            var obj = this.TestClass.GetValue("Simon");
            Trace.WriteLine($"{obj} is returned");
        }

        [TestMethod]
        public void GetValueGeneric()
        {
            var obj = this.TestClass.GetValue<string>("Simon");
            Trace.WriteLine($"{obj} is returned");
            Assert.IsTrue(obj == "Simon");
        }

        [TestMethod]
        public void GetValueAsync()
        {
            var obj = this.TestClass.GetValueAsync<string>("Simon").Result;
            Trace.WriteLine($"{obj} is returned");
            Assert.IsTrue(obj == "Simon");
        }
    }
}