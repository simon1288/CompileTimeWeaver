using System;
using System.Diagnostics;
using System.Threading.Tasks;
using CompileTimeWeaverUnitTests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CompileTimeWeaverUnitTests
{
    [TestClass]
    public class WhenAdviceAbstractMethods : WeaverTestBase
    {
        private readonly dynamic _testClass;

        public WhenAdviceAbstractMethods() : base("Weavee.AdviceAbstractMethods")
        {
            _testClass = this.TestClass;
        }
  
        [TestMethod]
        public void DoNotAdviceAbstractMethod()
        {
            _testClass.AbstractMethod();
        }
    }
}