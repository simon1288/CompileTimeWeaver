using System.Diagnostics;
using System.Linq;
using CompileTimeWeaverUnitTests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CompileTimeWeaverUnitTests
{
    [TestClass]
    public class WhenAdivceStruct : WeaverTestBase
    {
        private readonly dynamic _testClass;

        public WhenAdivceStruct() : base("Weavee.AdviceStruct", "arg1", "arg2")
        {
            _testClass = this.TestClass;
        }

        [TestMethod]
        public void AddWithOutArg()
        {
            var listener = new AdviceTraceListener();
            Trace.Listeners.Add(listener);
            try
            {
                int result = _testClass.AddWithOutArg(1, (short)2, "dummy", out int z);
                Trace.WriteLine("AddWithOutArg=" + result + ", z=" + z);

                Assert.IsTrue(listener.LogList.Any(x => x.StartsWith("Entering ")));
                Assert.IsTrue(listener.LogList.Any(x => x.StartsWith("Leaving ")));
                Assert.AreEqual(result, 3);
                Assert.AreEqual(z, 3);
            }
            finally
            {
                Trace.Listeners.Remove(listener);
            }
        }

        [TestMethod]
        public void AddWithRefArg()
        {
            var listener = new AdviceTraceListener();
            Trace.Listeners.Add(listener);
            try
            {
                int z = 10;
                int result = _testClass.AddWithRefArg(1, (short)2, "dumy", ref z);
                Trace.WriteLine("AddWithRefArg=" + result + ", z=" + z);

                Assert.IsTrue(listener.LogList.Any(x => x.StartsWith("Entering ")));
                Assert.IsTrue(listener.LogList.Any(x => x.StartsWith("Leaving ")));
                Assert.AreEqual(result, 3);
                Assert.AreEqual(z, 13);
            }
            finally
            {
                Trace.Listeners.Remove(listener);
            }
        }


        [TestMethod]
        public void HelloAsyncNoReturn()
        {
            _testClass.HelloAsyncNoReturn(1, (short) 2, "dumy").Wait();
            Trace.WriteLine("HelloAsyncNoReturn");
        }

        [TestMethod]
        public void AddAsyncWithReturn()
        {
            int x = _testClass.AddAsyncWithReturn(1, (short) 2, "dumy").Result;
            Trace.WriteLine("AddAsyncWithReturn=" + x);
            Assert.AreEqual(x, 3);
        }

        [TestMethod]
        public void AddWithRefArgNotReturn()
        {
            int z = 10;
            _testClass.AddWithRefArgNotReturn(1, (short) 2, "dumy", ref z);
            Trace.WriteLine("AddWithRefArgNotReturn z=" + z);
            Assert.AreEqual(z, 13);
        }

        [TestMethod]
        public void StaticAddWithRefArg()
        {
            int z = 10;
            int x = _testClass.StaticAddWithRefArgCaller(1, (short) 2, "dumy", ref z);
            Trace.WriteLine("StaticAddWithRefArg=" + x + ", z=" + z);
            Assert.AreEqual(x, 3);
            Assert.AreEqual(z, 13);
        }
    }
}