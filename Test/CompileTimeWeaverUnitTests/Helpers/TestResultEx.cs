﻿using System;
using Fody;

namespace CompileTimeWeaverUnitTests.Helpers
{
    public static class TestResultEx
    {
        public static dynamic GetInstance(this TestResult testResult, string className, params object[] args)
        {
            //Guard.AgainstNullAndEmpty(nameof(className), className);
            var type = testResult.Assembly.GetType(className, true);
            return Activator.CreateInstance(type, args);
        }

        public static dynamic GetGenericInstance(this TestResult testResult, string className, Type[] types, params object[] args)
        {
            //Guard.AgainstNullAndEmpty(nameof(className), className);
            var type = testResult.Assembly.GetType(className, true);
            var genericType = type.MakeGenericType(types);
            return Activator.CreateInstance(genericType);
        }
    }
}