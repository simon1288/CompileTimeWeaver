﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace CompileTimeWeaverUnitTests.Helpers
{
    internal class AdviceTraceListener : TraceListener
    {
        public List<string> LogList { get; } = new List<string>();

        public override void Write(string message)
        {
            LogList.Add(message);
        }

        public override void WriteLine(string message)
        {
            LogList.Add(message + Environment.NewLine);
        }
    }
}
