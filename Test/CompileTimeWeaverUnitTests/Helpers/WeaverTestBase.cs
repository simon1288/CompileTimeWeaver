﻿using CompileTimeWeaver.Fody;
using Fody;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;

namespace CompileTimeWeaverUnitTests.Helpers
{
    public class WeaverTestBase : IDisposable
    {
        private static TestResult testResult;

        static WeaverTestBase()
        {
#if DEBUG
            const string dllPath = @"..\..\..\..\Weavee\bin\Debug\netstandard2.0\";
#else
            const string dllPath = @"..\..\..\..\Weavee\bin\Release\netstandard2.0\";
#endif

            const string asm = "Weavee.dll";

            var weaver = new ModuleWeaver();
            testResult = weaver.ExecuteTestRun(
                dllPath + asm,
                runPeVerify: true,
                beforeExecuteCallback: moduleDefinition =>
                                       {
                                           //copy all references to fodytemp directory
                                           IoHelper.PurgeDirectory($"{dllPath}fodytemp");
                                           new DirectoryInfo(dllPath).GetFiles()
                                                             //.Where(x => !x.Name.StartsWith("Weavee.") && (x.Extension == ".dll" || x.Extension == ".pdb"))
                                                             .ToList()
                                                             .ForEach(file => { file.CopyTo($"{dllPath}fodytemp\\{file.Name}", true); });
                                       },
                afterExecuteCallback: moduleDefinition =>
                {
                    Trace.WriteLine("ModuleWeaver executed.");
                });
        }


        private readonly static object _sync = new object();

        public WeaverTestBase(string className, params object[] args)
        {
            // almost global lock to prevent parallel test run because we use static (
            Monitor.Enter(_sync);
            this.TestClass = testResult.GetInstance(className, args);
        }

        public void Dispose()
        {
            Monitor.Exit(_sync);
        }

        protected dynamic TestClass { get; private set; }
    }
}
