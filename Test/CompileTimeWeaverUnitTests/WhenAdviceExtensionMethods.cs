using System;
using System.Diagnostics;
using System.Threading.Tasks;
using CompileTimeWeaverUnitTests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CompileTimeWeaverUnitTests
{
    [TestClass]
    public class WhenAdviceExtensionMethods : WeaverTestBase
    {
        private readonly dynamic _testClass;

        public WhenAdviceExtensionMethods() : base("Weavee.AdviceExtensionMethods")
        {
            _testClass = this.TestClass;
        }
  
        [TestMethod]
        public void ReturnsString()
        {
            string s = _testClass.ReturnsString();
            Assert.IsTrue(s == "ToTitleCase2 is extension method.");
        }
    }
}