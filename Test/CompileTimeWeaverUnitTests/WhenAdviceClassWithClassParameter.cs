using System;
using System.Diagnostics;
using System.Threading.Tasks;
using CompileTimeWeaverUnitTests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CompileTimeWeaverUnitTests
{
    [TestClass]
    public class WhenAdviceClassWithClassParameter : WeaverTestBase
    {
        private readonly dynamic _testClass;

        public WhenAdviceClassWithClassParameter() : base("Weavee.AdviceClassWithClassParameter")
        {
            _testClass = this.TestClass;
        }

        [TestMethod]
        public void CopyTo()
        {
            object x = new ClassParam();
            object z;
            _testClass.CopyTo(x, out z);
            Assert.IsTrue(object.ReferenceEquals(x, z));
        }


        [TestMethod]
        public void CopyToAsync()
        {
            object x = new ClassParam();
            object z = _testClass.CopyToAsync(x).Result;
            Assert.IsTrue(object.ReferenceEquals(x, z));
        }
    }

    public class ClassParam
    {
    }
}