using System.ComponentModel;
using System.Diagnostics;
using CompileTimeWeaverUnitTests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CompileTimeWeaverUnitTests
{
    [TestClass]
    public class WhenStudentNotifyPropertyChanged : WeaverTestBase
    {
        string changedPropertyName = null;

        public WhenStudentNotifyPropertyChanged() : base("Weavee.Student")
        {
        }
        
        [TestMethod]
        public void SetSchool()
        {
            var student = (INotifyPropertyChanged)TestClass;
            student.PropertyChanged += Student_PropertyChanged;

            try
            {
                TestClass.Name = "James Lu";

                Assert.IsTrue(changedPropertyName == "Name");
                Assert.IsTrue(TestClass.Name == "James Lu");

                TestClass.School = "Borchard Element";
                Assert.IsTrue(changedPropertyName == "School");
                Assert.IsTrue(TestClass.School == "Borchard Element");

                changedPropertyName = null;
                TestClass.School = "Borchard Element";
                Assert.IsTrue(changedPropertyName == null);
                Assert.IsTrue(TestClass.School == "Borchard Element");
            }
            finally
            {
                student.PropertyChanged -= Student_PropertyChanged;
                Trace.WriteLine($"student {TestClass.Name} school is {TestClass.School}");
            }
        }

        private void Student_PropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            changedPropertyName = args.PropertyName;
        }
    }
}