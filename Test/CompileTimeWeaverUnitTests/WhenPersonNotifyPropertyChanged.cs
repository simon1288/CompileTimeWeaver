﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using CompileTimeWeaverUnitTests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CompileTimeWeaverUnitTests
{
    [TestClass]
    public class WhenPersonNotifyPropertyChanged : WeaverTestBase
    {
        List<string> changedPropertyName = new List<string>();

        public WhenPersonNotifyPropertyChanged() : base("Weavee.Person")
        {
        }

        [TestMethod]
        public void TestNotifyPropertyChangedAttribute()
        {
            var person = (INotifyPropertyChanged)TestClass;
            person.PropertyChanged += Person_PropertyChanged;

            try
            {
                changedPropertyName.Clear();
                TestClass.Name = "James Lu";

                Assert.IsTrue(changedPropertyName[0] == "Name");
                Assert.IsTrue(TestClass.Name == "James Lu");

                changedPropertyName.Clear();
                TestClass.Age = 21;
                Assert.IsTrue(changedPropertyName[0] == "Age");
                Assert.IsTrue(TestClass.Age == 21);

                changedPropertyName.Clear();
                TestClass.Weight = 130.8;
                Assert.IsTrue(changedPropertyName[0] == "Weight");
                Assert.IsTrue(TestClass.Weight == 130.8);
            }
            finally
            {
                person.PropertyChanged -= Person_PropertyChanged;
            }
        }

        [TestMethod]
        public void TestIgnoreNotifyPropertyChangedAttribute()
        {
            var person = (INotifyPropertyChanged)TestClass;
            person.PropertyChanged += Person_PropertyChanged;

            try
            {
                changedPropertyName.Clear();
                TestClass.Password = "xxxxx";
                Assert.IsTrue(changedPropertyName.Count == 0);
                Assert.IsTrue(TestClass.Password == "xxxxx");

                TestClass.Password = "yyyy";
                Assert.IsTrue(changedPropertyName.Count == 0);
                Assert.IsTrue(TestClass.Password == "yyyy");
            }
            finally
            {
                person.PropertyChanged -= Person_PropertyChanged;
            }
        }

        [TestMethod]
        public void TestDeterminedByPropertiesAttribute()
        {
            var person = (INotifyPropertyChanged)TestClass;
            person.PropertyChanged += Person_PropertyChanged;

            try
            {
                changedPropertyName.Clear();
                TestClass.A = 1;

                Assert.IsTrue(changedPropertyName.Contains("A"));
                Assert.IsTrue(changedPropertyName.Contains("C"));
                Assert.IsTrue(changedPropertyName.Contains("D"));
                Assert.IsTrue(TestClass.A == 1);

                changedPropertyName.Clear();
                TestClass.B = 21;
                Assert.IsTrue(changedPropertyName.Contains("B"));
                Assert.IsTrue(changedPropertyName.Contains("D"));
                Assert.IsTrue(TestClass.B == 21);
            }
            finally
            {
                person.PropertyChanged -= Person_PropertyChanged;
            }
        }

        private void Person_PropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            Trace.WriteLine(args.PropertyName + "  changed.");
            changedPropertyName.Add(args.PropertyName);
        }
    }
}