using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using CompileTimeWeaver;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CompileTimeWeaverUnitTests
{
    [TestClass]
    public class CallContextUnitTest
    {
        [TestMethod]
        public void SettingValueOnOneThreadDoesNotAffectTheOther()
        {
            var mre1 = new ManualResetEvent(false);
            var mre2 = new ManualResetEvent(false);
            int result = 0;
            var t1 = new Thread((_) =>
                                {
                                    CallContext.LogicSetData("foo", 1);
                                    mre1.Set();
                                    mre2.WaitOne();
                                    result = (int)CallContext.LogicalGetData("foo");
                                });
            var t2 = new Thread((_) =>
                                {
                                    mre1.WaitOne();
                                    CallContext.LogicSetData("foo", 2);
                                    mre2.Set();
                                });

            t1.Start();
            t2.Start();

            t1.Join();
            t2.Join();

            Assert.IsTrue(1 == result);
        }

        [TestMethod]
        public async Task SettingValueInCalledMethod()
        {
            CallContext.LogicSetData("foo1", 1);
            SetFoo(2);  //sync inner call changes outer setting
            Assert.AreEqual((int)CallContext.LogicalGetData("foo1"), 2);

            CallContext.LogicSetData("foo2", 1);
            await SetFooAsync(2).ConfigureAwait(false); //async inner call doesn't change outer setting
            var data = CallContext.LogicalGetData("foo2");
            Debug.WriteLine($"data={data}");

            Assert.AreEqual((int)data, 1);
        }

        private void SetFoo(int x)
        {
            CallContext.LogicSetData("foo1", x);
        }

        private async Task SetFooAsync(int x)
        {
            await Task.Delay(1).ConfigureAwait(false);

            var data = CallContext.LogicalGetData("foo2");
            Debug.WriteLine($"data={data}");
            Assert.AreEqual((int)data, 1);

            CallContext.LogicSetData("foo2", x);
            await Task.Delay(1).ConfigureAwait(false);

            data = CallContext.LogicalGetData("foo2");
            Debug.WriteLine($"data={data}");
            Assert.AreEqual((int)data, x);
        }
    }
}