﻿using System;
using System.Threading.Tasks;
using CompileTimeWeaverUnitTests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CompileTimeWeaverUnitTests
{
    [TestClass]
    public class WhenAdvisedByExceptionHandlerAttribute : WeaverTestBase
    {
        private readonly dynamic _testClass;

        public WhenAdvisedByExceptionHandlerAttribute() : base("Weavee.AdviceExceptionHandler")
        {
            _testClass = this.TestClass;
        }

        [TestMethod]
        public async Task TestMethodReturnVoid()
        {
            try
            {
                _testClass.Hello();
                await _testClass.HelloAsync();
            }
            catch (Exception e)
            {
                Assert.Fail("should not have exception to catch: " + e);
            }
        }

        [TestMethod]
        public async Task TestMethodReturnValueType()
        {
            try
            {
                int rt = _testClass.HelloReturnValueType();
                Assert.IsTrue(rt == 0, "should return default value to returned datatype even though there was exception thrown.");

                rt = await _testClass.HelloReturnValueTypeAsync();
                Assert.IsTrue(rt == 0, "should return default value to returned datatype even though there was exception thrown.");
            }
            catch (Exception)
            {
                Assert.Fail("should not have exception to catch");
            }
        }

        [TestMethod]
        public async Task TestMethodReturnRefType()
        {
            try
            {
                string rt = _testClass.HelloReturnRefType();
                Assert.IsTrue(rt == null, "should return default value to returned datatype even though there was exception thrown.");

                rt = await _testClass.HelloReturnRefTypeAsync();
                Assert.IsTrue(rt == null, "should return default value to returned datatype even though there was exception thrown.");
            }
            catch (Exception)
            {
                Assert.Fail("should not have exception to catch");
            }
        }
    }
}