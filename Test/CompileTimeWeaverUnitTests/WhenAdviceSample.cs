using System.Diagnostics;
using CompileTimeWeaverUnitTests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CompileTimeWeaverUnitTests
{
    [TestClass]
    public class WhenAdviceSample : WeaverTestBase
    {
        public WhenAdviceSample() : base("Weavee.AdviceSample")
        {
        }
        
        [TestMethod]
        public void CallCtorWithArgs()
        {
            var obj = TestClass.CallCtorWithArgs("Simon", "Lu");
            Trace.WriteLine($"{obj} is created");
        }

        [TestMethod]
        public void HelloAsyncNoReturn()
        {
            TestClass.HelloAsyncNoReturn(1, (short)2, "dumy").Wait();
            Trace.WriteLine("HelloAsyncNoReturn");
        }

        [TestMethod]
        public void AddAsyncWithReturn()
        {
            int x = TestClass.AddAsyncWithReturn(1, (short)2, "dumy").Result;
            Trace.WriteLine("AddAsyncWithReturn=" + x);
            Assert.AreEqual(x, 3);
        }

        [TestMethod]
        public void AddWithOutArg()
        {
            int z;
            int x = TestClass.AddWithOutArg(1, (short)2, "dumy", out z);
            Trace.WriteLine("AddWithOutArg=" + x + ", z=" + z);
            Assert.AreEqual(x, 3);
            Assert.AreEqual(z, 3);
        }

        [TestMethod]
        public void AddWithRefArg()
        {
            int z = 10;
            int x = TestClass.AddWithRefArg(1, (short)2, "dumy", ref z);
            Trace.WriteLine("AddWithRefArg=" + x + ", z=" + z);
            Assert.AreEqual(x, 3);
            Assert.AreEqual(z, 13);
        }


        [TestMethod]
        public void StaticAddWithRefArg()
        {
            int z = 10;
            int x = TestClass.StaticAddWithRefArgCaller(1, (short)2, "dumy", ref z);
            Trace.WriteLine("StaticAddWithRefArg=" + x + ", z=" + z);
            Assert.AreEqual(x, 3);
            Assert.AreEqual(z, 13);
        }

        [TestMethod]
        public void AddWithRefArgNotReturn()
        {
            int z = 10;
            TestClass.AddWithRefArgNotReturn(1, (short)2, "dumy", ref z);
            Trace.WriteLine("AddWithRefArgNotReturn z=" + z);
            Assert.AreEqual(z, 13);
        }
    }
}