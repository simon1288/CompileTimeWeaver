﻿using System.Diagnostics;
using Serilog;
using CompileTimeWeaverUnitTests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CompileTimeWeaverUnitTests
{
    [TestClass]
    public class WhenAdvisedByLoggingAttribute : WeaverTestBase
    {
        private readonly dynamic _testClass;

        public WhenAdvisedByLoggingAttribute() : base("Weavee.AdviceLoggingTarget")
        {
            _testClass = this.TestClass;
        }

        [TestMethod]
        public void TestMethod1()
        {
            Serilog.Log.Logger = new Serilog.LoggerConfiguration()
                                    .MinimumLevel.Debug()
                                    .Enrich.FromLogContext()
                                    .WriteTo.Trace()
                                    .CreateLogger();

            Serilog.Log.Information("startup...");

            int x = _testClass.HelloAsync("This is arg1", 2, "password").Result;
            Trace.WriteLine(x);

            Serilog.Log.CloseAndFlush();
        }
    }
}
