//using System;
//using System.Diagnostics;
//using System.Linq;
//using System.Threading.Tasks;
//using CompileTimeWeaverUnitTests.Helpers;
//using Microsoft.VisualStudio.TestTools.UnitTesting;

//namespace CompileTimeWeaverUnitTests
//{
//    [TestClass]
//    public class WhenAdvicedByAspect: WeaverTestBase
//    {
//        public WhenAdvicedByAspect() : base("Weavee.TargetAspectGenericType`1[[System.Int32, mscorlib]]", 1, 2)
//        {
//        }

//        [TestMethod]
//        public void MyProperty()
//        {
//            var listener = new AdviceTraceListener();
//            Trace.Listeners.Add(listener);
//            try
//            {
//                this.TestClass.MyProperty = 6;
//                this.TestClass.MyProperty = 8;

//                Assert.IsTrue(listener.LogList.Any(x => x.StartsWith("Entering ")));
//                Assert.IsTrue(listener.LogList.Any(x => x.StartsWith("Leaving ")));
//            }
//            finally
//            {
//                Trace.Listeners.Remove(listener);
//            }
//        }

//        [TestMethod]
//        public void CallGetValue()
//        {
//            var listener = new AdviceTraceListener();
//            Trace.Listeners.Add(listener);
//            try
//            {
//                int result = this.TestClass.GetValue(7);
//                Trace.WriteLine($"{result} is returned");

//                Assert.IsTrue(listener.LogList.Any(x => x.StartsWith("Entering ")));
//                Assert.IsTrue(listener.LogList.Any(x => x.StartsWith("Leaving ")));
//                Assert.IsTrue(result == default(int));
//            }
//            finally 
//            {
//                Trace.Listeners.Remove(listener);
//            }
//        }

//        [TestMethod]
//        public void CallGetValueAsync()
//        {
//            var listener = new AdviceTraceListener();
//            Trace.Listeners.Add(listener);
//            try
//            {
//                Task<int> task = this.TestClass.GetValueAsync(7);
//                int result = task.Result;
//                Trace.WriteLine($"{result} is returned");

//                Assert.IsTrue(listener.LogList.Any(x => x.StartsWith("Entering ")));
//                Assert.IsTrue(listener.LogList.Any(x => x.StartsWith("Leaving ")));
//                Assert.IsTrue(result == default(int));
//            }
//            finally
//            {
//                Trace.Listeners.Remove(listener);
//            }
//        }
//    }
//}