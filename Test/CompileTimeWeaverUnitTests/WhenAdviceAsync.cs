using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CompileTimeWeaverUnitTests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CompileTimeWeaverUnitTests
{
    [TestClass]
    public class WhenAdviceAsync : WeaverTestBase
    {
        private readonly dynamic _testClass;

        public WhenAdviceAsync() : base("Weavee.AdviceAsyncClass")
        {
            _testClass = this.TestClass;
        }

        [TestMethod]
        public void AutoProperty()
        {
            var listener = new AdviceTraceListener();
            Trace.Listeners.Add(listener);
            try
            {
                _testClass.Age = 16;
                int result = _testClass.Age;

                Assert.IsTrue(listener.LogList.Any(x => x.StartsWith("Entering ")));
                Assert.IsTrue(listener.LogList.Any(x => x.StartsWith("Leaving ")));
                Assert.IsTrue(result == 16);
            }
            finally
            {
                Trace.Listeners.Remove(listener);
            }
        }

        [TestMethod]
        public void Add()
        {
            var listener = new AdviceTraceListener();
            Trace.Listeners.Add(listener);
            try
            {
                int result = _testClass.Add(1, 2);

                Assert.IsTrue(listener.LogList.Any(x => x.StartsWith("Entering ")));
                Assert.IsTrue(listener.LogList.Any(x => x.StartsWith("Leaving ")));
                Assert.IsTrue(result == 3);
            }
            finally
            {
                Trace.Listeners.Remove(listener);
            }
        }

        [TestMethod]
        public void SimpleAsyncMethod()
        {
            var listener = new AdviceTraceListener();
            Trace.Listeners.Add(listener);
            try
            {
                Task r = _testClass.SimpleAsyncMethod();
                r.Wait();

                Assert.IsTrue(listener.LogList.Any(x => x.StartsWith("Entering ")));
                Assert.IsTrue(listener.LogList.Any(x => x.StartsWith("Leaving ")));
            }
            finally
            {
                Trace.Listeners.Remove(listener);
            }
        }

        [TestMethod]
        public void AsyncMethodWithResult()
        {
            var listener = new AdviceTraceListener();
            Trace.Listeners.Add(listener);
            try
            {
                Task<int> r = _testClass.SimpleAsyncMethodWithResult();
                var result = r.Result;
                Assert.AreEqual(1, result);

                Assert.IsTrue(listener.LogList.Count(x => x.StartsWith("Entering ")) == 1);
                Assert.IsTrue(listener.LogList.Count(x => x.StartsWith("Leaving ")) == 1);
            }
            finally
            {
                Trace.Listeners.Remove(listener);
            }
        }

        [TestMethod]
        public async Task AsyncMethodWithException()
        {
            var listener = new AdviceTraceListener();
            Trace.Listeners.Add(listener);
            try
            {
                int r = await _testClass.SimpleAsyncMethodWithException();
            }
            catch (Exception e)
            {
                Assert.IsTrue(e.Message == "123");
            }
            finally
            {
                Assert.IsTrue(listener.LogList.Count(x => x.StartsWith("Entering ")) >= 1);
                Assert.IsTrue(listener.LogList.Count(x => x.StartsWith("Leaving ")) >= 1);
                Trace.Listeners.Remove(listener);
            }
        }
    }
}