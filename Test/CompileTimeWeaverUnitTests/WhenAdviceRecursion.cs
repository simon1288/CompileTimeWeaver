using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CompileTimeWeaverUnitTests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CompileTimeWeaverUnitTests
{
    [TestClass]
    public class WhenAdviceRecursion : WeaverTestBase
    {
        public WhenAdviceRecursion() : base("Weavee.AdviceRecursion")
        {
        }

        [TestMethod]
        public void Loop()
        {
            var obj = this.TestClass;
            for (int i = 0; i < 5; i++)
            {
                int x = obj.Fabi(4);
                Assert.AreEqual(x, 5);
            }
        }

        [TestMethod]
        public void Recursion()
        {
            var listener = new AdviceTraceListener();
            Trace.Listeners.Add(listener);
            try
            {
                var obj = this.TestClass;
                int result = obj.Fabi(4);

                Assert.AreEqual(result, 5);

                // Recursive calls are intercepted only at the first entrance to the advised method
                Assert.IsTrue(listener.LogList.Count(x => x.StartsWith("Entering Fabi")) == 1);
                Assert.IsTrue(listener.LogList.Count(x => x.StartsWith("Leaving Fabi")) == 1);
            }
            finally
            {
                Trace.Listeners.Remove(listener);
            }
        }
    }
}