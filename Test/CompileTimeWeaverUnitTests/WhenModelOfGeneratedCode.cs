using System;
using System.Diagnostics;
using System.Threading.Tasks;
using CompileTimeWeaverUnitTests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CompileTimeWeaverUnitTests
{
    [TestClass]
    public class WhenModelOfGeneratedCode : WeaverTestBase
    {
        public WhenModelOfGeneratedCode() : base("Weavee.ModelOfGeneratedCode`1[[System.Int32, mscorlib]]")
        {
        }

        [TestMethod]
        public void AddAsync()
        {
            var obj = this.TestClass;
            for (int i = 0; i < 5; i++)
            {
                int x = obj.AddAsync(1, 2).Result;
                Trace.WriteLine("AddAsync=" + x);
            }
        }

        [TestMethod]
        public void Add()
        {
            var obj = this.TestClass;
            for (int i = 0; i < 5; i++)
            {
                int z;
                int x = obj.Add<string>(1, (short)2, "dumy", out z, "hello");
                Trace.WriteLine("add=" + x + ", z=" + z);
            }
        }

        [TestMethod]
        public void Recursion()
        {
            var obj = this.TestClass;
            int x = obj.Fabi(3);
            Trace.WriteLine("Fabi(3)=" + x);
        }
    }
}