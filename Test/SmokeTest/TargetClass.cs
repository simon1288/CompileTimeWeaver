﻿using System;
using Advices;
using CompileTimeWeaver;

namespace SmokeTest
{
    [XyzAdvice]
    [Logging]
    [NotifyPropertyChanged]
    public class TargetClass
    {
        public int Add(int x, int y)
        {
            return x + y;
        }

        public int Property1 { get; set; }
        public string Property2 { get; set; }
    }
}
