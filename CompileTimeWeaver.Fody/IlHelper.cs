using System.Collections.Generic;
using System.Reflection;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace CompileTimeWeaver.Fody
{
    internal static class IlHelper
    {
        /// <summary>
        /// MethodBase method = MethodBase.GetMethodFromHandle(__methodref (Sample.Method), __typeref (Sample));
        /// </summary>
        /// <param name="method"></param>
        /// <param name="methodVariableDefinition"></param>
        /// <param name="refFinder"></param>
        /// <returns></returns>
        public static List<Instruction> GetMethodBaseAsLocalVariable(
            this MethodDefinition method,
            VariableDefinition methodVariableDefinition,
            ReferenceFinder refFinder)
        {
            var il = method.Body.GetILProcessor();
            var getMethodFromHandle = refFinder.GetMethodReference(typeof(MethodBase),
                                                                   md => md.Name == "GetMethodFromHandle" && md.Parameters.Count == 2);
            return new List<Instruction>
            {
                il.Create(OpCodes.Ldtoken, method),
                il.Create(OpCodes.Ldtoken, method.DeclaringType),
                il.Create(OpCodes.Call, getMethodFromHandle),       // Push method onto the stack, GetMethodFromHandle, result on stack
                il.Create(OpCodes.Stloc, methodVariableDefinition)  // Store method in __fody$method
            };
        }

        /// <summary>
        /// object[] args = new object[1] { (object) value };
        /// </summary>
        /// <param name="method"></param>
        /// <param name="arrayVariable"></param>
        /// <param name="refFinder"></param>
        /// <param name="invocationBaseType"></param>
        /// <returns></returns>
        public static List<Instruction> CreateArgsArrayAsLocalVariable(
            this MethodDefinition method,
            VariableDefinition arrayVariable, /*parameters*/
            ReferenceFinder refFinder,
            TypeReference invocationBaseType)
        {
            var il = method.Body.GetILProcessor();

            var createArray = new List<Instruction>
            {
                il.Create(OpCodes.Ldc_I4, method.Parameters.Count), //method.Parameters.Count
                il.Create(OpCodes.Newarr,  refFinder.GetTypeReference(typeof(object))), // new object[method.Parameters.Count]
            };

            foreach (var p in method.Parameters)
            {
                createArray.AddRange(ProcessParam(refFinder, p, invocationBaseType));
            }

            createArray.Add(il.Create(OpCodes.Stloc, arrayVariable)); // var objArray = new object[method.Parameters.Count]

            return createArray;
        }

        private static IEnumerable<Instruction> ProcessParam(
            ReferenceFinder refFinder,
            ParameterDefinition parameterDefinition,
            TypeReference invocationBaseTypeRef)
        {
            var paramMetaData = parameterDefinition.ParameterType.MetadataType;
            if (paramMetaData == MetadataType.UIntPtr ||
                paramMetaData == MetadataType.FunctionPointer ||
                paramMetaData == MetadataType.IntPtr ||
                paramMetaData == MetadataType.Pointer)
            {
                yield break;
            }

            yield return Instruction.Create(OpCodes.Dup);
            yield return Instruction.Create(OpCodes.Ldc_I4, parameterDefinition.Index);

            var paramType = parameterDefinition.ParameterType;

            if (parameterDefinition.IsOut)
            {
                // If a parameter is passed by out then you need to initialize it
                // --------------------------------------------------------------
                var elementType = ((TypeSpecification) paramType).ElementType;
                //yield return Instruction.Create(OpCodes.Call, refFinder.GetMethodReference(typeof (InvocationBase), m => m.Name.StartsWith("GetDefault"))
                //                                                       .MakeGenericInstanceMethod(elementType));
                yield return Instruction.Create(OpCodes.Call, refFinder.GetMethodReference(invocationBaseTypeRef, m => m.Name.StartsWith("GetDefault"))
                                                                       .MakeGenericInstanceMethod(elementType));
                if (elementType.IsValueType || elementType.IsGenericParameter)
                {
                    // Box the parameter type
                    yield return Instruction.Create(OpCodes.Box, elementType);
                }
            }
            else
            {
                yield return Instruction.Create(OpCodes.Ldarg, parameterDefinition);
                if (paramType.IsByReference)
                {
                    // If a parameter is passed by reference then you need to use Ldind
                    // ------------------------------------------------------------
                    var referencedTypeSpec = (TypeSpecification) paramType;
                    var pointerToValueTypeVariable = false;
                    switch (referencedTypeSpec.ElementType.MetadataType)
                    {
                        //Indirect load value of type int8 as int32 on the stack
                        case MetadataType.Boolean:
                        case MetadataType.SByte:
                            yield return Instruction.Create(OpCodes.Ldind_I1);
                            pointerToValueTypeVariable = true;
                            break;

                        // Indirect load value of type int16 as int32 on the stack
                        case MetadataType.Int16:
                            yield return Instruction.Create(OpCodes.Ldind_I2);
                            pointerToValueTypeVariable = true;
                            break;

                        // Indirect load value of type int32 as int32 on the stack
                        case MetadataType.Int32:
                            yield return Instruction.Create(OpCodes.Ldind_I4);
                            pointerToValueTypeVariable = true;
                            break;

                        // Indirect load value of type int64 as int64 on the stack
                        // Indirect load value of type unsigned int64 as int64 on the stack (alias for ldind.i8)
                        case MetadataType.Int64:
                        case MetadataType.UInt64:
                            yield return Instruction.Create(OpCodes.Ldind_I8);
                            pointerToValueTypeVariable = true;
                            break;

                        // Indirect load value of type unsigned int8 as int32 on the stack
                        case MetadataType.Byte:
                            yield return Instruction.Create(OpCodes.Ldind_U1);
                            pointerToValueTypeVariable = true;
                            break;

                        // Indirect load value of type unsigned int16 as int32 on the stack
                        case MetadataType.UInt16:
                        case MetadataType.Char:
                            yield return Instruction.Create(OpCodes.Ldind_U2);
                            pointerToValueTypeVariable = true;
                            break;

                        // Indirect load value of type unsigned int32 as int32 on the stack
                        case MetadataType.UInt32:
                            yield return Instruction.Create(OpCodes.Ldind_U4);
                            pointerToValueTypeVariable = true;
                            break;

                        // Indirect load value of type float32 as F on the stack
                        case MetadataType.Single:
                            yield return Instruction.Create(OpCodes.Ldind_R4);
                            pointerToValueTypeVariable = true;
                            break;

                        // Indirect load value of type float64 as F on the stack
                        case MetadataType.Double:
                            yield return Instruction.Create(OpCodes.Ldind_R8);
                            pointerToValueTypeVariable = true;
                            break;

                        // Indirect load value of type native int as native int on the stack
                        case MetadataType.IntPtr:
                        case MetadataType.UIntPtr:
                            yield return Instruction.Create(OpCodes.Ldind_I);
                            pointerToValueTypeVariable = true;
                            break;

                        default:
                            // Need to check if it is a value type instance, in which case
                            // we use Ldobj instruction to copy the contents of value type
                            // instance to stack and then box it
                            if (referencedTypeSpec.ElementType.IsValueType)
                            {
                                yield return Instruction.Create(OpCodes.Ldobj, referencedTypeSpec.ElementType);
                                pointerToValueTypeVariable = true;
                            }
                            else
                            {
                                // It is a reference type so just use reference the pointer
                                yield return Instruction.Create(OpCodes.Ldind_Ref);
                            }
                            break;
                    }

                    if (pointerToValueTypeVariable)
                    {
                        // Box the de-referenced parameter type
                        yield return Instruction.Create(OpCodes.Box, referencedTypeSpec.ElementType);
                    }
                }
                else
                {
                    // If it is a value type then you need to box the instance as we are going 
                    // to add it to an array which is of type object (reference type)
                    // ------------------------------------------------------------
                    if (paramType.IsValueType || paramType.IsGenericParameter)
                    {
                        // Box the parameter type
                        yield return Instruction.Create(OpCodes.Box, paramType);
                    }
                }
            }

            // Store parameter in object[] array
            // ------------------------------------------------------------
            yield return Instruction.Create(OpCodes.Stelem_Ref);
        }
    }
}