﻿using Mono.Cecil;

namespace CompileTimeWeaver.Fody
{
    internal static class MethodDefinitionExts
    {
        public static bool IsAutoProperty(this MethodDefinition method)
        {
            return method.IsSpecialName && (method.IsGetter || method.IsSetter);
        }

        public static bool IsEventAdderRemover(this MethodDefinition method)
        {
            return method.IsSpecialName && (method.Name.StartsWith("add_") || method.Name.StartsWith("remove_"));
        }

        public static bool IsVisibleFromChildren(this MethodDefinition method)
        {
            return method.IsFamilyOrAssembly ||
                   method.IsFamily ||
                   method.IsFamilyAndAssembly ||
                   method.IsPublic;
        }
    }
}
