﻿//using System;
//using System.Reflection;
//using Mono.Cecil;
//using Mono.Cecil.Cil;

//namespace CompileTimeWeaver.Fody
//{
//    public static class MethodFinder
//    {
//        //private static readonly dynamic Converter;

//        //static MethodFinder()
//        //{
//        //    var path = Path.GetTempFileName();
//        //    CreateAssembly(path);
//        //    var assembly = Assembly.Load(File.ReadAllBytes(path));
//        //    Converter = assembly.CreateInstance("HelloWorld.MyClass");
//        //    File.Delete(path);
//        //}

//        public static MethodBase GetMethodBase(MethodDefinition methodDefinition)
//        {
//            //var referenceFinder = new ReferenceFinder(methodDefinition.DeclaringType.Module);

//            //var assembly = Assembly.Load(methodDefinition.DeclaringType.Module.Assembly.FullName);
//            //var type = assembly.GetType(methodDefinition.DeclaringType.FullName);

//            var type = methodDefinition.DeclaringType.GetMonoType();
//            if (type == null)
//                return null;

//            var parameters = methodDefinition.Parameters;
//            int paramCount = parameters.Count;

//            var paramTypes = new Type[paramCount];
//            for (int i = 0; i < paramCount; i++)
//            {
//                var param = parameters[i];
//                paramTypes[i] = param.ParameterType.GetMonoType();
//            }

//            return type.GetMethod(methodDefinition.FullName, paramTypes);

//            //try
//            //{
//            //    var methodBase = Converter.Convert(methodDefinition.MetadataToken.RID, methodDefinition.DeclaringType.MetadataToken.RID);
//            //    return (MethodBase)methodBase;
//            //}
//            //catch (Exception e)
//            //{
//            //    Debug.WriteLine(e);
//            //    throw new WeavingException(e.Message);
//            //}
//        }

//        public static void CreateAssembly(string path)
//        {
//            var assembly = AssemblyDefinition.CreateAssembly(
//                new AssemblyNameDefinition("HelloWorld", new Version(1, 0, 0, 0)), "HelloWorld", ModuleKind.Dll);

//            var module = assembly.MainModule;

//            // create the MyClass type and add it to the module
//            var clss = new TypeDefinition("HelloWorld",
//                                          "MyClass",
//                                          Mono.Cecil.TypeAttributes.Class | Mono.Cecil.TypeAttributes.Public, module.TypeSystem.Object);

//            module.Types.Add(clss);

//            // add an empty constructor
//            var ctor = new MethodDefinition(".ctor", Mono.Cecil.MethodAttributes.Public | Mono.Cecil.MethodAttributes.HideBySig
//                | Mono.Cecil.MethodAttributes.SpecialName | Mono.Cecil.MethodAttributes.RTSpecialName, module.TypeSystem.Void);

//            // create the constructor's method body
//            var il = ctor.Body.GetILProcessor();
//            il.Append(il.Create(OpCodes.Ldarg_0));
//            // call the base constructor
//            il.Append(il.Create(OpCodes.Call, module.ImportReference(typeof(object).GetConstructor(new Type[0]))));
//            //il.Append(il.Create(OpCodes.Nop));
//            il.Append(il.Create(OpCodes.Ret));
//            clss.Methods.Add(ctor);

//            // define the 'Convert' method and add it to MyClass 
//            var refFinder = new ReferenceFinder(module);
//            var returnType = refFinder.GetTypeReference(typeof(MethodBase));
//            var method = new MethodDefinition("Convert", Mono.Cecil.MethodAttributes.Public, returnType);

//            clss.Methods.Add(method);

//            // add the 'args' parameter
//            method.Parameters.Add(new ParameterDefinition("methodRuntimeID", Mono.Cecil.ParameterAttributes.None, refFinder.GetTypeReference(typeof(uint))));
//            method.Parameters.Add(new ParameterDefinition("typeRuntimeID", Mono.Cecil.ParameterAttributes.None, refFinder.GetTypeReference(typeof(uint))));

//            // create the method body
//            var getMethodFromHandle = refFinder.GetMethodReference(
//                typeof(MethodBase),
//                x => x.Name == "GetMethodFromHandle" && x.Parameters.Count == 2);
//            il = method.Body.GetILProcessor();
//            il.Append(il.Create(OpCodes.Ldarg_1));
//            il.Append(il.Create(OpCodes.Ldarg_2));
//            il.Append(il.Create(OpCodes.Call, getMethodFromHandle));
//            il.Append(il.Create(OpCodes.Ret));

//            // save the module
//            assembly.Write(path);
//        }
//    }
//}
