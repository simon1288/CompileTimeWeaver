﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace CompileTimeWeaver.Fody.Algorithms
{
    [Serializable]
    internal class Graph<VertexType, EdgeType>
    {
        protected List<VertexType> _vertexes;
        protected Sparse2DMatrix<EdgeType> _edges;
        protected bool _optimized;

        public Graph()
        {
            _edges = new Sparse2DMatrix<EdgeType>();
            _vertexes = new List<VertexType>();
        }

        public Graph(int vertexCapicity)
        {
            _edges = new Sparse2DMatrix<EdgeType>();
            _vertexes = new List<VertexType>(vertexCapicity);
        }

        public ReadOnlyCollection<VertexType> Vertexes
        {
            get { return _vertexes.AsReadOnly(); }
        }

        public int IndexOfVertex(VertexType vertex)
        {
            return _optimized ? _vertexes.BinarySearch(vertex) : _vertexes.IndexOf(vertex);
        }

        public int AddVertex(VertexType v)
        {
            int idx = IndexOfVertex(v);
            if (idx < 0)
            {
                idx = _vertexes.Count;
                _vertexes.Add(v);
                _optimized = false;
            }
            return idx;
        }

        public void AddVertexes(IEnumerable<VertexType> vs)
        {
            if (vs.Any())
            {
                _vertexes.AddRange(vs);
                _optimized = false;
            }
        }

        public void RemoveVertex(VertexType v)
        {
            int idx = IndexOfVertex(v);
            if (idx >= 0)
                _vertexes.RemoveAt(idx);
            _edges.RemoveRowColumn((uint)idx, (uint)idx);
        }

        public void RemoveVertexes(uint vertexIdx)
        {
            if (vertexIdx >= _vertexes.Count)
                throw new ArgumentException("The vertex doesn't exist.");
            _vertexes.RemoveAt((int)vertexIdx);
            _edges.RemoveRowColumn(vertexIdx, vertexIdx);
        }

        public void Optimize()
        {
            if (_edges.Count > 0)
                throw new InvalidOperationException("Optimization is not allowed after any edge is added.");
            if (!typeof(IComparable<VertexType>).IsAssignableFrom(typeof(VertexType)) && !typeof(IComparable).IsAssignableFrom(typeof(VertexType)))
                throw new InvalidOperationException("vertex are not comparable.");

            _vertexes.Sort();
            _optimized = true;
        }

        public void AddEdge(VertexType from, VertexType to, EdgeType weight)
        {
            int fromIdx = IndexOfVertex(from);
            if (fromIdx < 0)
                throw new ArgumentException("The from vertex doesn't exist.");
            int toIdx = IndexOfVertex(to);
            if (toIdx < 0)
                throw new ArgumentException("The to vertex doesn't exist.");
            _edges[(uint)fromIdx, (uint)toIdx] = weight;
        }

        public void AddEdge(uint fromIdx, uint toIdx, EdgeType weight)
        {
            if (fromIdx >= _vertexes.Count)
                throw new ArgumentException("The from vertex doesn't exist.");
            if (toIdx >= _vertexes.Count)
                throw new ArgumentException("The to vertex doesn't exist.");
            _edges[(uint)fromIdx, (uint)toIdx] = weight;
        }

        public void RemoveEdge(VertexType from, VertexType to)
        {
            int fromIdx = IndexOfVertex(from);
            if (fromIdx < 0)
                throw new ArgumentException("The from vertex doesn't exist.");
            int toIdx = IndexOfVertex(to);
            if (toIdx < 0)
                throw new ArgumentException("The to vertex doesn't exist.");
            RemoveEdge((uint)fromIdx, (uint)toIdx);
        }

        public void RemoveEdge(uint fromIdx, uint toIdx)
        {
            if (fromIdx >= _vertexes.Count)
                throw new ArgumentException("The from vertex doesn't exist.");
            if (toIdx >= _vertexes.Count)
                throw new ArgumentException("The to vertex doesn't exist.");
            _edges.Remove((uint)fromIdx, (uint)toIdx);
        }
        public void AddVertexesAndEdge(VertexType from, VertexType to, EdgeType weight)
        {
            int fromIdx = AddVertex(from);
            int toIdx = AddVertex(to);
            _edges[(uint)fromIdx, (uint)toIdx] = weight;
        }
        public bool HasChild(VertexType vertex)
        {
            int x = IndexOfVertex(vertex);
            if (x < 0)
                throw new ArgumentException($"The vertex {vertex} doesn't exit.");
            for (int y = _edges.YLowerBound; y <= _edges.YUpperBound; y++)
            {
                if (_edges.HasValueAt((uint)x, (uint)y))
                    return true;
            }
            return false;
        }

        public EdgeType GetWeight(VertexType from, VertexType to)
        {
            int fromIdx = AddVertex(from);
            if (fromIdx < 0)
                throw new ArgumentException("The from vertex doesn't exit.");
            int toIdx = AddVertex(to);
            if (toIdx < 0)
                throw new ArgumentException("The to vertex doesn't exit.");
            if (_edges.HasValueAt((uint)fromIdx, (uint)toIdx))
                throw new ArgumentException("The edge doesn't exist.");
            return _edges[(uint)fromIdx, (uint)toIdx];
        }

        public IEnumerable<VertexType> GetChildren(VertexType vertex)
        {
            int x = IndexOfVertex(vertex);
            if (x < 0)
                throw new ArgumentException($"The vertex {vertex} doesn't exit.");

            for (int y = _edges.YLowerBound; y <= _edges.YUpperBound; y++)
            {
                if (_edges.HasValueAt((uint)x, (uint)y))
                    yield return _vertexes[y];
            }
        }

        public List<VertexType> GetDescendants(VertexType vertex)
        {
            var descendents = new List<VertexType>(1000);
            Queue<VertexType> queue = new Queue<VertexType>(this.GetChildren(vertex));
            while (queue.Count > 0)
            {
                var v = queue.Dequeue();
                if (descendents.IndexOf(v) < 0)
                {
                    descendents.Add(v);
                    var children = this.GetChildren(v);
                    foreach (var child in children)
                    {
                        queue.Enqueue(child);
                    }
                }
            }

            return descendents;
        }

        public bool HasParent(VertexType vertex)
        {
            int y = IndexOfVertex(vertex);
            if (y < 0)
                return false;

            for (int x = _edges.XLowerBound; x < _edges.XUpperBound; x++)
            {
                if (_edges.HasValueAt((uint)x, (uint)y))
                    return true;
            }

            return false;
        }

        public IEnumerable<VertexType> GetParents(VertexType vertex)
        {
            int y=IndexOfVertex(vertex);
            if (y < 0)
                throw new ArgumentException("The vertext doesn't exit.");

            for (int x = _edges.XLowerBound; x < _edges.XUpperBound; x++)
            {
                if (_edges.HasValueAt((uint)x, (uint)y))
                    yield return _vertexes[x];
            }
        }

        public List<VertexType> GetAncestors(VertexType vertex)
        {
            var ascendents = new List<VertexType>(1000);

            var queue = new Queue<VertexType>(this.GetParents(vertex));
            while(queue.Count > 0)
            {
                VertexType v = queue.Dequeue();
                if (ascendents.IndexOf(v)<0)
                {
                    ascendents.Add(v);

                    var parents = this.GetParents(v);
                    foreach(var parent in parents)
                    {
                        queue.Enqueue(parent);
                    }
                }
            }

            return ascendents;
        }

        public IEnumerable<VertexType> GetTopMostAncestors(VertexType vertex)
        {
            var ascendents = new List<VertexType>(1000);

            var queue = new Queue<VertexType>(this.GetParents(vertex));
            while (queue.Count > 0)
            {
                VertexType v = queue.Dequeue();
                if (ascendents.IndexOf(v) < 0)
                {
                    ascendents.Add(v);

                    var parents = this.GetParents(v);
                    if (parents.Count() == 0)
                    {
                        yield return v;
                    }
                    else
                    {
                        foreach (var parent in parents)
                        {
                            queue.Enqueue(parent);
                        }
                    }
                }
            }
        }

        public void Clear()
        {
            _edges.Clear();
            _vertexes.Clear();
        }
    }
}
