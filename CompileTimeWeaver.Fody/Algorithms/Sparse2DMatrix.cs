﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CompileTimeWeaver.Fody.Algorithms
{
    internal class Sparse2DMatrix<T> : ICloneable
    {
        protected Dictionary<UInt64, T> _hashtable;
        protected int _xLowerBound = 0, _xUpperBound = -1;
        protected int _yLowerBound = 0, _yUpperBound = -1;

        public Sparse2DMatrix()
        {
            _hashtable = new Dictionary<UInt64, T>();
        }

        public object Clone()
        {
            var clone = new Sparse2DMatrix<T>();
            clone._hashtable = new Dictionary<ulong, T>(_hashtable);
            clone._xLowerBound = _xLowerBound;
            clone._xUpperBound = _xUpperBound;
            clone._yLowerBound = _yLowerBound;
            clone._yUpperBound = _yUpperBound;
            return clone;
        }

        public int Count { get { return _hashtable.Count; } }

        public int XLowerBound => _xLowerBound;

        public int XUpperBound => _xUpperBound;

        public int YLowerBound => _yLowerBound;

        public int YUpperBound => _yUpperBound;

        public T this[uint x, uint y]
        {
            get
            {
                if (_hashtable.TryGetValue(((UInt64)x << 32) | y, out T value))
                    return value;
                return default(T);
            }
            set
            {
                lock (_hashtable)
                {
                    _hashtable[((UInt64)x << 32) | y] = value;
                    if (_xLowerBound > (int)x)
                        _xLowerBound = (int)x;
                    if (_xUpperBound < (int)x)
                        _xUpperBound = (int)x;
                    if (_yLowerBound > (int)y)
                        _yLowerBound = (int)y;
                    if (_yUpperBound < (int)y)
                        _yUpperBound = (int)y;
                }
            }
        }

        public bool HasValueAt(uint x, uint y)
        {
            return _hashtable.ContainsKey(((UInt64)x << 32) | y);
        }

        public void Remove(uint x, uint y)
        {
            lock (_hashtable)
            {
                _hashtable.Remove(((UInt64)x << 32) | y);
            }
        }

        public void RemoveRowColumn(uint x, uint y)
        {
            lock (_hashtable)
            {
                //recreate keys after x and y
                var keys = _hashtable.Keys.ToList();
                keys.Sort();
                foreach (var key in keys)
                {
                    uint x2 = (uint)(key >> 32);
                    uint y2 = (uint)key;
                    if (x2 == x || y2 == y)
                    {
                        _hashtable.Remove(key);
                    }
                    else if (x2 > x && y2 > y)
                    {
                        T value = _hashtable[key];
                        _hashtable.Remove(key);
                        _hashtable[((UInt64)(x2 - 1) << 32) | (y2 - 1)] = value;
                    }
                    else if (x2 > x)
                    {
                        T value = _hashtable[key];
                        _hashtable.Remove(key);
                        _hashtable[((UInt64)(x2 - 1) << 32) | y2] = value;
                    }
                    else if (y2 > y)
                    {
                        T value = _hashtable[key];
                        _hashtable.Remove(key);
                        _hashtable[((UInt64)x2 << 32) | (y2 - 1)] = value;
                    }
                }

                if (_xUpperBound >= (int)x)
                    _xUpperBound--;
                if (_yUpperBound > (int)y)
                    _yUpperBound--;
            }
        }

        public void Clear()
        {
            _hashtable.Clear();
            _xLowerBound = 0;
            _xUpperBound = -1;
            _yLowerBound = 0;
            _yUpperBound = -1;
        }
    }
}
