using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Mono.Cecil;
using Mono.Cecil.Cil;
using Mono.Cecil.Rocks;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Fody;

namespace CompileTimeWeaver.Fody
{
    /// <summary>
    /// Weave method with AdviceAttributes
    /// </summary>
    internal partial class MethodWeaver
    {
        private readonly ReferenceFinder _refFinder;
        private readonly TypeReference _callContextTypeRef;
        private readonly TypeReference _invocationBaseTypeRef;

        public MethodWeaver(ModuleWeaver moduleWeaver, TypeReference callContextTypeRef, TypeReference invocationBaseTypeRef)
        {
            _refFinder = new ReferenceFinder(moduleWeaver);
            _callContextTypeRef = callContextTypeRef;
            _invocationBaseTypeRef = invocationBaseTypeRef;
        }

        //For old version Foday: variable has no name now.
        //private static VariableDefinition AddVariableDefinition(MethodDefinition method, string variableName, TypeReference variableType)
        //{
        //    var variableDefinition = method.Body.Variables.FirstOrDefault(x => x.Name == variableName);
        //    if (variableDefinition == null)
        //    {
        //        variableDefinition = new VariableDefinition(variableName, variableType);
        //        method.Body.Variables.Add(variableDefinition);
        //    }
        //    return variableDefinition;
        //}

        private static VariableDefinition AddVariableDefinition(MethodDefinition method, string variableName, TypeReference variableType)
        {
            //Debug.WriteLine($"Adding variable {method.Name}.{variableName}");
            var variableDefinition = new VariableDefinition(variableType);
            method.Body.Variables.Add(variableDefinition);
            return variableDefinition;
        }

        /// <summary>
        /// Rewrite type.method as this pattern:
        ///         public int Add<MT>(int x, short y, string dummy, out int z, MT t)
        ///         {
        ///             if (CallContext.LogicalGetData("Add_Invocation_1`1") == null)
        ///             {
        ///                var args = new object[] { x, y, dummy, default(int), t };
        ///                var invocation = new Add_Invocation_1<MT>(this, MethodBase.GetCurrentMethod(), args);
        ///                var result = (int)invocation.Proceed();
        ///                z = (int)args[3];
        ///                return result;
        ///            }
        ///
        ///            //your Code - int Add<MT>(int x, short y, string dummy, out int z)");
        ///            z = x + y;
        ///            return x + y;
        ///         }
        /// </summary>
        /// <param name="type"></param>
        /// <param name="method"></param>
        /// <param name="attributes"></param>
        internal void WeaveWithAdviceAttributes(TypeDefinition type, MethodDefinition method)
        {
            if (method.IsConstructor)
            {
                var initMethod = SplitCtor(method);
                WeaveWithAdviceAttributes(type, initMethod);
            }
            else
            {
                var methodBaseTypeRef = _refFinder.GetTypeReference(typeof(System.Reflection.MethodBase));
                var objectTypeRef = _refFinder.GetTypeReference(typeof(object));

                method.Body.SimplifyMacros();
                method.Body.InitLocals = true;

                //first instruction before which to insert new code
                var methodBodyFirstInstruction = method.Body.Instructions.First();

                //define two local fields
                var methodBaseVar = AddVariableDefinition(method, "methodBaseVar", methodBaseTypeRef);
                var argsVar = AddVariableDefinition(method, "argsVar", new ArrayType(objectTypeRef));

                //create nested Add_Invocation_1 class
                TypeReference invocation = CreateMethodInvocationType(type, method);
                var invocationGenericTypeArgs = invocation.HasGenericParameters
                                                    ? invocation.GenericParameters.Select(igp => type.GenericParameters.Concat(method.GenericParameters)
                                                                                                     .First(g => g.Name == igp.Name))
                                                                .Cast<TypeReference>()
                                                                .ToArray()
                                                    : new TypeReference[0];

                var il = method.Body.GetILProcessor();

                //if (CallContext.LogicalGetData("Add_Invocation_1") == null)
                var instructions = new List<Instruction>()
                {
                    il.Create(OpCodes.Ldstr, invocation.Name),
                    //il.Create(OpCodes.Call, _refFinder.GetMethodReference(_refFinder.GetTypeReference(typeof (CallContext)), m => m.Name == "LogicalGetData")),
                    il.Create(OpCodes.Call, _refFinder.GetMethodReference(_callContextTypeRef, m => m.Name == "LogicalGetData")),
                    il.Create(OpCodes.Brtrue, methodBodyFirstInstruction),
                };

                // MethodBase method = MethodBase.GetMethodFromHandle(__methodref (Sample.Method), __typeref (Sample))
                instructions.AddRange(method.GetMethodBaseAsLocalVariable(methodBaseVar, _refFinder));

                // object[] args = new object[1] { (object) value };
                instructions.AddRange(method.CreateArgsArrayAsLocalVariable(argsVar, _refFinder, _invocationBaseTypeRef));

                //var invocation = new Invocation<T1,T2,...>(this, methodBaseVar, argsVar);
                if (method.IsStatic)
                {
                    instructions.Add(il.Create(OpCodes.Ldnull)); //null
                }
                else
                {
                    instructions.Add(il.Create(OpCodes.Ldarg_0)); //this
                    if (method.DeclaringType.IsValueType) //load value object if *this is ValueType
                    {
                        TypeReference thisTypeRef = type.HasGenericParameters
                            ? (TypeReference) type.MakeGenericInstanceType(type.GenericParameters.Cast<TypeReference>().ToArray())
                            : type;
                        instructions.Add(il.Create(OpCodes.Ldobj, thisTypeRef));
                        instructions.Add(il.Create(OpCodes.Box, thisTypeRef));
                    }
                }
                instructions.Add(il.Create(OpCodes.Ldloc, methodBaseVar));
                instructions.Add(il.Create(OpCodes.Ldloc, argsVar));
                instructions.Add(il.Create(OpCodes.Newobj, _refFinder.GetFirstConstructorReference(invocation).MakeHostInstanceGeneric(invocationGenericTypeArgs)));
                if (!IsAsyncMethod(method))
                {
                    //invocation.Proceed();
                    instructions.Add(il.Create(OpCodes.Call, _refFinder.GetMethodReference(invocation, m => m.Name == "Proceed")));
                    if (method.ReturnType.FullName == "System.Void")
                    {
                        instructions.Add(il.Create(OpCodes.Pop));
                    }
                    else
                    {
                        instructions.Add(method.ReturnType.IsValueType || method.ReturnType.IsGenericParameter
                            ? il.Create(OpCodes.Unbox_Any, method.ReturnType)
                            : il.Create(OpCodes.Castclass, method.ReturnType));
                    }
                }
                else
                {
                    //x = invocation.ProceedAsync();
                    instructions.Add(il.Create(OpCodes.Call, _refFinder.GetMethodReference(invocation, m => m.Name == "ProceedAsync")));
                    if (method.ReturnType.IsGenericInstance)
                    {
                        //Task<T> InvocationBase.Convert1<T>(Task<object> task), for example:
                        //To weave method async Task<int> AddAsyncWithReturn(int x, short y, string dumy), T is int the return type in Task<>
                        instructions.Add(il.Create(OpCodes.Call, _refFinder.GetMethodReference(invocation, m => m.Name.StartsWith("Convert1"))
                                                                           .MakeGenericInstanceMethod(((GenericInstanceType)method.ReturnType).GenericArguments[0])));
                    }
                    else
                    {
                        //Task InvocationBase.Convert4(Task<object> task), for example:
                        //To weave method async Task HelloAsyncNoReturn(int x, short y, string dumy)
                        instructions.Add(il.Create(OpCodes.Call, _refFinder.GetMethodReference(invocation, m => m.Name.StartsWith("Convert4"))));
                    }
                }

                //update ref/out parameters with values in args array
                for (int i = 0; i < method.Parameters.Count; i++)
                {
                    if (method.Parameters[i].ParameterType.IsByReference)
                    {
                        instructions.Add(il.Create(OpCodes.Ldarg, method.IsStatic ? i : i + 1)); //z1
                        //(int)args[i]
                        instructions.Add(il.Create(OpCodes.Ldloc, argsVar)); //args
                        instructions.Add(il.Create(OpCodes.Ldc_I4, i));
                        instructions.Add(il.Create(OpCodes.Ldelem_Ref));

                        var parameterElemType = ((TypeSpecification) method.Parameters[i].ParameterType).ElementType;
                        //cast args[i] from object type to parameters[i]'s type: int *z1 = (int)args[i]
                        if (parameterElemType.IsValueType)
                        {
                            instructions.Add(il.Create(OpCodes.Unbox_Any, parameterElemType));
                        }
                        else if (parameterElemType.IsGenericInstance)
                        {
                            instructions.Add(il.Create(OpCodes.Unbox_Any, parameterElemType));
                            //var genericParam = method.GetGenericArgumentType(paramGenericTypeName);
                            //instructions.Add(il.Create(OpCodes.Unbox_Any, genericParam));
                        }
                        else if (parameterElemType.FullName != "System.Object")
                        {
                            instructions.Add(il.Create(OpCodes.Castclass, parameterElemType));
                        }

                        //*z1 = (int)args[i]
                        instructions.Add(CreateStInd((TypeSpecification) method.Parameters[i].ParameterType));
                    }
                }

                instructions.Add(il.Create(OpCodes.Ret));

                il.InsertBefore(methodBodyFirstInstruction, instructions);

                method.Body.OptimizeMacros();
            }
        }

        /// <summary>
        /// Split ctor to a private initMethod, and ctor calls initMethod
        /// </summary>
        /// <param name="ctor"></param>
        /// <returns>New initMetod</returns>
        private MethodDefinition SplitCtor(MethodDefinition ctor)
        {
            if (!ctor.IsConstructor)
                throw new WeavingException("Not .ctor");

            //Find first instruction after call base constructor
            var methodBodyFirstInstruction = ctor.Body.Instructions.First();
            if (!ctor.DeclaringType.IsValueType && ctor.Body.Instructions.Any(i => i.OpCode == OpCodes.Call))
            {
                methodBodyFirstInstruction = ctor.Body.Instructions.First(i => i.OpCode == OpCodes.Call).Next;
            }

            var id = ctor.GetHashCode();

            var initMethod = new MethodDefinition($"<.ctor>_{id}", ctor.Attributes, ctor.ReturnType);
            initMethod.IsSpecialName = false;
            initMethod.IsRuntimeSpecialName = false;
            initMethod.IsPrivate = true;
            ctor.DeclaringType.Methods.Add(initMethod);
            ctor.CopyParameters(initMethod);
            //ctor.CopyGenericParameters(initMethod); this line is unnecessary because constructor doesn't have generic type
            ctor.CopyCustomAttributes(initMethod);
            ctor.CustomAttributes.Clear();
            ctor.Body.SimplifyMacros();
            initMethod.Body = new MethodBody(initMethod);
            initMethod.Body.SimplifyMacros();

            //Move all local variables from ctor to initMethod
            foreach (var variable in ctor.Body.Variables)
            {
                initMethod.Body.InitLocals = true;
                ctor.Body.InitLocals = true;
                //TODO: initMethod.Body.Variables.Add(new VariableDefinition(variable.Name, variable.VariableType));
                initMethod.Body.Variables.Add(new VariableDefinition(variable.VariableType));
            }
            ctor.Body.Variables.Clear();

            //move instructions below call base.ctor from ctor body to initMethod body
            var initMethodCode = initMethod.Body.Instructions;
            var ctorCode = ctor.Body.Instructions;
            for (int i = 0; i < ctorCode.Count;)
            {
                if (ctorCode[i] != methodBodyFirstInstruction)
                {
                    i++;
                    continue;
                }

                for (; i < ctorCode.Count;)
                {
                    initMethodCode.Add(ctorCode[i]);
                    ctorCode.RemoveAt(i);
                }
                break;
            }

            MethodReference initMethodRef = initMethod;
            if (initMethodRef.DeclaringType.HasGenericParameters)
            {
                //make init method be of generic target type
                var typeGenericParameters = initMethodRef.DeclaringType.GenericParameters
                                                                 .Cast<TypeReference>()
                                                                 .ToArray();
                initMethodRef = initMethod.MakeHostInstanceGeneric(typeGenericParameters);
            }

            //let ctor call initMethod
            ctorCode.Add(Instruction.Create(OpCodes.Ldarg_0));
            for (int i = 0; i < ctor.Parameters.Count; i++)
            {
                ctorCode.Add(Instruction.Create(OpCodes.Ldarg, ctor.Parameters[i]));
            }
            ctorCode.Add(Instruction.Create(OpCodes.Call, initMethodRef));
            ctorCode.Add(Instruction.Create(OpCodes.Ret));

            //Move all exception handers from ctor to initMethod
            foreach (var eh in ctor.Body.ExceptionHandlers)
            {
                initMethod.Body.ExceptionHandlers.Add(eh);
            }
            ctor.Body.ExceptionHandlers.Clear();

            ctor.Body.OptimizeMacros();
            initMethod.Body.OptimizeMacros();
            return initMethod;
        }

        private Instruction CreateStInd(TypeSpecification referencedTypeSpec)
        {
            switch (referencedTypeSpec.ElementType.MetadataType)
            {
                //Indirect load value of type int8 as int32 on the stack
                case MetadataType.Boolean:
                case MetadataType.SByte:
                case MetadataType.Byte:
                    return Instruction.Create(OpCodes.Stind_I1);   //z1
                // Indirect load value of type int16 as int32 on the stack
                case MetadataType.Int16:
                case MetadataType.UInt16:
                case MetadataType.Char:
                    return Instruction.Create(OpCodes.Stind_I2);   //z1
                // Indirect load value of type int32 as int32 on the stack
                case MetadataType.Int32:
                case MetadataType.UInt32:
                    return Instruction.Create(OpCodes.Stind_I4);   //z1
                // Indirect load value of type int64 as int64 on the stack
                // Indirect load value of type unsigned int64 as int64 on the stack (alias for ldind.i8)
                case MetadataType.Int64:
                case MetadataType.UInt64:
                    return Instruction.Create(OpCodes.Stind_I8);   //z1
                // Indirect load value of type float32 as F on the stack
                case MetadataType.Single:
                    return Instruction.Create(OpCodes.Stind_R4);   //z1
                // Indirect load value of type float64 as F on the stack
                case MetadataType.Double:
                    return Instruction.Create(OpCodes.Stind_R8);   //z1
                // Indirect load value of type native int as native int on the stack
                case MetadataType.IntPtr:
                case MetadataType.UIntPtr:
                    return Instruction.Create(OpCodes.Stind_I);   //z1
                default:
                    // Need to check if it is a value type instance, in which case
                    // we use Ldobj instruction to copy the contents of value type
                    // instance to stack and then box it
                    if (referencedTypeSpec.ElementType.IsValueType)
                    {
                        return Instruction.Create(OpCodes.Stobj, referencedTypeSpec.ElementType);   //z1
                    }
                    else
                    {
                        // It is a reference type so just use reference the pointer
                        return Instruction.Create(OpCodes.Stind_Ref);   //z1
                    }
            }
        }

        private bool IsAsyncMethod(MethodDefinition method)
        {
            var taskTypeRef = _refFinder.GetTypeReference(typeof(Task));
            var asyncAttr = _refFinder.GetTypeReference(typeof(AsyncStateMachineAttribute));
            return method.CustomAttributes.Any(x => x.AttributeType.FullName == asyncAttr.FullName) && method.ReturnType.DerivesFrom(taskTypeRef);
        }

        #region For creating Invocation class

        /// <summary>
        /// Create a nested derived method invocation class as this pattern:
        /// private sealed class Method1Invocation<T> : InvocationBase
        /// {
        ///     public Method1Invocation(NoAdivceStruct<T> instance, MethodBase method, object[] args)
        ///         : base((object)instance, method, args)
        ///     {
        ///     }
        /// 
        ///     protected override object Invoke()
        ///     {
        ///         ((NoAdivceStruct<T>)Instance).Method146763000((string)Args[0], (string)Args[1]);
        ///         return null;
        ///     }
        /// }
        /// </summary>
        /// <param name="targetType"></param>
        /// <param name="targetMethod"></param>
        /// <returns></returns>
        private TypeDefinition CreateMethodInvocationType(TypeDefinition targetType, MethodDefinition targetMethod)
        {
            var objectTypeRef = _refFinder.GetTypeReference(typeof(object));
            var id = targetMethod.GetHashCode();    // Guid.NewGuid().ToString().Replace("-", string.Empty);

            //generate Invocation nested class for this method
            var innerType = new TypeDefinition(targetType.Namespace, $"<{targetMethod.Name}>Invocation_{id}",
                                               TypeAttributes.NestedPrivate | TypeAttributes.Sealed | TypeAttributes.Class,
                                               _invocationBaseTypeRef);
            targetType.NestedTypes.Add(innerType);

            //let Invocation nested class bear all the generic paramenters that target type and method have
            targetType.CopyGenericParameters(innerType);
            targetMethod.CopyGenericParameters(innerType);
            if (targetMethod.GenericParameters.Count > 0)
                innerType.Name = $"{innerType.Name}`{targetMethod.GenericParameters.Count}";

            //make target generic instance if it is generic type
            TypeReference targetTypeRef = targetType;
            if (targetType.HasGenericParameters)
                targetTypeRef = targetTypeRef.MakeGenericInstanceType(targetType.GenericParameters.Cast<TypeReference>().ToArray());
            // add constructor
            AddInvocationConstructor(innerType,
                                     new ParameterDefinition("instance", ParameterAttributes.None, objectTypeRef),
                                     new ParameterDefinition("method", ParameterAttributes.None, _refFinder.GetTypeReference(typeof (System.Reflection.MethodBase))),
                                     new ParameterDefinition("args", ParameterAttributes.None, new ArrayType(objectTypeRef)));

            if (IsAsyncMethod(targetMethod))
            {
                AddInvokeAsyncMethod(innerType, targetMethod);
            }
            else
            {
                AddInvokeMethod(innerType, targetMethod);
            }

            //targetType.Module.ImportReference(innerType);
            return innerType;
        }

        private MethodDefinition AddInvocationConstructor(TypeDefinition innerType, params ParameterDefinition[] parameters)
        {
            var ctor = new MethodDefinition(".ctor",
                                            MethodAttributes.Public | MethodAttributes.HideBySig | MethodAttributes.SpecialName | MethodAttributes.RTSpecialName,
                                            innerType.Module.TypeSystem.Void);

            foreach (var parameter in parameters)
            {
                ctor.Parameters.Add(parameter);
            }

            ctor.Body.MaxStackSize = 8;
            //call InvocationBase.ctor(this, (object)instance, methodBase, args)
            ctor.Body.Instructions.Add(Instruction.Create(OpCodes.Ldarg_0));
            ctor.Body.Instructions.Add(Instruction.Create(OpCodes.Ldarg_1));
            if (parameters[0].ParameterType.IsValueType)
                ctor.Body.Instructions.Add(Instruction.Create(OpCodes.Box, parameters[0].ParameterType));
            ctor.Body.Instructions.Add(Instruction.Create(OpCodes.Ldarg_2));
            ctor.Body.Instructions.Add(Instruction.Create(OpCodes.Ldarg_3));
            ctor.Body.Instructions.Add(Instruction.Create(OpCodes.Call, _refFinder.GetFirstConstructorReference(innerType.BaseType)));  //call InvocationBase.ctor()
            ctor.Body.Instructions.Add(Instruction.Create(OpCodes.Ret));

            innerType.Methods.Add(ctor);
            return ctor;
        }

        /// <summary>
        /// create Invoke method of this pattern:
        /// protected override async Task<object> Invoke()
        /// {
        ///      CallContext.LogicalSetData("Reenter", true);
        ///      return await ((Sample)Instance).AddAsync((int)Args[0], (int)Args[1]);
        /// }
        /// </summary>
        /// <param name="innerType"></param>
        /// <param name="targetMethod"></param>
        /// <returns></returns>
        private MethodDefinition AddInvokeAsyncMethod(TypeDefinition innerType, MethodDefinition targetMethod)
        {
            var objectTypeRef = _refFinder.GetTypeReference(typeof(object));
            var returnType = _refFinder.GetTypeReference(typeof(Task<>)).MakeGenericInstanceType(objectTypeRef);
            return AddInvocationMethod(innerType, "InvokeAsync", returnType, new ParameterDefinition[0],
                                invokeMethod =>
                                {
                                    //var id = targetMethod.GetHashCode();// Guid.NewGuid().ToString().Replace("-", string.Empty);
                                    //var callContextTypeRef = _refFinder.GetTypeReference(typeof(CallContext));

                                    MethodReference targetMethodRef = targetMethod;
                                    //make generic target type
                                    if (targetMethod.DeclaringType.HasGenericParameters)
                                    {
                                        var targetTypeGenericParameters = targetMethod.DeclaringType.GenericParameters
                                                                                         .Select(targetTypeGp => innerType.GenericParameters.Last(innerTypeGp => innerTypeGp.Name == targetTypeGp.Name))
                                                                                         .Cast<TypeReference>()
                                                                                         .ToArray();
                                        targetMethodRef = targetMethodRef.MakeHostInstanceGeneric(targetTypeGenericParameters);
                                    }
                                    //make targetMethod generic instance method with innerType's generic arguments if targetMethod is generic method
                                    if (targetMethod.HasGenericParameters)
                                    {
                                        targetMethodRef = targetMethodRef.MakeGenericInstanceMethod(
                                            targetMethod.GenericParameters
                                                        .Select(targetMethodGp => innerType.GenericParameters.Last(innerTypeGp => innerTypeGp.Name == targetMethodGp.Name))
                                                        .Cast<TypeReference>()
                                                        .ToArray());
                                        //Trace.WriteLine(((GenericInstanceMethod)targetMethodRef).GenericArguments.Count());
                                    }

                                    var x = invokeMethod.Body.GetILProcessor();
                                    var instructions = invokeMethod.Body.Instructions;

                                    //create variables for out/ref args
                                    var outVariables = new VariableDefinition[targetMethod.Parameters.Count];

                                    invokeMethod.Body.InitLocals = true;

                                    //object[] argsVar = this.get_Args();
                                    var argsVar = AddVariableDefinition(invokeMethod, "argsVar", new ArrayType(objectTypeRef));
                                    instructions.Add(x.Create(OpCodes.Ldarg_0));
                                    instructions.Add(x.Create(OpCodes.Call, _refFinder.GetMethodReference(innerType.BaseType, m => m.Name == "get_Args")));
                                    instructions.Add(x.Create(OpCodes.Stloc, argsVar));

                                    if (!targetMethod.IsConstructor && !targetMethod.IsStatic)
                                    {
                                        //load instance: (Sample)base.get_Instance
                                        instructions.Add(x.Create(OpCodes.Ldarg_0)); //this
                                        instructions.Add(x.Create(OpCodes.Call, _refFinder.GetMethodReference(innerType.BaseType, m => m.Name == "get_Instance")));
                                        if (!targetMethod.DeclaringType.IsValueType)
                                        {
                                            //and cast instance: (Sample)base.get_Instance
                                            instructions.Add(x.Create(OpCodes.Castclass, targetMethodRef.DeclaringType));
                                        }
                                        else
                                        {
                                            //and unbox instance: (Sample)base.get_Instance
                                            instructions.Add(x.Create(OpCodes.Unbox_Any, targetMethodRef.DeclaringType));
                                            var instanceVar = AddVariableDefinition(invokeMethod, "instanceVar", targetMethodRef.DeclaringType);
                                            instructions.Add(x.Create(OpCodes.Stloc, instanceVar));
                                            instructions.Add(x.Create(OpCodes.Ldloca, instanceVar));    //load &instance because it is value type
                                        }
                                    }
                                    //load parameters into stack
                                    for (int i = 0; i < targetMethodRef.Parameters.Count; i++)
                                    {
                                        instructions.Add(x.Create(OpCodes.Ldloc, argsVar));
                                        instructions.Add(x.Create(OpCodes.Ldc_I4, i));
                                        instructions.Add(x.Create(OpCodes.Ldelem_Ref));
                                        //store ref/out args into local variable
                                        if (targetMethodRef.Parameters[i].ParameterType.IsByReference)
                                        {
                                            //int z = (int)argVar[3]
                                            var parameterElemType = ((TypeSpecification)targetMethodRef.Parameters[i].ParameterType).ElementType;
                                            if (parameterElemType.IsValueType)
                                            {
                                                instructions.Add(x.Create(OpCodes.Unbox_Any, parameterElemType));
                                            }
                                            else if (parameterElemType.IsGenericParameter)
                                            {
                                                var paramGenericTypeName = parameterElemType.Name;
                                                var genericParam = targetMethodRef.GetGenericArgumentType(paramGenericTypeName);
                                                instructions.Add(x.Create(OpCodes.Unbox_Any, genericParam));
                                            }
                                            else
                                            {
                                                instructions.Add(x.Create(OpCodes.Castclass, parameterElemType));
                                            }

                                            outVariables[i] = AddVariableDefinition(invokeMethod, $"outArg{i}", parameterElemType);
                                            instructions.Add(x.Create(OpCodes.Stloc, outVariables[i]));
                                            //load &z into stack
                                            instructions.Add(x.Create(OpCodes.Ldloca, outVariables[i]));
                                        }
                                        else
                                        {
                                            if (targetMethodRef.Parameters[i].ParameterType.IsValueType)
                                            {
                                                instructions.Add(x.Create(OpCodes.Unbox_Any, targetMethodRef.Parameters[i].ParameterType));
                                            }
                                            else if (targetMethodRef.Parameters[i].ParameterType.IsGenericParameter)
                                            {
                                                var paramGenericTypeName = targetMethodRef.Parameters[i].ParameterType.Name;
                                                var genericParam = targetMethodRef.GetGenericArgumentType(paramGenericTypeName);
                                                instructions.Add(x.Create(OpCodes.Unbox_Any, genericParam));
                                            }
                                            else
                                            {
                                                instructions.Add(x.Create(OpCodes.Castclass, targetMethodRef.Parameters[i].ParameterType));
                                            }
                                        }
                                    }

                                    // call ((T)_instance).Add((int)argVar[0], (int)argVar[1], out z1, out z2), and leave return value in stack
                                    instructions.Add(x.Create(targetMethod.IsConstructor | targetMethod.IsStatic | targetMethod.DeclaringType.IsValueType ? OpCodes.Call : OpCodes.Callvirt, targetMethodRef));

                                    //update ref/out args with local variables
                                    for (int i = 0; i < targetMethodRef.Parameters.Count; i++)
                                    {
                                        if (targetMethodRef.Parameters[i].ParameterType.IsByReference)
                                        {
                                            //argVar[i] = (object)z1
                                            instructions.Add(x.Create(OpCodes.Ldloc, argsVar));
                                            instructions.Add(x.Create(OpCodes.Ldc_I4, i));
                                            instructions.Add(x.Create(OpCodes.Ldloc, outVariables[i]));
                                            var parameterElemType = ((TypeSpecification)targetMethodRef.Parameters[i].ParameterType).ElementType;
                                            if (parameterElemType.IsValueType)
                                            {
                                                instructions.Add(x.Create(OpCodes.Box, parameterElemType));
                                            }
                                            else if (parameterElemType.IsGenericParameter)
                                            {
                                                var paramGenericTypeName = targetMethodRef.Parameters[i].ParameterType.Name;
                                                var genericParam = targetMethodRef.GetGenericArgumentType(paramGenericTypeName);

                                                instructions.Add(x.Create(OpCodes.Box, genericParam));
                                            }
                                            instructions.Add(x.Create(OpCodes.Stelem_Ref));
                                        }
                                    }

                                    if (targetMethodRef.ReturnType.IsGenericInstance)   //Target method returns Task<T>
                                    {
                                        //I cast target method return value to Task<object> by "Task<object> InvocationBase.Convert2<T>(Task<T>)"
                                        var convert2 = _refFinder.GetMethodReference(innerType.BaseType, m => m.Name.StartsWith("Convert2"));

                                        var convert2GenericArgType = ((GenericInstanceType) targetMethodRef.ReturnType).GenericArguments[0];
                                        if (convert2GenericArgType.IsGenericParameter)
                                        {
                                            convert2GenericArgType = targetMethodRef.GetGenericArgumentType(convert2GenericArgType.Name);
                                        }
                                        convert2 = convert2.MakeGenericInstanceMethod(convert2GenericArgType);

                                        instructions.Add(x.Create(OpCodes.Call, convert2));
                                    }
                                    else   //Target method returns Task
                                    {
                                        //I cast target method return value to Task<object> by "Task<object> InvocationBase.Convert3(Task)"
                                        instructions.Add(x.Create(OpCodes.Call, _refFinder.GetMethodReference(innerType.BaseType, m => m.Name.StartsWith("Convert3"))));
                                    }

                                    instructions.Add(x.Create(OpCodes.Ret));
                                });
        }

        /// <summary>
        /// create Invoke method of this pattern:
        /// protected override object Invoke()
        /// {
        ///      CallContext.LogicalSetData("Reenter", true);
        ///      return ((Sample) Instance).Add((int) Args[0], (int) Args[1]);
        /// }
        /// </summary>
        /// <param name="innerType"></param>
        /// <param name="targetMethod"></param>
        /// <returns></returns>
        private MethodDefinition AddInvokeMethod(TypeDefinition innerType, MethodDefinition targetMethod)
        {
            var objectTypeRef = _refFinder.GetTypeReference(typeof(object));
            var returnType = objectTypeRef;
            return AddInvocationMethod(innerType, "Invoke", returnType, new ParameterDefinition[0],
                                invokeMethod =>
                                {
                                    //var id = targetMethod.GetHashCode();// Guid.NewGuid().ToString().Replace("-", string.Empty);
                                    //var callContextTypeRef = _refFinder.GetTypeReference(typeof(CallContext));
                                    
                                    //make generic target type
                                    MethodReference targetMethodRef = targetMethod;
                                    if (targetMethodRef.DeclaringType.HasGenericParameters)
                                    {
                                        var targetTypeGenericParameters = targetMethodRef.DeclaringType.GenericParameters
                                                                                         .Select(targetTypeGp => innerType.GenericParameters.Last(innerTypeGp => innerTypeGp.Name == targetTypeGp.Name))
                                                                                         .Cast<TypeReference>()
                                                                                         .ToArray();
                                        targetMethodRef = targetMethod.MakeHostInstanceGeneric(targetTypeGenericParameters);
                                    }
                                    //make targetMethod generic instance method with innerType's generic arguments if targetMethod is generic method
                                    if (targetMethod.HasGenericParameters)
                                    {
                                        targetMethodRef = targetMethodRef.MakeGenericInstanceMethod(
                                            targetMethod.GenericParameters
                                                        .Select(targetMethodGp => innerType.GenericParameters.Last(innerTypeGp => innerTypeGp.Name == targetMethodGp.Name))
                                                        .Cast<TypeReference>()
                                                        .ToArray());
                                        //Trace.WriteLine(((GenericInstanceMethod)targetMethodRef).GenericArguments.Count());
                                    }

                                    var x = invokeMethod.Body.GetILProcessor();
                                    var instructions = invokeMethod.Body.Instructions;

                                    //create variables for out/ref args
                                    var outVariables = new VariableDefinition[targetMethod.Parameters.Count];

                                    invokeMethod.Body.InitLocals = true;

                                    ////CallContext.LogicalSetData("Reenter", true);
                                    //instructions.Add(x.Create(OpCodes.Ldstr, $"Reenter_{id}"));
                                    //instructions.Add(x.Create(OpCodes.Ldc_I4_1));
                                    //instructions.Add(x.Create(OpCodes.Box, _refFinder.GetTypeReference(typeof(bool))));
                                    //instructions.Add(x.Create(OpCodes.Call, _refFinder.GetMethodReference(callContextTypeRef, m => m.Name == "LogicalSetData")));

                                    //object[] argsVar = this.get_Args();
                                    var argsVar = AddVariableDefinition(invokeMethod, "argsVar", new ArrayType(objectTypeRef));
                                    instructions.Add(x.Create(OpCodes.Ldarg_0));
                                    instructions.Add(x.Create(OpCodes.Call, _refFinder.GetMethodReference(innerType.BaseType, m => m.Name == "get_Args")));
                                    instructions.Add(x.Create(OpCodes.Stloc, argsVar));

                                    if (!targetMethod.IsConstructor && !targetMethod.IsStatic)
                                    {
                                        //load instance: base.get_Instance
                                        instructions.Add(x.Create(OpCodes.Ldarg_0)); //this
                                        instructions.Add(x.Create(OpCodes.Call, _refFinder.GetMethodReference(innerType.BaseType, m => m.Name == "get_Instance")));
                                        if (!targetMethod.DeclaringType.IsValueType)
                                        {
                                            // and cast instance: (Sample)base.get_Instance
                                            instructions.Add(x.Create(OpCodes.Castclass, targetMethodRef.DeclaringType));
                                        }
                                        else
                                        {
                                            // and unbox instance: (Sample)base.get_Instance
                                            instructions.Add(x.Create(OpCodes.Unbox_Any, targetMethodRef.DeclaringType));
                                            var instanceVar = AddVariableDefinition(invokeMethod, "instanceVar", targetMethodRef.DeclaringType);
                                            instructions.Add(x.Create(OpCodes.Stloc, instanceVar));
                                            instructions.Add(x.Create(OpCodes.Ldloca, instanceVar));    //load &instance because it is value type
                                        }
                                    }
                                    
                                    //load parameters into stack
                                    for (int i = 0; i < targetMethodRef.Parameters.Count; i++)
                                    {
                                        //load argsVar[i]
                                        instructions.Add(x.Create(OpCodes.Ldloc, argsVar));
                                        instructions.Add(x.Create(OpCodes.Ldc_I4, i));
                                        instructions.Add(x.Create(OpCodes.Ldelem_Ref));
                                        //store ref/out argsVar[i] into local variable
                                        if (targetMethodRef.Parameters[i].ParameterType.IsByReference)
                                        {
                                            //int z = (int)argVar[i]
                                            var parameterElemType = ((TypeSpecification)targetMethodRef.Parameters[i].ParameterType).ElementType;
                                            if (parameterElemType.IsValueType)
                                            {
                                                instructions.Add(x.Create(OpCodes.Unbox_Any, parameterElemType));
                                            }
                                            else if (parameterElemType.IsGenericParameter)
                                            {
                                                var paramGenericTypeName = parameterElemType.Name;
                                                var genericParam = targetMethodRef.GetGenericArgumentType(paramGenericTypeName);
                                                instructions.Add(x.Create(OpCodes.Unbox_Any, genericParam));
                                            }
                                            else
                                            {
                                                instructions.Add(x.Create(OpCodes.Castclass, parameterElemType));
                                            }

                                            outVariables[i] = AddVariableDefinition(invokeMethod, $"outArg{i}", parameterElemType);
                                            instructions.Add(x.Create(OpCodes.Stloc, outVariables[i]));
                                            //load &z into stack
                                            instructions.Add(x.Create(OpCodes.Ldloca, outVariables[i]));
                                        }
                                        else
                                        {
                                            if (targetMethodRef.Parameters[i].ParameterType.IsValueType)
                                            {
                                                instructions.Add(x.Create(OpCodes.Unbox_Any, targetMethodRef.Parameters[i].ParameterType));
                                            }
                                            else if (targetMethodRef.Parameters[i].ParameterType.IsGenericParameter)
                                            {
                                                var paramGenericTypeName = targetMethodRef.Parameters[i].ParameterType.Name;
                                                var genericParam = targetMethodRef.GetGenericArgumentType(paramGenericTypeName);
                                                instructions.Add(x.Create(OpCodes.Unbox_Any, genericParam));
                                            }
                                            else
                                            {
                                                instructions.Add(x.Create(OpCodes.Castclass, targetMethodRef.Parameters[i].ParameterType));
                                            }
                                        }
                                    }

                                    // call ((T)_instance).Add((int)argVar[0], (int)argVar[1], out z1, out z2), and leave return value in stack
                                    instructions.Add(x.Create(targetMethod.IsConstructor | targetMethod.IsStatic | targetMethod.DeclaringType.IsValueType ? OpCodes.Call : OpCodes.Callvirt, targetMethodRef));

                                    //update ref/out args with local variables
                                    for (int i = 0; i < targetMethodRef.Parameters.Count; i++)
                                    {
                                        if (targetMethodRef.Parameters[i].ParameterType.IsByReference)
                                        {
                                            //argVar[i] = (object)z1
                                            instructions.Add(x.Create(OpCodes.Ldloc, argsVar));
                                            instructions.Add(x.Create(OpCodes.Ldc_I4, i));
                                            instructions.Add(x.Create(OpCodes.Ldloc, outVariables[i]));
                                            var parameterElemType = ((TypeSpecification)targetMethodRef.Parameters[i].ParameterType).ElementType;
                                            if (parameterElemType.IsValueType)
                                            {
                                                instructions.Add(x.Create(OpCodes.Box, parameterElemType));
                                            }
                                            else if (parameterElemType.IsGenericParameter)
                                            {
                                                var paramGenericTypeName = targetMethodRef.Parameters[i].ParameterType.Name;
                                                var genericParam = targetMethodRef.GetGenericArgumentType(paramGenericTypeName);
                                                instructions.Add(x.Create(OpCodes.Box, genericParam));
                                            }
                                            instructions.Add(x.Create(OpCodes.Stelem_Ref));
                                        }
                                    }

                                    if (targetMethodRef.ReturnType.FullName == "System.Void")
                                    {
                                        //return null if targetMethod Instance.Add return type is void
                                        instructions.Add(x.Create(OpCodes.Ldnull));
                                    }
                                    else
                                    {
                                        //cast return value to object type: return (object)(...)
                                        if (targetMethodRef.ReturnType.IsValueType)
                                        {
                                            instructions.Add(x.Create(OpCodes.Box, targetMethodRef.ReturnType));
                                        }
                                        else if (targetMethodRef.ReturnType.IsGenericParameter)
                                        {
                                            instructions.Add(x.Create(OpCodes.Box, targetMethodRef.GetGenericArgumentType(targetMethodRef.ReturnType.Name)));
                                        }
                                    }
                                    instructions.Add(x.Create(OpCodes.Ret));
                                });
        }

        private static MethodDefinition AddInvocationMethod(TypeDefinition innerType, string name, TypeReference returnType,
            ParameterDefinition[] parameters,
            Action<MethodDefinition> buildBody)
        {
            var invokeMethod = new MethodDefinition(name,
                                            MethodAttributes.Family | MethodAttributes.HideBySig | MethodAttributes.Virtual,
                                            returnType);

            foreach (var parameter in parameters)
            {
                invokeMethod.Parameters.Add(parameter);
            }

            //method.Body.MaxStackSize = 4;
            invokeMethod.Body.InitLocals = true;

            buildBody(invokeMethod);

            invokeMethod.Body.OptimizeMacros();

            innerType.Methods.Add(invokeMethod);

            return invokeMethod;
        }

        #endregion
    }
}