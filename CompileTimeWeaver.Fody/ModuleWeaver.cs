﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using Fody;
using Mono.Cecil;

namespace CompileTimeWeaver.Fody
{
    public partial class ModuleWeaver : BaseModuleWeaver
    {
        private const string RuntimeDll = "CompileTimeWeaver";
        private TypeReference _iAdviceType;
        private string[] _additionalAdviceTypes;
        //private TypeReference _aspectAttributeType;
        private TypeReference _callContextType;
        private TypeReference _invocationBaseType;

        public ModuleWeaver()
        {
            //this.LogDebug = s => { Trace.WriteLine(s); };
            //this.LogInfo = s => { Trace.WriteLine(s); };
            //this.LogWarning = s => { Trace.WriteLine(s); };
            //this.LogError = s => { Trace.WriteLine(s); };
        }

        public override void Execute()
        {
            Trace.WriteLine("CompileTimeWeaver.Fody.ModuleWeaver executing...");
            //Debugger.Break();
#if DEBUG
            // try to resolve all references
            foreach (var x in this.ModuleDefinition.AssemblyReferences)
            {
                var assembly = this.ModuleDefinition.AssemblyResolver.Resolve(x);
                if (assembly == null)
                    this.WriteWarning($"Assembly {x.FullName} is missing.");
                else
                    this.WriteDebug($"Resolved assembly {x.FullName}");
            }
#endif

            _iAdviceType = this.ModuleDefinition.ImportReference(this.FindTypeDefinition("CompileTimeWeaver.IAdvice"));
            if (_iAdviceType == null)
                throw new WeavingException("CompileTimeWeaver assembly is not referenced.");

            //_aspectAttributeType = this.ModuleDefinition.ImportReference(this.FindType("CompileTimeWeaver.AspectAttribute"));
            _callContextType = this.ModuleDefinition.ImportReference(this.FindTypeDefinition("CompileTimeWeaver.CallContext"));
            _invocationBaseType = this.ModuleDefinition.ImportReference(this.FindTypeDefinition("CompileTimeWeaver.InvocationBase"));

            var adviceAttrs = this.Config?.Attribute("AdviceAttributes")?.Value ?? string.Empty;
            _additionalAdviceTypes = adviceAttrs.Split('|').Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();

            var methodWeaver = new MethodWeaver(this, _callContextType, _invocationBaseType);
            // Read all the top-level and nested types from this module
            var allTypes = this.ModuleDefinition.Types.SelectMany(GetAllTypes).ToList();
            WeaveTypesWithNotifyProperyChangedAttributes(allTypes);

            foreach (var type in allTypes)
            {
                WeaveTypeWithAdviceAttributes(methodWeaver, type);
            }
        }

        public override IEnumerable<string> GetAssembliesForScanning()
        {
            yield return "netstandard";
            yield return "mscorlib";
            yield return "System.Private.CoreLib";
            yield return "System";
            yield return "System.Runtime";
            yield return "System.Core";
            yield return "System.Collections";
            yield return "System.ObjectModel";
            yield return "System.Threading";
            yield return RuntimeDll;

            // add additional references given in configuration file FodyWeavers.xml
            var additionalRefs = this.Config?.Attribute("References")?.Value ?? string.Empty;
            foreach (var ar in additionalRefs.Split('|'))
            {
                if (!string.IsNullOrWhiteSpace(ar))
                    yield return ar;
            }
        }        

        public override bool ShouldCleanReference => false;

        private void WeaveTypeWithAdviceAttributes(MethodWeaver weaver, TypeDefinition type)
        {
            foreach (var method in type.Methods.ToArray())
            {
                var advices = FindActiveAdvices(method);
                if (advices.Any())
                {
                    Debug.WriteLine($"Weaving {type.FullName}.{method.Name}");
                    WriteInfo($"Weaving {type.FullName}.{method.Name}");
                    weaver.WeaveWithAdviceAttributes(type, method);
                }
            }
        }

        private static IEnumerable<TypeDefinition> GetAllTypes(TypeDefinition type)
        {
            yield return type;

            var allNestedTypes = from t in type.NestedTypes
                                 from t2 in GetAllTypes(t)
                                 select t2;

            foreach (var t in allNestedTypes)
                yield return t;
        }

        private IEnumerable<TypeReference> FindActiveAdvices(MethodDefinition method)
        {
            if (method.HasBody && !(method.Name == ".cctor" || method.Name.StartsWith("<") || method.IsEventAdderRemover()))
            {
                // find class attriutes first
                var classAttributes = method.DeclaringType.CustomAttributes.Where(x => x.Implements(_iAdviceType) ||
                                                                                       //x.AttributeType.FullName == _aspectAttributeType.FullName ||
                                                                                       _additionalAdviceTypes.Contains(x.AttributeType.FullName));
                var attributes = classAttributes.Where(x => IsActiveAdvice(x));

                foreach (var attribute in attributes)
                {
                    yield return attribute.AttributeType;
                }

                // find method attributes
                var methodAttributes = method.CustomAttributes.Where(x => x.Implements(_iAdviceType) ||
                                                                          //x.AttributeType.FullName == _aspectAttributeType.FullName ||
                                                                          _additionalAdviceTypes.Contains(x.AttributeType.FullName));
                attributes = methodAttributes.Where(x => IsActiveAdvice(x));
                foreach (var attribute in attributes)
                {
                    yield return attribute.AttributeType;
                }
            }

            bool IsActiveAdvice(CustomAttribute x)
            {
                if (method.IsConstructor && x.GetAttributeProperty<bool>("ExcludeConstructors"))
                    return false;
                if (method.IsAutoProperty() && x.GetAttributeProperty<bool>("ExcludeAutoProperties"))
                    return false;
                if (method.IsStatic && x.GetAttributeProperty<bool>("ExcludeStatic"))
                    return false;
                if (method.IsGetter && x.GetAttributeProperty<bool>("ExcludeGetters"))
                    return false;
                if (method.IsSetter && x.GetAttributeProperty<bool>("ExcludeSetters"))
                    return false;
                return true;
            }
        }

        //private bool IsAdvised(TypeDefinition type)
        //{
        //    //var classAttributes = type.CustomAttributes.Where(x => x.Implements(typeof (IAdvice))).ToArray();
        //    var classAttributes = type.CustomAttributes.Where(x => x.AttributeType == _aspectAttributeType || x.Implements(_iAdviceType));

        //    // Loop through all methods in this type
        //    foreach (var method in type.Methods.Where(x => x.HasBody && !(x.Name == ".cctor" || x.Name.StartsWith("<"))))
        //    {
        //        //var methodAttributes = method.CustomAttributes.Where(x => x.Implements(typeof (IAdvice)));
        //        var methodAttributes = method.CustomAttributes.Where(x => x.AttributeType == _aspectAttributeType || x.Implements(_iAdviceType));
        //        var attributes = classAttributes.Concat(methodAttributes)
        //                                        .Where(x =>
        //                                        {
        //                                            if (x.Implements(_iAdviceType))
        //                                            {
        //                                                if (method.IsConstructor && x.GetAttributeProperty<bool>("ExcludeConstructors"))
        //                                                    return false;
        //                                                if (method.IsSpecialName && (method.IsGetter || method.IsSetter) && x.GetAttributeProperty<bool>("ExcludeAutoProperties"))
        //                                                    return false;
        //                                                if (method.IsStatic && x.GetAttributeProperty<bool>("ExcludeStatic"))
        //                                                    return false;
        //                                                if (method.IsGetter && x.GetAttributeProperty<bool>("ExcludeGetters"))
        //                                                    return false;
        //                                                if (method.IsSetter && x.GetAttributeProperty<bool>("ExcludeSetters"))
        //                                                    return false;
        //                                            }
        //                                            return true;
        //                                        });
        //        if (attributes.Any())
        //            return true;
        //    }

        //    return false;
        //}

        /*
        private IEnumerable<CustomAttribute> FindByMarkerType(
            IList<TypeDefinition> markerTypeDefinitions,
            Collection<CustomAttribute> customAttributes,
            ReferenceFinder referenceFinder)
        {
            foreach (var attr in customAttributes)
            {
                var attributeTypeDef = attr.AttributeType.Resolve();
                if (markerTypeDefinitions.Any(m => attributeTypeDef.Implements(m) ||
                                                   attributeTypeDef.DerivesFrom(m) ||
                                                   this.AreEquals(attributeTypeDef, m)) &&
                   attr.HasUsefulInterceptionMethods(referenceFinder))
                {

                    yield return attr;
                }
            }
        }

        private IEnumerable<AttributeMethodInfo> FindAttributedMethods(IEnumerable<TypeDefinition> markerTypeDefintions)
        {
            return from topLevelType in this.ModuleDefinition.Types
                from type in GetAllTypes(topLevelType)
                from method in type.Methods
                where method.HasBody
                from attribute in method.CustomAttributes.Concat(method.DeclaringType.CustomAttributes)
                let attributeTypeDef = attribute.AttributeType.Resolve()
                from markerTypeDefinition in markerTypeDefintions
                where attributeTypeDef.Implements(markerTypeDefinition) ||
                      attributeTypeDef.DerivesFrom(markerTypeDefinition) ||
                      this.AreEquals(attributeTypeDef, markerTypeDefinition)
                select new AttributeMethodInfo
                {
                    CustomAttribute = attribute,
                    TypeDefinition = type,
                    MethodDefinition = method
                };
        }

        /// <summary>
        /// Find all the types that has correct OnEntry/OnExit/OnException methods
        /// </summary>
        /// <returns></returns>
        private List<TypeDefinition> FindMarkerTypes()
        {
            return this.GetAttributes().Where(HasCorrectMethods).ToList();
        }

        private HostAttributeMapping ToHostAttributeMapping(CustomAttribute arg)
        {
            var prms = arg.ConstructorArguments.First().Value as CustomAttributeArgument[];
            if (null == prms)
                return null;
            return new HostAttributeMapping
            {
                HostAttribute = arg,
                AttribyteTypes = prms.Select(c => ((TypeReference) c.Value).Resolve()).ToArray()
            };
        }

        private List<TypeDefinition> GetAttributes()
        {
            var res = new List<TypeDefinition>();

            res.AddRange(this.ModuleDefinition.CustomAttributes.Select(c => c.AttributeType.Resolve()));
            res.AddRange(this.ModuleDefinition.Assembly.CustomAttributes.Select(c => c.AttributeType.Resolve()));

            if (this.ModuleDefinition.Runtime >= TargetRuntime.Net_4_0)
            {
                //will find if assembly is loaded
                var decorator = Type.GetType("CompileTimeWeaver.IDecorator, CompileTimeWeaver");
                //make using of CompileTimeWeaver assembly optional because it can break exists code
                if (decorator != null)
                {
                    res.AddRange(this.ModuleDefinition.Types.Where(c => c.Implements(decorator)));
                }
            }

            return res;
        }

        private bool AreEquals(TypeDefinition attributeTypeDef, TypeDefinition markerTypeDefinition)
        {
            return attributeTypeDef.FullName == markerTypeDefinition.FullName;
        }

        private class HostAttributeMapping
        {
            public TypeDefinition[] AttribyteTypes { get; set; }
            public CustomAttribute HostAttribute { get; set; }
        }

        private class AttributeMethodInfo
        {
            public TypeDefinition TypeDefinition { get; set; }
            public MethodDefinition MethodDefinition { get; set; }
            public CustomAttribute CustomAttribute { get; set; }
        }
        */
    }
}
