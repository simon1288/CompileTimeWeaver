﻿//using System;
//using System.Linq;
//using Mono.Cecil;

//namespace CompileTimeWeaver.Fody
//{
//    internal static class MethodCreater
//    {
//        public static MethodDefinition DefineMethod(
//            this TypeDefinition typeDef,
//            string methodName,
//            MethodAttributes attributes,
//            Type returnType,
//            params Type[] parameterTypes)
//        {
//            var method = new MethodDefinition(methodName, attributes, null);
//            //var module = typeDef.Module;

//            typeDef.Methods.Add(method);

//            // Match the parameter types 
//            method.AddParameters(parameterTypes);

//            // Match the return type 
//            method.SetReturnType(returnType);

//            return method;
//        }

//        public static TypeReference AddGenericParameter(this MethodDefinition method, Type parameterType)
//        {
//            // Check if the parameter type already exists 
//            var match = (from GenericParameter p in method.GenericParameters where p.Name == parameterType.Name select p).FirstOrDefault();

//            // Reuse the existing parameter 
//            if (match != null)
//                return match;

//            var parameter = new GenericParameter(parameterType.Name, method);
//            method.GenericParameters.Add(parameter);

//            return parameter;
//        }

//        public static void AddParameters(this MethodDefinition method, Type[] parameterTypes)
//        {
//            var module = method.DeclaringType.Module;

//            // Build the parameter list 
//            foreach (var type in parameterTypes)
//            {
//                var parameterType = type.IsGenericParameter ? method.AddGenericParameter(type) : module.ImportReference(type);

//                var param = new ParameterDefinition(parameterType);
//                method.Parameters.Add(param);
//            }
//        }

//        public static void SetReturnType(this MethodDefinition method, Type returnType)
//        {
//            ModuleDefinition module = method.DeclaringType.Module;

//            TypeReference actualReturnType;
//            if (returnType.IsGenericParameter)
//            {
//                actualReturnType = method.AddGenericParameter(returnType);
//            }
//            else
//            {
//                actualReturnType = module.ImportReference(returnType);
//            }

//            method.ReturnType = actualReturnType;
//        }
//    }
//}