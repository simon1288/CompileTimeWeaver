using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Fody;
using Mono.Cecil;
using Mono.Cecil.Cil;
using Mono.Cecil.Rocks;
using CustomAttributeNamedArgument = Mono.Cecil.CustomAttributeNamedArgument;
using MethodBody = Mono.Cecil.Cil.MethodBody;

namespace CompileTimeWeaver.Fody
{
    internal static class TypeExt
    {
        public static bool Implements(this CustomAttribute x, TypeReference interfaceTypeRef)
        {
            /*
            // Avoid problem on initial load of types where mscorlib not loaded - the Implements()
            // method crashes if this happens.
            if (x.AttributeType.Module.AssemblyReferences.All(a => a.Name != "mscorlib" && a.Name!= "System.Private.CoreLib"))
                return false;
            */

            var typeDefinition = x.AttributeType.Resolve();
            if (typeDefinition == null)
            {
                //var typeRef = x.AttributeType.Module.ImportReference(x.AttributeType);
                //typeDefinition = typeRef.Resolve();
                //if (typeDefinition == null)
                    throw new WeavingException($"Cannot resolve {x.AttributeType.FullName}");
            }

            return typeDefinition.Implements(interfaceTypeRef);
        }

        public static bool Implements(this TypeDefinition typeDefinition, TypeReference interfaceTypeReference)
        {
            return typeDefinition.Implements(interfaceTypeReference.FullName);
        }

        public static bool Implements(this TypeDefinition typeDefinition, Type interfaceType)
        {
            return typeDefinition.Implements(interfaceType.FullName);
        }

        public static bool Implements(this TypeDefinition typeDefinition, string interfaceTypeFullName)
        {
            while (typeDefinition?.BaseType != null)
            {
                if (typeDefinition.Interfaces != null && typeDefinition.Interfaces.Any(i => i.InterfaceType.FullName == interfaceTypeFullName))
                    return true;

                typeDefinition = typeDefinition.BaseType.Resolve();
            }

            return false;
        }

        public static bool DerivesFrom(this TypeReference typeReference, TypeReference baseTypeReference)
        {
            return typeReference.DerivesFrom(baseTypeReference.FullName);
        }

        public static bool DerivesFrom(this TypeReference typeReference, Type expectedBaseType)
        {
            return typeReference.DerivesFrom(expectedBaseType.FullName);
        }

        public static bool DerivesFrom(this TypeReference typeReference, string baseTypeFullName)
        {
            while (typeReference != null)
            {
                if (typeReference.FullName == baseTypeFullName)
                    return true;

                typeReference = typeReference.Resolve().BaseType;
            }

            return false;
        }

        public static Type GetMonoType(this TypeReference type)
        {
            var name = GetReflectionName(type);
            return Type.GetType(name, false);
        }

        private static string GetReflectionName(TypeReference type)
        {
            string fullName;
            if (type.IsGenericInstance)
            {
                var genericInstance = (GenericInstanceType)type;
                fullName = $"{genericInstance.Namespace}.{type.Name}[{string.Join(",", genericInstance.GenericArguments.Select(p => GetReflectionName(p)).ToArray())}]";
            }
            else
            {
                fullName = type.FullName;
            }

            return Assembly.CreateQualifiedName(type.Module.Assembly.FullName, fullName);
        }

        public static T GetAttributeProperty<T>(this CustomAttribute attr, string propertyName)
        {
            if (attr.Properties.All(x => x.Name != propertyName))
                return default(T);

            return (T)attr.Properties.First(x => x.Name == propertyName).Argument.Value;
        }

        public static void SetAttributeProperty(this CustomAttribute attr, string propertyName, TypeReference valueType, object value)
        {
            for (int i = 0; i < attr.Properties.Count; i++)
            {
                if (attr.Properties[i].Name == propertyName)
                {
                    attr.Properties.RemoveAt(i);
                    break;
                }
            }

            attr.Properties.Add(new CustomAttributeNamedArgument(propertyName, new CustomAttributeArgument(valueType, value)));
        }

        public static MethodReference MakeHostInstanceGeneric(this MethodReference self, params TypeReference[] hostGenerics)
        {
            if (self.DeclaringType.GenericParameters.Count != hostGenerics.Length)
                throw new ArgumentException("Invalid number of generic type arguments supplied", nameof(hostGenerics));
            if (hostGenerics.Length == 0)
                return self;

            var reference = new MethodReference(self.Name, self.ReturnType, self.DeclaringType.MakeGenericInstanceType(hostGenerics))
            {
                HasThis = self.HasThis,
                ExplicitThis = self.ExplicitThis,
                CallingConvention = self.CallingConvention
            };

            foreach (var parameter in self.Parameters)
            {
                reference.Parameters.Add(new ParameterDefinition(parameter.ParameterType));
            }

            foreach (var genericParam in self.GenericParameters)
            {
                reference.GenericParameters.Add(new GenericParameter(genericParam.Name, reference));
            }

            return reference;
        }

        public static MethodReference MakeGenericInstanceMethod(this MethodReference method, params TypeReference[] genericTypeArgs)
        {
            if (genericTypeArgs.Length == 0)
                return method;

            if (method.GenericParameters.Count != genericTypeArgs.Length)
                throw new ArgumentException("Invalid number of generic type arguments supplied", nameof(genericTypeArgs));


            var genericTypeRef = new GenericInstanceMethod(method);
            foreach (var arg in genericTypeArgs)
            {
                genericTypeRef.GenericArguments.Add(arg);
            }

            return genericTypeRef;
        }

        public static TypeReference GetGenericArgumentType(this MethodReference targetMethodRef, string paramGenericTypeName)
        {
            var genericParam = ((GenericInstanceMethod)targetMethodRef).GenericArguments.FirstOrDefault(p => p.Name == paramGenericTypeName);
            if (genericParam == null)
            {
                var genericInstanceType = targetMethodRef.DeclaringType as GenericInstanceType;
                if (genericInstanceType != null)
                    genericParam = genericInstanceType.GenericArguments.FirstOrDefault(p => p.Name == paramGenericTypeName);
            }
            return genericParam;
        }

        public static TypeReference GetGenericArgumentType(this GenericInstanceType genericInstanceType, string paramGenericTypeName)
        {
            return genericInstanceType.GenericArguments.FirstOrDefault(p => p.Name == paramGenericTypeName);
        }

        public static void CopyParameters(this MethodDefinition source, MethodDefinition destination)
        {
            foreach (var parameter in source.Parameters)
            {
                var newParameter = new ParameterDefinition(parameter.Name, parameter.Attributes, parameter.ParameterType);
                destination.Parameters.Add(newParameter);
            }
        }

        public static void CopyGenericParameters(this IGenericParameterProvider source, IGenericParameterProvider destination)
        {
            if (source.HasGenericParameters)
            {
                foreach (var genericParameter in source.GenericParameters)
                {
                    var newGenericParameter = new GenericParameter(genericParameter.Name, destination);
                    foreach (var constraint in genericParameter.Constraints)
                    {
                        newGenericParameter.Constraints.Add(constraint);
                    }
                    destination.GenericParameters.Add(newGenericParameter);
                }
            }
        }

        public static void CopyCustomAttributes(this MethodDefinition source, MethodDefinition destination)
        {
            foreach (var attr in source.CustomAttributes)
            {
                //var newAttr = new CustomAttribute(attr.Name, attr.Attributes, attr.ParameterType);
                destination.CustomAttributes.Add(attr);
            }
        }

        public static MethodDefinition CreatePrivateCopy(this MethodDefinition method)
        {
            if (method.IsAbstract)
                return null;

            var privateCopy = new MethodDefinition("__" + method.Name, method.Attributes, method.ReturnType);
            //method.CopyCustomAttributes(privateCopy);
            method.CopyParameters(privateCopy);
            method.CopyGenericParameters(privateCopy);
            privateCopy.Body = new MethodBody(privateCopy);
            foreach (var variable in method.Body.Variables)
            {
                privateCopy.Body.InitLocals = true;
                //TODO: privateCopy.Body.Variables.Add(new VariableDefinition(variable.Name, variable.VariableType));
                privateCopy.Body.Variables.Add(new VariableDefinition(variable.VariableType));
            }
            privateCopy.Body.GetILProcessor().Append(method.Body.Instructions);
            foreach (var handler in method.Body.ExceptionHandlers)
            {
                privateCopy.Body.ExceptionHandlers.Add(handler);
            }

            method.DeclaringType.Methods.Add(privateCopy);

            return privateCopy;
        }

        //public static TypeDefinition ResolvePreserve(this TypeReference self)
        //{
        //    if (self.IsGenericInstance)
        //    {
        //        var previous_instance = (GenericInstanceType)self;
        //        var instance = new GenericInstanceType(previous_instance.ElementType.ResolvePreserve());
        //        foreach (var argument in previous_instance.GenericArguments)
        //            instance.GenericArguments.Add(argument.ResolvePreserve());
        //    }

        //    if (self.IsArray)
        //    {
        //        // ..
        //    }

        //    if (self.IsByReference)
        //    {
        //        //
        //    }

        //    return self.Resolve();
        //}
    }
}