﻿using CompileTimeWeaver.Fody.Algorithms;
using Fody;
using Mono.Cecil;
using Mono.Cecil.Cil;
using Mono.Cecil.Rocks;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace CompileTimeWeaver.Fody.PropertyChanged
{
    internal class NotifyPropertyChangedInjector
    {
        private readonly ModuleWeaver _moduleWeaver;
        private readonly ReferenceFinder _referenceFinder;

        private readonly TypeReference PropChangedInterfaceReference;
        private readonly TypeReference PropChangedHandlerReference;
        private readonly MethodReference PropertyChangedEventHandlerInvokeReference;
        private readonly TypeReference PropertyChangedEventArgsReference;
        private readonly MethodReference DelegateCombineMethodRef;
        private readonly MethodReference DelegateRemoveMethodRef;

        private readonly MethodReference PropertyChangedEventArgsConstructorReference;
        private readonly TypeReference EqualityComparerTypeReference;
        private readonly GenericInstanceMethod InterlockedCompareExchangeForPropChangedHandler;

        public NotifyPropertyChangedInjector(ModuleWeaver moduleWeaver)
        {
            _moduleWeaver = moduleWeaver;
            _referenceFinder = new ReferenceFinder(moduleWeaver);

            PropChangedInterfaceReference = _moduleWeaver.ModuleDefinition.ImportReference(_moduleWeaver.FindTypeDefinition("System.ComponentModel.INotifyPropertyChanged"));
            var propChangedHandlerDefinition = _moduleWeaver.FindTypeDefinition("System.ComponentModel.PropertyChangedEventHandler");
            PropChangedHandlerReference = _moduleWeaver.ModuleDefinition.ImportReference(propChangedHandlerDefinition);
            PropertyChangedEventHandlerInvokeReference = _moduleWeaver.ModuleDefinition.ImportReference(propChangedHandlerDefinition.Methods.First(x => x.Name == "Invoke"));
            var propChangedArgsDefinition = _moduleWeaver.FindTypeDefinition("System.ComponentModel.PropertyChangedEventArgs");
            PropertyChangedEventArgsReference = _moduleWeaver.ModuleDefinition.ImportReference(propChangedArgsDefinition);
            PropertyChangedEventArgsConstructorReference = _moduleWeaver.ModuleDefinition.ImportReference(propChangedArgsDefinition.Methods.First(x => x.IsConstructor));
            
            EqualityComparerTypeReference = _moduleWeaver.ModuleDefinition.ImportReference(_moduleWeaver.FindTypeDefinition("System.Collections.Generic.EqualityComparer`1"));

            var interlockedDefinition = _moduleWeaver.FindTypeDefinition("System.Threading.Interlocked");
            var genericCompareExchangeMethodDefinition = interlockedDefinition
                .Methods.First(x =>
                    x.IsStatic &&
                    x.Name == "CompareExchange" &&
                    x.GenericParameters.Count == 1 &&
                    x.Parameters.Count == 3);
            var genericCompareExchangeMethod = _moduleWeaver.ModuleDefinition.ImportReference(genericCompareExchangeMethodDefinition);

            var delegateDefinition = _moduleWeaver.FindTypeDefinition("System.Delegate");
            var combineMethodDefinition = delegateDefinition.Methods
                .Single(x =>
                    x.Name == "Combine" &&
                    x.Parameters.Count == 2 &&
                    x.Parameters.All(p => p.ParameterType == delegateDefinition));
            DelegateCombineMethodRef = _moduleWeaver.ModuleDefinition.ImportReference(combineMethodDefinition);
            var removeMethodDefinition = delegateDefinition.Methods.First(x => x.Name == "Remove");
            DelegateRemoveMethodRef = _moduleWeaver.ModuleDefinition.ImportReference(removeMethodDefinition);

            InterlockedCompareExchangeForPropChangedHandler = new GenericInstanceMethod(genericCompareExchangeMethod);
            InterlockedCompareExchangeForPropChangedHandler.GenericArguments.Add(PropChangedHandlerReference);
        }

        #region INotifyPropertyChanged

        public void InjectINotifyPropertyChangedInterfaceIfNecessary(TypeDefinition targetType)
        {
            if (!targetType.Implements(PropChangedInterfaceReference))
            {
                targetType.Interfaces.Add(new InterfaceImplementation(PropChangedInterfaceReference));
                WeavePropertyChangedEvent(targetType);
            }
        }

        private void WeavePropertyChangedEvent(TypeDefinition type)
        {
            var propertyChangedFieldDef = new FieldDefinition("PropertyChanged", FieldAttributes.Private | FieldAttributes.NotSerialized, PropChangedHandlerReference);
            type.Fields.Add(propertyChangedFieldDef);
            var propertyChangedField = propertyChangedFieldDef.GetGeneric();

            var eventDefinition = new EventDefinition("PropertyChanged", EventAttributes.None, PropChangedHandlerReference)
            {
                AddMethod = CreateEventMethod("add_PropertyChanged", DelegateCombineMethodRef, propertyChangedField),
                RemoveMethod = CreateEventMethod("remove_PropertyChanged", DelegateRemoveMethodRef, propertyChangedField)
            };

            type.Methods.Add(eventDefinition.AddMethod);
            type.Methods.Add(eventDefinition.RemoveMethod);
            type.Events.Add(eventDefinition);
        }

        private MethodDefinition CreateEventMethod(string methodName, MethodReference delegateMethodReference, FieldReference propertyChangedField)
        {
            const MethodAttributes Attributes = MethodAttributes.Public |
                                                MethodAttributes.HideBySig |
                                                MethodAttributes.Final |
                                                MethodAttributes.SpecialName |
                                                MethodAttributes.NewSlot |
                                                MethodAttributes.Virtual;

            var method = new MethodDefinition(methodName, Attributes, _moduleWeaver.TypeSystem.VoidReference);

            method.Parameters.Add(new ParameterDefinition("value", ParameterAttributes.None, PropChangedHandlerReference));
            var handlerVariable0 = new VariableDefinition(PropChangedHandlerReference);
            method.Body.Variables.Add(handlerVariable0);
            var handlerVariable1 = new VariableDefinition(PropChangedHandlerReference);
            method.Body.Variables.Add(handlerVariable1);
            var handlerVariable2 = new VariableDefinition(PropChangedHandlerReference);
            method.Body.Variables.Add(handlerVariable2);

            var loopBegin = Instruction.Create(OpCodes.Ldloc, handlerVariable0);
            method.Body.Instructions.Append(
                Instruction.Create(OpCodes.Ldarg_0),
                Instruction.Create(OpCodes.Ldfld, propertyChangedField),
                Instruction.Create(OpCodes.Stloc, handlerVariable0),
                loopBegin,
                Instruction.Create(OpCodes.Stloc, handlerVariable1),
                Instruction.Create(OpCodes.Ldloc, handlerVariable1),
                Instruction.Create(OpCodes.Ldarg_1),
                Instruction.Create(OpCodes.Call, delegateMethodReference),
                Instruction.Create(OpCodes.Castclass, PropChangedHandlerReference),
                Instruction.Create(OpCodes.Stloc, handlerVariable2),
                Instruction.Create(OpCodes.Ldarg_0),
                Instruction.Create(OpCodes.Ldflda, propertyChangedField),
                Instruction.Create(OpCodes.Ldloc, handlerVariable2),
                Instruction.Create(OpCodes.Ldloc, handlerVariable1),
                Instruction.Create(OpCodes.Call, InterlockedCompareExchangeForPropChangedHandler),
                Instruction.Create(OpCodes.Stloc, handlerVariable0),
                Instruction.Create(OpCodes.Ldloc, handlerVariable0),
                Instruction.Create(OpCodes.Ldloc, handlerVariable1),
                Instruction.Create(OpCodes.Bne_Un_S, loopBegin), // go to begin of loop
                Instruction.Create(OpCodes.Ret));
            method.Body.InitLocals = true;
            method.Body.OptimizeMacros();

            return method;
        }

        #endregion

        #region OnPropertyChanged method

        const string InjectedPropertyChangedEventInvokerName = "<>OnPropertyChanged";

        public MethodReference FindOnPropertyChangedMethod(TypeDefinition type)
        {
            var onPropertyChangedMethod = _referenceFinder.GetMethodReference(type, m => m.FullName == InjectedPropertyChangedEventInvokerName, false);

            if (onPropertyChangedMethod?.Resolve()?.IsVisibleFromChildren() ?? false)
                return null;

            return onPropertyChangedMethod;
        }

        public MethodDefinition AddOnPropertyChangedMethod(TypeDefinition targetType)
        {
            var propertyChangedFieldDef = GetPropertyChangedEventHandlerField(targetType);
            if (propertyChangedFieldDef == null)
            {
                var message = $"Could not inject EventInvoker method on type '{targetType.FullName}'. It is possible you are inheriting from a base class and have not correctly set 'EventInvokerNames' or you are using a explicit PropertyChanged event and the event field is not visible to this instance. Either correct 'EventInvokerNames' or implement your own EventInvoker on this class. If you want to suppress this place a [DoNotNotifyAttribute] on {targetType.FullName}.";
                throw new WeavingException(message);
            }

            var propertyChangedField = propertyChangedFieldDef.GetGeneric();
            var method = new MethodDefinition(InjectedPropertyChangedEventInvokerName, GetMethodAttributes(targetType), _moduleWeaver.TypeSystem.VoidReference);
            method.Parameters.Add(new ParameterDefinition("eventArgs", ParameterAttributes.None, PropertyChangedEventArgsReference));

            var handlerVariable = new VariableDefinition(PropChangedHandlerReference);
            method.Body.Variables.Add(handlerVariable);

            var instructions = method.Body.Instructions;

            var last = Instruction.Create(OpCodes.Ret);
            instructions.Add(Instruction.Create(OpCodes.Ldarg_0));
            instructions.Add(Instruction.Create(OpCodes.Ldfld, propertyChangedField));
            instructions.Add(Instruction.Create(OpCodes.Stloc_0));
            instructions.Add(Instruction.Create(OpCodes.Ldloc_0));
            instructions.Add(Instruction.Create(OpCodes.Brfalse_S, last));
            instructions.Add(Instruction.Create(OpCodes.Ldloc_0));
            instructions.Add(Instruction.Create(OpCodes.Ldarg_0));
            instructions.Add(Instruction.Create(OpCodes.Ldarg_1));
            instructions.Add(Instruction.Create(OpCodes.Tail));
            instructions.Add(Instruction.Create(OpCodes.Callvirt, PropertyChangedEventHandlerInvokeReference));

            instructions.Add(last);
            method.Body.InitLocals = true;
            targetType.Methods.Add(method);
            return method;
        }

        private FieldDefinition GetPropertyChangedEventHandlerField(TypeDefinition targetType)
        {
            var addMethods = targetType.GetPropertyChangedAddMethods().ToList();

            if (!addMethods.Any())
            {
                return null;
            }

            if (addMethods.Count > 1)
            {
                throw new WeavingException("Found more than one PropertyChanged event");
            }

            var method = addMethods[0];
            if (method.IsAbstract)
            {
                throw new WeavingException($"{targetType.FullName}.PropertyChanged event is abstract");
            }

            var fieldReferences = method.Body.Instructions
                .Where(i => i.OpCode == OpCodes.Ldfld || i.OpCode == OpCodes.Ldflda || i.OpCode == OpCodes.Stfld)
                .Select(i => i.Operand)
                .OfType<FieldReference>()
                .Where(fld => IsPropertyChangedEventHandler(fld.FieldType))
                .ToList();

            if (fieldReferences.Select(i => i.FullName).Distinct().Count() != 1)
            {
                return null;
            }

            return fieldReferences[0].Resolve();
        }

        private static bool IsPropertyChangedEventHandler(TypeReference type)
        {
            return type.FullName == "System.ComponentModel.PropertyChangedEventHandler"
                   || type.FullName == "Windows.UI.Xaml.Data.PropertyChangedEventHandler"
                   || type.FullName == "System.Runtime.InteropServices.WindowsRuntime.EventRegistrationTokenTable`1<Windows.UI.Xaml.Data.PropertyChangedEventHandler>";
        }

        private static MethodAttributes GetMethodAttributes(TypeDefinition targetType)
        {
            if (targetType.IsSealed)
            {
                return MethodAttributes.Private | MethodAttributes.HideBySig;
            }

            return MethodAttributes.Family | MethodAttributes.HideBySig;
        }

        #endregion

        #region SetField method

        //const string injectedSetFieldMethodName = "<>SetField";
        const string NotifyPropertyChangedAttributeName = "CompileTimeWeaver.NotifyPropertyChangedAttribute";
        const string IgnoreNotifyPropertyChangedAttributeName = "CompileTimeWeaver.IgnoreNotifyPropertyChangedAttribute";

        /*
        /// <summary>
        /// Create a method like this:
        /// protected bool SetField<T>(ref T field, T value, string propertyName)
        /// {
        ///     if (System.Collections.Generic.EqualityComparer<T>.Default.Equals(field, value))
        ///         return false;
        ///     field = value;
        ///     OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        ///     return true;
        /// }
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns>SetField method</returns>
        public MethodDefinition AddSetFieldMethod(TypeDefinition targetType, MethodDefinition onPropertyChangedMethod)
        {
            var method = new MethodDefinition(injectedSetFieldMethodName, GetMethodAttributes(targetType), _moduleWeaver.TypeSystem.BooleanReference);
            var genericT = new Mono.Cecil.GenericParameter("T", method);
            method.GenericParameters.Add(genericT);

            //Parameters of 'protected bool SetField<T>( T field, T value,  string propertyName)'
            var pField = new ParameterDefinition(new ByReferenceType(genericT)) { Name = "field" };
            method.Parameters.Add(pField);
            method.Parameters.Add(new ParameterDefinition("value", ParameterAttributes.None, genericT));
            method.Parameters.Add(new ParameterDefinition("propertyName", ParameterAttributes.None, _moduleWeaver.ModuleDefinition.TypeSystem.String));

            targetType.Methods.Add(method);

            method.Body.InitLocals = true;
            var il = method.Body.GetILProcessor();


            //if (System.Collections.Generic.EqualityComparer<T>.Default.Equals(field, value))
            var eq = EqualityComparerTypeReference.MakeGenericInstanceType(genericT);
            var getDefault = _moduleWeaver.ModuleDefinition.ImportReference(eq.Resolve().Properties.First(m => m.Name == "Default").GetMethod.MakeHostInstanceGeneric(genericT));
            //var specificEqualityComparerType = _moduleWeaver.ModuleDefinition.ImportReference(genericEq, method);
            //var defaultProperty = _moduleWeaver.ModuleDefinition.ImportReference(eq.Properties.Single(p => p.Name == "Default").GetMethod);
            //defaultProperty.DeclaringType = specificEqualityComparerType;
            il.Emit(OpCodes.Call, getDefault);
            il.Emit(OpCodes.Ldarg_1);   //field
            il.Emit(OpCodes.Ldobj);     //!!0
            il.Emit(OpCodes.Ldarg_2);   //value
            var equals = _moduleWeaver.ModuleDefinition.ImportReference(eq.Resolve().Methods.First(m => m.Name == "Equals").MakeHostInstanceGeneric(genericT));
            //var equalsMethod = _moduleWeaver.ModuleDefinition.ImportReference(eq.Methods.Single(p => p.Name == "Equals" && p.Parameters.Count == 2));
            //equalsMethod.DeclaringType = specificEqualityComparerType;
            il.Emit(OpCodes.Callvirt, equals);
            var lbl_ElseEntryPoint = il.Create(OpCodes.Nop);
            il.Emit(OpCodes.Brfalse, lbl_ElseEntryPoint);
            //if body (field and value are equal)
            il.Emit(OpCodes.Ldc_I4, 0);
            il.Emit(OpCodes.Ret); //return false;

            //else body
            il.Append(lbl_ElseEntryPoint); //value
            il.Emit(OpCodes.Ldarg_1); //field
            il.Emit(OpCodes.Ldarg_2); //value
            il.Emit(OpCodes.Stobj); //field = value;

            //<>OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
            il.Emit(OpCodes.Ldarg_0); //this
            il.Emit(OpCodes.Ldarg_3); //propertyName
            il.Emit(OpCodes.Newobj, PropertyChangedEventArgsConstructorReference); //new PropertyChangedEventArgs(propertyName)
            //call <>OnPropertyChanged
            il.Emit(OpCodes.Call, onPropertyChangedMethod);

            //return true;
            il.Emit(OpCodes.Ldc_I4, 1);
            il.Emit(OpCodes.Ret);

            return method;
        }
        */

        /// <summary>
        /// Rewrite all setters and remove NotifyPropertyChangedAttribute and IgnoreNotifyPropertyChangedAttribute
        /// </summary>
        /// <param name="targetType"></param>
        /// <param name="onPropertyChangedMethod"></param>
        public void RewriteAllSetters(TypeDefinition targetType, MethodReference onPropertyChangedMethod)
        {
            // create property dependency relation graph
            var propertyDependencyRelations = new Graph<PropertyDefinition, bool>(targetType.Properties.Count);
            propertyDependencyRelations.AddVertexes(targetType.Properties);
            foreach (var property in targetType.Properties)
            {
                var reader = new DeterminedByPropertiesAttributeReader(property);
                foreach (var ancestorProperty in reader.GetAncestors())
                {
                    propertyDependencyRelations.AddEdge(ancestorProperty, property, true);
                }
            }

            foreach (var property in targetType.Properties)
            {
                this.RewriteSetter(property, onPropertyChangedMethod, propertyDependencyRelations);

                //remove NotifyPropertyChangedAttribute
                foreach (var attr in property.CustomAttributes.GetAttributes(NotifyPropertyChangedAttributeName).ToArray())
                {
                    property.CustomAttributes.Remove(attr);
                }

                //remove IgnoreNotifyPropertyChangedAttribute
                foreach (var ignoreAttr in property.CustomAttributes.GetAttributes(IgnoreNotifyPropertyChangedAttributeName).ToArray())
                {
                    property.CustomAttributes.Remove(ignoreAttr);
                }

                //remove DeterminedByPropertiesAttribute
                foreach (var attr in property.CustomAttributes.GetAttributes(DeterminedByPropertiesAttributeReader.DeterminedByPropertiesAttributeName).ToArray())
                {
                    property.CustomAttributes.Remove(attr);
                }
            }

            //remove NotifyPropertyChangedAttribute
            foreach (var attr in targetType.CustomAttributes.GetAttributes(NotifyPropertyChangedAttributeName).ToArray())
            {
                targetType.CustomAttributes.Remove(attr);
            }
        }

        /// <summary>
        /// Create a setter like this:
        /// public void set_Age(int value)
        /// {
        ///     if (System.Collections.Generic.EqualityComparer<int>.Default.Equals(<Age>k__BackingField, value))
        ///         return;
        ///     <Age>k__BackingField = value;
        ///     OnPropertyChanged(new PropertyChangedEventArgs("Age"));
        ///     return;
        /// }
        /// </summary>
        /// <returns></returns>
        public bool RewriteSetter(PropertyDefinition property, MethodReference onPropertyChangedMethodDef, Graph<PropertyDefinition, bool> propertyDependencyRelations)
        {
            var method = property.SetMethod;
            if (method == null)
                return false;
            if (property.CustomAttributes.Any(a => a.AttributeType.FullName == IgnoreNotifyPropertyChangedAttributeName))
                return false;
            var backingField = GetPropertyBackingField(property.DeclaringType, property);
            if (backingField == null)
                return false;

            var eq = EqualityComparerTypeReference.Resolve();
            var getDefault = _moduleWeaver.ModuleDefinition.ImportReference(eq.Properties.First(m => m.Name == "Default").GetMethod.MakeHostInstanceGeneric(property.PropertyType));
            var equals = _moduleWeaver.ModuleDefinition.ImportReference(eq.Methods.First(m => m.Name == "Equals").MakeHostInstanceGeneric(property.PropertyType));

            Debug.WriteLine($"Rewriting {property.DeclaringType.FullName}.{method.Name}...");
            _moduleWeaver.WriteDebug($"Rewriting {property.DeclaringType.FullName}.{method.Name}...");

            //method.Body.SimplifyMacros();
            method.Body.Instructions.Clear();

            var il = method.Body.GetILProcessor();
            //if (System.Collections.Generic.EqualityComparer<T>.Default.Equals(field, value))
            il.Emit(OpCodes.Call, getDefault);      //EqualityComparer<T>.Default
            il.Emit(OpCodes.Ldarg_0);               //this
            il.Emit(OpCodes.Ldfld, backingField);   //backing field
            il.Emit(OpCodes.Ldarg_1);               //value
            il.Emit(OpCodes.Callvirt, equals);      //Equals()
            var lbl_ElseEntryPoint = il.Create(OpCodes.Nop);
            il.Emit(OpCodes.Brfalse_S, lbl_ElseEntryPoint);

            //if body (field and value are equal)
            il.Emit(OpCodes.Ret); //return false;

            //else body
            il.Append(lbl_ElseEntryPoint);
            il.Emit(OpCodes.Ldarg_0); //this
            il.Emit(OpCodes.Ldarg_1); //value
            il.Emit(OpCodes.Stfld, backingField); //backing field <= value;

            //<>OnPropertyChanged(new PropertyChangedEventArgs(property Name));
            il.Emit(OpCodes.Ldarg_0); //this
            il.Emit(OpCodes.Ldstr, property.Name); //property Name
            il.Emit(OpCodes.Newobj, PropertyChangedEventArgsConstructorReference); //new PropertyChangedEventArgs(propertyName)
            //call <>OnPropertyChanged
            il.Emit(OpCodes.Call, onPropertyChangedMethodDef);
            foreach (var descendant in propertyDependencyRelations.GetDescendants(property))
            {
                //<>OnPropertyChanged(new PropertyChangedEventArgs(descendant property Name));
                il.Emit(OpCodes.Ldarg_0); //this
                il.Emit(OpCodes.Ldstr, descendant.Name); //property Name
                il.Emit(OpCodes.Newobj, PropertyChangedEventArgsConstructorReference); //new PropertyChangedEventArgs(descendant propertyName)
                                                                                       //call <>OnPropertyChanged
                il.Emit(OpCodes.Call, onPropertyChangedMethodDef);
            }

            il.Emit(OpCodes.Ret);

            method.Body.InitLocals = true;
            method.Body.OptimizeMacros();
            return true;
        }

        #endregion

        public static IEnumerable<TypeDefinition> FindTypesWithNotifyPropertyChangedAttribute(IEnumerable<TypeDefinition> types)
        {
            var typesToWeave = types.Where(x => x.CustomAttributes.Any(a => a.AttributeType.FullName == NotifyPropertyChangedAttributeName) ||
                                    x.Properties.Any(p => GetPropertyBackingField(x, p) != null &&
                                                          p.CustomAttributes.Any(a => a.AttributeType.FullName == NotifyPropertyChangedAttributeName)));
            //typesToWeave.Where(x => !x.Properties.All(p => p.CustomAttributes.Any(a => a.AttributeType.FullName == IgnoreNotifyPropertyChangedAttributeName) ||
            //                                               GetPropertyBackingField(x, p) == null));
            return typesToWeave;
        }

        private static FieldDefinition GetPropertyBackingField(TypeDefinition typeDefinition, PropertyDefinition property)
        {
            var propertyName = property.Name;
            var fieldsWithSameType = typeDefinition.Fields.Where(x => x.DeclaringType == typeDefinition && x.FieldType.FullName == property.PropertyType.FullName).ToList();
            foreach (var field in fieldsWithSameType)
            {
                //AutoProp
                if (field.Name == $"<{propertyName}>k__BackingField")
                {
                    return field;
                }
            }

            /* Uncomment these line if I want to rewrite non-auto properties, too.
            foreach (var field in fieldsWithSameType)
            {
                //diffCase
                var upperPropertyName = propertyName.ToUpper();
                var fieldUpper = field.Name.ToUpper();
                if (fieldUpper == upperPropertyName)
                {
                    return field;
                }

                //underScore
                if (fieldUpper == "_" + upperPropertyName)
                {
                    return field;
                }
            }
            */

            return null;
        }
    }
}
