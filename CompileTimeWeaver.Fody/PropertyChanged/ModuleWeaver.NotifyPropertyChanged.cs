﻿using CompileTimeWeaver.Fody.PropertyChanged;
using Mono.Cecil;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace CompileTimeWeaver.Fody
{
    partial class ModuleWeaver
    {
        private void WeaveTypesWithNotifyProperyChangedAttributes(IEnumerable<TypeDefinition> types)
        {
            var typesToWeave = NotifyPropertyChangedInjector.FindTypesWithNotifyPropertyChangedAttribute(types).ToList();
            if (typesToWeave.Count == 0)
                return;

            var referenceFinder = new ReferenceFinder(this);
            var injector = new NotifyPropertyChangedInjector(this);
            var typesWeaved = new List<TypeDefinition>(typesToWeave.Count);
            while (typesToWeave.Count>0)
            {
                var topMostBaseType = FindTopMostBaseType(typesToWeave[0]);

                // let top most base type implement INotifyPropertyChanged
                injector.InjectINotifyPropertyChangedInterfaceIfNecessary(topMostBaseType);

                var onPropertyChangedMethod = injector.FindOnPropertyChangedMethod(topMostBaseType);
                if (onPropertyChangedMethod == null)
                {
                    // add <>OnPropertyChanged() to top most base type
                    Debug.WriteLine($"Adding {topMostBaseType.FullName}.<>OnPropertyChanged()...");
                    WriteInfo($"Adding {topMostBaseType.FullName}.<>OnPropertyChanged()...");
                    onPropertyChangedMethod = injector.AddOnPropertyChangedMethod(topMostBaseType);

                    // add <>SetField() to top most base type
                    //injector.AddSetFieldMethod(topMostBaseType, onPropertyChangedMethod);
                }

                //move topMostBaseType and its children into typesWeaved list
                for (int i = 0; i < typesToWeave.Count;)
                {
                    var type = typesToWeave[i];
                    if (type == topMostBaseType || type.DerivesFrom(topMostBaseType))
                    {
                        //rewrite setters of topMostBaseType and its children to call <>OnPropertyChanged()
                        Debug.WriteLine($"Rewriting {topMostBaseType.FullName} setters...");
                        WriteInfo($"Rewriting {topMostBaseType.FullName} setters...");
                        injector.RewriteAllSetters(type, onPropertyChangedMethod);

                        typesWeaved.Add(type);
                        typesToWeave.RemoveAt(i);
                    }
                    else
                    {
                        i++;
                    }
                }
            }

            TypeDefinition FindTopMostBaseType(TypeDefinition targetType)
            {
                //find top super class of targetType in typesToWeave list
                var topType = targetType;
                do
                {
                    var baseType = typesToWeave.FirstOrDefault(x => x.FullName == topType.BaseType.FullName);
                    if (baseType == null)
                        break;
                    topType = baseType;
                } while (true);

                return topType;
            }
        }
    }
}