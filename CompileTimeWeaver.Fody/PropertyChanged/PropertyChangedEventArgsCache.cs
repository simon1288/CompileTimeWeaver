﻿//using System.Collections.Generic;
//using System.Linq;
//using Mono.Cecil;
//using Mono.Cecil.Cil;

//namespace CompileTimeWeaver.Fody
//{
//    internal class PropertyChangedEventArgsCache
//    {
//        private ModuleWeaver _moduleWeaver;
//        private TypeDefinition _cacheTypeDefinition;
//        private Dictionary<string, FieldDefinition> _properties = new Dictionary<string, FieldDefinition>();

//        public PropertyChangedEventArgsCache(ModuleWeaver moduleWeaver)
//        {
//            _moduleWeaver = moduleWeaver;
//            var attributes = TypeAttributes.AutoClass | TypeAttributes.AutoLayout | TypeAttributes.Abstract | TypeAttributes.Sealed | TypeAttributes.BeforeFieldInit | TypeAttributes.Class | TypeAttributes.NotPublic;
//            _cacheTypeDefinition = new TypeDefinition(moduleWeaver.ModuleDefinition.Assembly.Name.Name, "<>PropertyChangedEventArgs", attributes, moduleWeaver.TypeSystem.ObjectReference);
//            //moduleWeaver.MarkAsGeneratedCode(cacheTypeDefinition.CustomAttributes);
//        }

//        //public FieldReference GetEventArgsField(string propertyName)
//        //{
//        //    if (!_properties.TryGetValue(propertyName, out var field))
//        //    {
//        //        var attributes = FieldAttributes.Assembly | FieldAttributes.Static | FieldAttributes.InitOnly;
//        //        field = new FieldDefinition(propertyName, attributes, _moduleWeaver.PropertyChangedEventArgsReference);
//        //        _properties.Add(propertyName, field);
//        //    }

//        //    return field;
//        //}

//        public void InjectType()
//        {
//            if (_properties.Count == 0)
//            {
//                return;
//            }

//            var attributes = MethodAttributes.Private | MethodAttributes.HideBySig | MethodAttributes.SpecialName | MethodAttributes.RTSpecialName | MethodAttributes.Static;
//            var cctor = new MethodDefinition(".cctor", attributes, _moduleWeaver.TypeSystem.VoidReference);

//            foreach (var pair in _properties.OrderBy(i => i.Key))
//            {
//                var propertyName = pair.Key;
//                var eventArgsField = pair.Value;

//                _cacheTypeDefinition.Fields.Add(eventArgsField);

//                cctor.Body.Instructions.Append(
//                    Instruction.Create(OpCodes.Ldstr, propertyName),
//                    Instruction.Create(OpCodes.Newobj, _moduleWeaver.PropertyChangedEventConstructorReference),
//                    Instruction.Create(OpCodes.Stsfld, eventArgsField)
//                );
//            }

//            cctor.Body.Instructions.Append(
//                Instruction.Create(OpCodes.Ret)
//            );

//            _cacheTypeDefinition.Methods.Add(cctor);
//            _moduleWeaver.ModuleDefinition.Types.Add(_cacheTypeDefinition);
//        }
//    }
//}