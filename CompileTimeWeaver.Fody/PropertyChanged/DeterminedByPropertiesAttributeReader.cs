﻿using Fody;
using Mono.Cecil;
using System.Collections.Generic;
using System.Linq;

namespace CompileTimeWeaver.Fody.PropertyChanged
{
    internal class DeterminedByPropertiesAttributeReader
    {
        internal const string DeterminedByPropertiesAttributeName = "CompileTimeWeaver.DeterminedByPropertiesAttribute";
        private readonly PropertyDefinition _property;

        public DeterminedByPropertiesAttributeReader(PropertyDefinition property)
        {
            _property = property;
        }

        public IEnumerable<PropertyDefinition> GetAncestors()
        {
            return _GetAncestors().Distinct();
        }

        private IEnumerable<PropertyDefinition> _GetAncestors()
        {
            var ancestorPropertyAttributes = _property.CustomAttributes.GetAttributes(DeterminedByPropertiesAttributeName);
            foreach (var attr in ancestorPropertyAttributes)
            {
                var customAttributeArguments = attr.ConstructorArguments.ToList();
                var ancestorPropertyName = (string)customAttributeArguments[0].Value;
                var ancestor = MakeSurePropertyExists(ancestorPropertyName);
                if (ancestor != _property)
                    yield return ancestor;

                if (customAttributeArguments.Count > 1)
                {
                    var otherValue = (CustomAttributeArgument[])customAttributeArguments[1].Value;
                    foreach (string otherAncestorPropertyName in otherValue.Select(x => x.Value))
                    {
                        ancestor = MakeSurePropertyExists(otherAncestorPropertyName);
                        if (ancestor != _property)
                            yield return ancestor;
                    }
                }
            }
         }

        private PropertyDefinition MakeSurePropertyExists(string parentProperty)
        {
            var propertyDefinition = _property.DeclaringType.Properties.FirstOrDefault(x => x.Name == parentProperty);
            if (propertyDefinition == null)
            {
                throw new WeavingException($"Could not find property '{parentProperty}' for DeterminedByPropertiesAttribute assigned to '{_property.Name}'.");
            }

            return propertyDefinition;
        }
    }
}
