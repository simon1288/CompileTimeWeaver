using System;
using System.Linq;
using System.Reflection;
using Fody;
using Mono.Cecil;
using Mono.Cecil.Rocks;

namespace CompileTimeWeaver.Fody
{
    internal class ReferenceFinder
    {
        private readonly ModuleWeaver _moduleWeaver;

        public ReferenceFinder(ModuleWeaver moduleWeaver)
        {
            _moduleWeaver = moduleWeaver;
        }

        public MethodReference GetMethodReference(Type declaringType, Func<MethodDefinition, bool> predicate)
        {
            return GetMethodReference(GetTypeReference(declaringType), predicate);
        }

        public MethodReference GetMethodReference(TypeReference typeReference, Func<MethodDefinition, bool> predicate, bool throwIfNotFound = true)
        {
            var typeDefinition = typeReference.Resolve();

            MethodDefinition methodDefinition;
            do
            {
                methodDefinition = typeDefinition.Methods.FirstOrDefault(predicate);
                typeDefinition = typeDefinition.BaseType?.Resolve();
            }
            while (methodDefinition == null && typeDefinition != null);

            if (methodDefinition == null)
            {
                if (!throwIfNotFound)
                    return null;

                throw new WeavingException($"Failed to locate a method in {typeReference.FullName}");
            }

            return _moduleWeaver.ModuleDefinition.ImportReference(methodDefinition);
        }

        //public MethodReference GetOptionalMethodReference(TypeReference typeReference, Func<MethodDefinition, bool> predicate)
        //{
        //    var typeDefinition = typeReference.Resolve();

        //    MethodDefinition methodDefinition;
        //    do
        //    {
        //        methodDefinition = typeDefinition.Methods.FirstOrDefault(predicate);
        //        typeDefinition = typeDefinition.BaseType?.Resolve();
        //    }
        //    while (methodDefinition == null && typeDefinition != null);

        //    return methodDefinition != null ? _moduleDefinition.ImportReference(methodDefinition) : null;
        //}

        //public MethodReference GetOptionalConstructorReference(TypeReference typeReference, Func<MethodDefinition, bool> predicate)
        //{
        //    var typeDefinition = typeReference.Resolve();

        //    MethodDefinition methodDefinition = typeDefinition.GetConstructors().FirstOrDefault(predicate);

        //    return methodDefinition != null ? _moduleDefinition.ImportReference(methodDefinition) : null;
        //}
        
        public MethodReference GetFirstConstructorReference(TypeReference typeReference)
        {
            var typeDefinition = typeReference.Resolve();

            MethodDefinition methodDefinition = typeDefinition.GetConstructors().FirstOrDefault();

            return methodDefinition != null ? _moduleWeaver.ModuleDefinition.ImportReference(methodDefinition) : null;
        }

        public TypeReference GetTypeReference(Type type)
        {
            return _moduleWeaver.ModuleDefinition.ImportReference(_moduleWeaver.FindTypeDefinition(type.FullName));
        }

        //public MethodReference ResolveMethod(Type declaringType, string methodName, BindingFlags bindingFlags, string typeArgumentList, params string[] paramTypes)
        //{
        //    //var containingAssembly = Assembly.Load(new AssemblyName(assemblyName));
        //    //var declaringType = containingAssembly.GetType(declaringTypeName);

        //    if (declaringType.IsGenericType)
        //    {
        //        var typeArguments = typeArgumentList.Split(',');
        //        declaringType = declaringType.MakeGenericType(typeArguments.Select(Type.GetType).ToArray());
        //    }

        //    if (methodName == ".ctor")
        //    {
        //        var resolvedCtor = declaringType.GetConstructor(
        //            bindingFlags,
        //            null,
        //            paramTypes.Select(Type.GetType).ToArray(),
        //            null);

        //        if (resolvedCtor == null)
        //        {
        //            throw new WeavingException($"Failed to resolve ctor {declaringType}({string.Join(",", paramTypes)})");
        //        }

        //        //return resolvedCtor;
        //        return _moduleWeaver.ModuleDefinition.ImportReference(resolvedCtor);
        //    }

        //    var resolvedMethod = declaringType.GetMethod(methodName,
        //                                                    bindingFlags,
        //                                                    null,
        //                                                    paramTypes.Select(Type.GetType).ToArray(),
        //                                                    null);

        //    if (resolvedMethod == null)
        //    {
        //        throw new WeavingException($"Failed to resolve method {declaringType}.{methodName}({string.Join(",", paramTypes)})");
        //    }

        //    //return resolvedMethod;
        //    return _moduleWeaver.ModuleDefinition.ImportReference(resolvedMethod);
        //}
    }
}